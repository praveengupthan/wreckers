<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row">
                <!-- left navigation -->
                <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <div class="db-pagetitle d-flex justify-content-between">
                        <article>
                            <h2 class="h5 fbold">Dashboard Overview</h2>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, nulla.</p>
                        </article>
                        <div class="form-group">
                            <select class="form-control">
                                <option>This Day</option>
                                <option>This Week</option>
                                <option>This Month</option>
                                <option>This Year</option>
                            </select>
                        </div>
                    </div>

                    <!-- main blocks row -->
                    <div class="row">
                        <!-- col 3-->
                        <div class="col-lg-3">
                            <div class="dbcol text-center col01">
                                <div class="icon">
                                    <span class="icon-check-square-o icomoon"></span>
                                </div>
                                <h2 class="h2 fbold pt-2">122</h2>
                                <p class="text-center">Cars Uploaded</p>
                                <span>5%</span>
                            </div>
                        </div>
                        <!-- /col 3 -->

                         <!-- col 3-->
                         <div class="col-lg-3">
                            <div class="dbcol text-center col02">
                                <div class="icon">
                                    <span class="icon-telephone icomoon"></span>
                                </div>
                                <h2 class="h2 fbold pt-2">44</h2>
                                <p class="text-center">Parts Uploaded</p>
                                <span>15%</span>
                            </div>
                        </div>
                        <!-- /col 3 -->

                        <!-- col 3-->
                        <div class="col-lg-3">
                            <div class="dbcol text-center col03">
                                <div class="icon">
                                    <span class="icon-wechat icomoon"></span>
                                </div>
                                <h2 class="h2 fbold pt-2">22</h2>
                                <p class="text-center">No.of Sales</p>
                                <span>17.00%</span>
                            </div>
                        </div>
                        <!-- /col 3 -->

                         <!-- col 3-->
                         <div class="col-lg-3">
                            <div class="dbcol text-center col04">
                                <div class="icon">
                                    <span class="icon-list-alt icomoon"></span>
                                </div>
                                <h2 class="h2 fbold pt-2">5</h2>
                                <p class="text-center">Total Earned</p>
                                <span>-15.00%</span>
                            </div>
                        </div>
                        <!-- /col 3 -->
                    </div>
                    <!--/ main blocks row -->

                    <!-- main blocks row -->
                    <div class="row">
                        <!-- col 3-->
                        <div class="col-lg-3">
                            <div class="dbcol text-center col01">
                                <div class="icon">
                                    <span class="icon-check-square-o icomoon"></span>
                                </div>
                                <h2 class="h2 fbold pt-2">122</h2>
                                <p class="text-center">Commission Paid</p>
                                <span>-5.00%</span>
                            </div>
                        </div>
                        <!-- /col 3 -->

                         <!-- col 3-->
                         <div class="col-lg-3">
                            <div class="dbcol text-center col02">
                                <div class="icon">
                                    <span class="icon-telephone icomoon"></span>
                                </div>
                                <h2 class="h2 fbold pt-2">44</h2>
                                <p class="text-center">Cancelled</p>
                                <span>15.00%</span>
                            </div>
                        </div>
                        <!-- /col 3 -->

                        <!-- col 3-->
                        <div class="col-lg-3">
                            <div class="dbcol text-center col03">
                                <div class="icon">
                                    <span class="icon-wechat icomoon"></span>
                                </div>
                                <h2 class="h2 fbold pt-2">22</h2>
                                <p class="text-center">Un Read Messages</p>
                                <span>17.00%</span>
                            </div>
                        </div>
                        <!-- /col 3 -->

                         <!-- col 3-->
                         <div class="col-lg-3">
                            <div class="dbcol text-center col04">
                                <div class="icon">
                                    <span class="icon-list-alt icomoon"></span>
                                </div>
                                <h2 class="h2 fbold pt-2">5</h2>
                                <p class="text-center">Part Requests</p>
                                <span>-15.00%</span>
                            </div>
                        </div>
                        <!-- /col 3 -->
                    </div>
                    <!--/ main blocks row -->

                    <!-- graphs row -->
                    <div class="row py-5">
                        <!-- title -->
                        <div class="col-lg-12">
                            <div class="db-pagetitle">
                                <h2 class="h5 fbold">Car Purchases</h2>
                                <p>Lorem ipsum dolor sit amet consectetur.</p>
                            </div>
                        </div>
                        <!--/ title -->
                        <!-- col 12-->
                        <div class="col-lg-12">
                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                        </div>
                        <!--/ col 12-->                     

                    </div>
                    <!--/ graphs row -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>