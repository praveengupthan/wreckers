<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>List of Parts</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left filter col -->
                    <div class="col-lg-3 listleft">
                        <h2 class="h5 fbold">Categories</h2>

                        <ul class="cat-list nav flex-column pb-3">
                            <li class="nav-link">
                                <a href="javascript:void(0)" class="nav-item d-flex justify-content-between">
                                    <span>Body Parts</span>
                                    <span>(20)</span>
                                </a>
                            </li>
                            <li class="nav-link">
                                <a href="javascript:void(0)" class="nav-item d-flex justify-content-between">
                                    <span>Electronics</span>
                                    <span>(12)</span>
                                </a>
                            </li>
                            <li class="nav-link">
                                <a href="javascript:void(0)" class="nav-item d-flex justify-content-between">
                                    <span>Performance Parts</span>
                                    <span>(14)</span>
                                </a>
                            </li>
                            <li class="nav-link">
                                <a href="javascript:void(0)" class="nav-item d-flex justify-content-between">
                                    <span>Repair Parts</span>
                                    <span>(14)</span>
                                </a>
                            </li>
                            <li class="nav-link">
                                <a href="javascript:void(0)" class="nav-item d-flex justify-content-between">
                                    <span>Lighting</span>
                                    <span>(12)</span>
                                </a>
                            </li>
                            <li class="nav-link">
                                <a href="javascript:void(0)" class="nav-item d-flex justify-content-between">
                                    <span>Body Parts</span>
                                    <span>(20)</span>
                                </a>
                            </li>
                            <li class="nav-link">
                                <a href="javascript:void(0)" class="nav-item d-flex justify-content-between">
                                    <span>Denting Parts</span>
                                    <span>(5)</span>
                                </a>
                            </li>
                            <li class="nav-link">
                                <a href="javascript:void(0)" class="nav-item d-flex justify-content-between">
                                    <span>Body Parts</span>
                                    <span>(10)</span>
                                </a>
                            </li>
                            <li class="nav-link">
                                <a href="javascript:void(0)" class="nav-item d-flex justify-content-between">
                                    <span>Tyres & Tubes</span>
                                    <span>(25)</span>
                                </a>
                            </li>
                            <li class="nav-link">
                                <a href="javascript:void(0)" class="nav-item d-flex justify-content-between">
                                    <span>Glass & materials</span>
                                    <span>(18)</span>
                                </a>
                            </li>
                        </ul>

                        <div class="pb-2">
                            <h3 class="h5 fbold">Prices</h3>
                            <ul class="spanlink nav">
                                <li class="nav-link"><a class="nav-item" href="javascript:void(0)">0-$99</a></li>
                                <li class="nav-link"><a class="nav-item" href="javascript:void(0)">$100-$199</a></li>
                                <li class="nav-link"><a class="nav-item" href="javascript:void(0)">$200-$299</a></li>
                                <li class="nav-link"><a class="nav-item" href="javascript:void(0)">$300-$399</a></li>
                                <li class="nav-link"><a class="nav-item" href="javascript:void(0)">$400-$499</a></li>
                                <li class="nav-link"><a class="nav-item" href="javascript:void(0)">$500-$599</a></li>
                                <li class="nav-link"><a class="nav-item" href="javascript:void(0)">$600-$699</a></li>
                                <li class="nav-link"><a class="nav-item" href="javascript:void(0)">$700-$799</a></li>
                            </ul>
                        </div>

                        <div class="pb-2">
                        <h3 class="h5 fbold">Brands</h3>
                            <ul class="spanlink nav">
                            <li class="nav-link"><a class="nav-item" href="javascript:void(0)">Channel</a></li>
                            <li class="nav-link"><a class="nav-item" href="javascript:void(0)">Dior</a></li>
                            <li class="nav-link"><a class="nav-item" href="javascript:void(0)">Hermes</a></li>
                            <li class="nav-link"><a class="nav-item" href="javascript:void(0)">Cherry</a></li>
                            <li class="nav-link"><a class="nav-item" href="javascript:void(0)">Hot</a></li>
                            <li class="nav-link"><a class="nav-item" href="javascript:void(0)">New</a></li>
                            <li class="nav-link"><a class="nav-item" href="javascript:void(0)">Acer</a></li>
                            <li class="nav-link"><a class="nav-item" href="javascript:void(0)">Nokia</a></li>
                            </ul>
                       </div>
                    </div>
                    <!--/ left filter col -->

                    <!-- right products list -->
                    <div class="col-lg-9">
                        <!-- title row -->
                        <div class="row justify-content-between">
                            <!-- col -->
                            <div class="col-lg-4">
                                <h4 class="h5">Wheels and Tiers</h4>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-8 text-right justify-content-end">
                                <div class="form-group float-right">                                   
                                    <select class="form-control">
                                        <option>Sort by</option>
                                        <option>Alphabetically A-Z</option>
                                        <option>Alphabetically Z-A</option>
                                        <option>Price: Low - High</option>
                                        <option>Price: High - Low</option>
                                        <option>New to Old</option>
                                        <option>Old to New</option>
                                    </select>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ title row -->
                        <!-- products row -->
                        <div class="row">
                            <!-- product col -->
                            <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro01.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro02.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro03.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro04.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro05.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro06.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro07.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro08.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro09.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro01.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro02.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->

                             <!-- product col -->
                             <div class="col-lg-4">
                                <div class="product-col">
                                    <figure class="position-relative border-bottom">
                                        <a href="javascript:void(0)"><img src="img/data/pro03.jpg" alt="" class="img-fluid"></a>
                                        <span class="discount">-25%</span>
                                        <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                                    </figure>
                                    <article class="p-2 text-center">
                                        <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                        <p class="text-center pt-3 addcartbtn">
                                            <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                        </p>
                                        <p class="price d-flex justify-content-between py-2 price">
                                            <span class="fred">$76.00</span>
                                            <span class="oldprice">$126.00</span>
                                        </p>
                                    </article>
                                </div>
                            </div>
                            <!--/ product col -->
                        </div>
                        <!--/ products row -->

                        <!-- number nav-->
                        <div class="row justify-content-end pr-3">
                            
                            <nav aria-label="Page navigation example">
                                <ul class="pagination ">
                                    <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                    </li>
                                </ul>
                            </nav>

                        </div>
                        <!--/ numb er nav -->
                    </div>
                    <!--/ right products list -->
                </div>
                <!--/ row -->  
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>