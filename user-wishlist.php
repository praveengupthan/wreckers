<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
                <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->


                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <div class="db-pagetitle">
                        <article>
                            <h2 class="h5 fbold">My Wishlist Products</h2>
                            <p>My Wishlist Products and My Favourite Products</p>
                        </article>                        
                    </div>

                    <!-- row -->
                    <div class="row">
                         <!-- small column -->
                         <div class="col-lg-12">
                             <!-- white box -->
                            <div class="smallcol mb-2 whitebox">                               
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                        <figure class="position-relative">
                                            <a href="javascript:void(0)"><img src="img/data/part01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                            <span class="outofstock position-absolute">
                                                <img src="img/sold-outimg.png" alt="" title="">
                                            </span>
                                        </figure>
                                    </div>
                                    <div class="col-lg-9 col-md-9 pl-0">                                               
                                        <article class="pt-2">
                                            <h5 class="h5 mb-0"><a href="javascript:void(0)">Part Name will be here </a></h5>
                                            <p>In publishing and graphic design, lorem ipsum is a placeholder text . </p>
                                        </article>                                                
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <span class="priceold">$ 5000</span>
                                                <span class="pricenew">$ 3,200</span> 
                                                <span class="pricenew">(-25%)</span>
                                            </div>                                                    
                                        </div>
                                        <ul class="addstosmallcol nav">
                                            <li><a href="javascript:void(0)"><span class="icon-shopping-cart-of-checkered-design icomoon"></span> Add to Cart</a></li>
                                            <li><a href="javascript:void(0)"><span class="icon-trash-o icomoon"></span> Remove from Wishlist</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                             <!--/ white box -->
                        </div>
                        <!-- small column -->

                        <!-- small column -->
                        <div class="col-lg-12">
                             <!-- white box -->
                            <div class="smallcol mb-2 whitebox">                               
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                        <figure class="position-relative">
                                            <a href="javascript:void(0)"><img src="img/data/part01.jpg" alt="" title="" class="img-fluid w-100"></a>  
                                        </figure>
                                    </div>
                                    <div class="col-lg-9 col-md-9 pl-0">                                               
                                        <article class="pt-2">
                                            <h5 class="h5 mb-0"><a href="javascript:void(0)">Part Name will be here </a></h5>
                                            <p>In publishing and graphic design, lorem ipsum is a placeholder text . </p>
                                        </article>                                                
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <span class="priceold">$ 5000</span>
                                                <span class="pricenew">$ 3,200</span> 
                                                <span class="pricenew">(-25%)</span>
                                            </div>                                                    
                                        </div>
                                        <ul class="addstosmallcol nav">
                                            <li><a href="javascript:void(0)"><span class="icon-shopping-cart-of-checkered-design icomoon"></span> Add to Cart</a></li>
                                            <li><a href="javascript:void(0)"><span class="icon-trash-o icomoon"></span> Remove from Wishlist</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                             <!--/ white box -->
                        </div>
                        <!-- small column -->

                        
                        <!-- small column -->
                        <div class="col-lg-12">
                             <!-- white box -->
                            <div class="smallcol mb-2 whitebox">                               
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                        <figure class="position-relative">
                                            <a href="javascript:void(0)"><img src="img/data/part03.jpg" alt="" title="" class="img-fluid w-100"></a>  
                                        </figure>
                                    </div>
                                    <div class="col-lg-9 col-md-9 pl-0">                                               
                                        <article class="pt-2">
                                            <h5 class="h5 mb-0"><a href="javascript:void(0)">Part Name will be here </a></h5>
                                            <p>In publishing and graphic design, lorem ipsum is a placeholder text . </p>
                                        </article>                                                
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <span class="priceold">$ 5000</span>
                                                <span class="pricenew">$ 3,200</span> 
                                                <span class="pricenew">(-25%)</span>
                                            </div>                                                    
                                        </div>
                                        <ul class="addstosmallcol nav">
                                            <li><a href="javascript:void(0)"><span class="icon-shopping-cart-of-checkered-design icomoon"></span> Add to Cart</a></li>
                                            <li><a href="javascript:void(0)"><span class="icon-trash-o icomoon"></span> Remove from Wishlist</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                             <!--/ white box -->
                        </div>
                        <!-- small column -->

                          
                        <!-- small column -->
                        <div class="col-lg-12">
                             <!-- white box -->
                            <div class="smallcol mb-2 whitebox">                               
                                <div class="row">
                                    <div class="col-lg-3 col-md-3">
                                        <figure class="position-relative">
                                            <a href="javascript:void(0)"><img src="img/data/part04.jpg" alt="" title="" class="img-fluid w-100"></a>  
                                        </figure>
                                    </div>
                                    <div class="col-lg-9 col-md-9 pl-0">                                               
                                        <article class="pt-2">
                                            <h5 class="h5 mb-0"><a href="javascript:void(0)">Part Name will be here </a></h5>
                                            <p>In publishing and graphic design, lorem ipsum is a placeholder text . </p>
                                        </article>                                                
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <span class="priceold">$ 5000</span>
                                                <span class="pricenew">$ 3,200</span> 
                                                <span class="pricenew">(-25%)</span>
                                            </div>                                                    
                                        </div>
                                        <ul class="addstosmallcol nav">
                                            <li><a href="javascript:void(0)"><span class="icon-shopping-cart-of-checkered-design icomoon"></span> Add to Cart</a></li>
                                            <li><a href="javascript:void(0)"><span class="icon-trash-o icomoon"></span> Remove from Wishlist</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                             <!--/ white box -->
                        </div>
                        <!-- small column -->


                    </div>
                    <!--/ row -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>