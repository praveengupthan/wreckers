<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Part Request</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Make an Offer</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
            <!-- steps -->
            <div class="steps-wreckers">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6">
                    <article class="text-center pb-3">                       
                        <p class="text-center">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quis sit recusandae fugiat praesentium maxime nobis?</p>
                    </article>
                        <!-- form -->
                        <form action="">                            
                            <!-- div id wizard-->
                            <div id="wizard" class="threesteps">
                                <!-- SECTION 1 -->
                                <h4></h4>
                                <section>
                                    <h5>Your Offer</h5>
                                    <!-- form group -->
                                    <div class="form-group mt-3">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">$</span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Ex:20">
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                    <p class="d-flex justify-content-between pb-1">
                                        <span>Bronze Service Fee</span>
                                        <span>-$27.50</span>   
                                    </p>
                                    <h5 class="d-flex justify-content-between">
                                        <span>You'll Receive</span>
                                        <span class="fbold fblue">$97.50</span>
                                    </h5>

                                    <!-- gray block -->
                                    <div class="graybox p-3 text-center mb-2">
                                        <p class="text-center">Great work - earn more end enjoy a lower service fee! </p>
                                        <img src="img/medal.png" alt="" style="width:90px;">
                                        <p class="text-center">Your Last 30 day total: $0 </p>
                                        <p class="text-center">Your Current tier <span class="fbold fblue">Bronze</span> </p>                                       
                                    </div>
                                    <!--/ gray block -->


                                </section>

                                <!--/ SECTION 1 -->
                                
                                <!-- SECTION 2 -->
                                <h4></h4>
                                <section>
                                    <h5>Make an Offder</h5>                                    
                                    <h5 class="pb-0 h5">Why are you the best person for this task?</h5>

                                    <div class="form-group">
                                        <p>For your safety please do not share personal information, eg: email, phone or address <span>*</span></p>
                                        <textarea style="height:200px;" class="form-control" placeholder="eg. I will    be great for this task.  I have the necessary experience skills and equipment required to get this done"></textarea>
                                        <small>1500 Characters remaining</small>
                                    </div>

                                    <!-- package dimensions -->
                                    <div class="dimensions">
                                        <h5 class="pb-0 h5">Package weight &amp; dimensions <a href="javascript:void(0)"data-toggle="tooltip" Title="I will    be great for this task.  I have the necessary experience skills and equipment required to get this done"><span class="icon-question-circle"></span></a></h5>

                                        <!--form-->
                                        <form>
                                            <!-- form group -->
                                            <div class="form-group">
                                                <label>Package type</label>
                                                <div class="input-group">
                                                    <select class="form-control">
                                                        <option>
                                                            Padded Bags / Satches
                                                        </option>
                                                        <option>
                                                            Bag Type one
                                                        </option>
                                                        <option>
                                                            Bag type Two
                                                        </option>
                                                    </select>
                                                </div>
                                                <input type="checkbox"> irregular package
                                            </div>
                                            <!--/ form group -->

                                            <!-- form group -->
                                            <div class="form-group">
                                                <label>Dimensions </label>
                                                <div class="input-group d-flex">
                                                    <input type="text" style="width:75px">
                                                    <span class="d-inline-block px-1">cm x</span>
                                                    <input type="text" style="width:75px">
                                                    <span class="d-inline-block px-1">cm x</span>
                                                    <input type="text" style="width:75px">
                                                    <span class="d-inline-block px-1">cm</span>
                                                </div>
                                            </div>
                                            <!--/ form group -->
                                            <!-- form group -->
                                            <div class="form-group">
                                                <label>Weight</label>
                                                <div class="input-group d-flex">
                                                     <select class="form-control w-50 mr-2">
                                                        <option>Custom Weight</option>
                                                        <option>100g or Less</option>
                                                        <option>101g to 250g</option>
                                                        <option>251g to 500g</option>
                                                        <option>501g to 1kg</option>
                                                        <option>1+ to 2kg</option>
                                                        <option>2+ to 3kg</option>
                                                        <option>3+ to 4kg</option>
                                                        <option>4+ to 5kg</option>
                                                        <option>5+ to 6kg</option>
                                                        <option>6+ to 7kg</option>
                                                        <option>7+ to 8kg</option>
                                                        <option>8+ to 9kg</option>
                                                        <option>10+ to 11g</option>  
                                                    </select>
                                                    <input type="text" style="width:75px" placeholder="0">
                                                    <span class="d-inline-block px-1">kg</span>
                                                    <input type="text" style="width:75px" placeholder="0">
                                                    <span class="d-inline-block px-1">g</span>
                                                </div>
                                                
                                            </div>
                                            <!--/ form group -->
                                             <!-- form group -->
                                             <div class="form-group">
                                                <label>Enter your Post Code </label>
                                                <div class="input-group d-flex">
                                                    <input type="text" class="form-control"> 
                                                </div>
                                            </div>
                                            <!--/ form group -->
                                        </form>
                                        <!--/ form -->
                                    </div>
                                    <!--/ package dimentions -->
                                        
                                </section>
                                <!-- / SECTION 2-->

                                <!-- SECTION 3 -->
                                <h4></h4>
                                <section>
                                    <h5>Preview Offder</h5>

                                    <div class="graybox p-3 text-center">
                                        <p class="text-center pb-1">Your Offer</p>
                                        <h2 class="fbold fblue h2">$50</h2>
                                    </div>

                                    <p class="d-flex justify-content-between pb-1">
                                        <span>Bronze Service Fee</span>
                                        <span>-$27.50</span>   
                                    </p>
                                    <h5 class="d-flex justify-content-between">
                                        <span>You'll Receive</span>
                                        <span class="fbold fblue">$97.50</span>
                                    </h5>
                                   
                                    <p class="small p-2 text-center">Get and Fix automatically includes Service fee to cover variable insurance and transaction costs</p>     

                                     <div class="graybox p-3 text-center getpaidsecurely">
                                        <p class="text-center pb-0"><span class="icon-unlock-alt icomoon"></span></p>
                                        <h3 class="fbold fblue h3">Get Paid securely</h3>
                                        <p class="text-center pb-1">Task funds are held securely until the task is completed</p>                                       
                                    </div>                               
                                   
                                </section>
                                <!-- Section 3-->
                                
                            </div>
                            <!--/ div id wizard -->
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ col-->
                </div>
                <!--/ row -->
            </div>
            <!-- /steps -->                
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>