<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Blogs</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">                  
                   <!-- col -->
                   <div class="col-lg-12">                       
                        <h2 class="h5 fred">8 Things to keep in mind while driving in Fog</h2>
                        <p class="blog-extra">
                            <span class="extraspan"><span class="icon-calendar icomoon"></span> Oct 26, 2015 5:37:57 PM</span>

                            <span class="extraspan"><span class="icon-user-silhouette icomoon"></span> By Manish</span>

                            <span class="extraspan"><span class="icon-wechat icomoon"></span> <a href="javascript:void(0)">20 Comments</a> </span>
                        </p>
                        <img src="img/data/blog01.jpg" alt="" class="img-fluid w-100">
                        <p>It is that time of the year again, especially for the people in the northern part of India, when the visibility drops suddenly with all the dust and smoke staying close to the ground, making the days very very gloomy. And then one of the days, you will find yourself driving in such thick fog like never before. People who have done it before, know it is no less than nightmare.  </p>
                        <p>It's best to avoid driving in foggy conditions, but if you still need to navigate the roads in thick fog, here are some tips that will help you out.</p>
                        <p>1.Check wipers and defroster before heading out. Even the rear ones. Since most people use the heater inside the car, the temperature difference between the outside and the inside of the car condenses moisture on the windshield. It's difficult to get rid off. Use wipers if it is on the outside. If it's on the inside, use the AC to vent near the windshield to equalise the temperature. </p>
                        <p>2.DO NOT USE HIGH BEAM. As wrong as it may sound to people who love using high beams on their car, it actually reflects straight back into your eyes from the fog, making it really difficult to see anything. Your aim is to see the road right in front of you. So, the lights should be directed as low as possible.</p>
                        <p>3.Use FOG LAMPS. They are mounted low and throw the light right in front and side of the tyres. Making the edge of the road visible.Use blinkers to get other drivers' attention.</p>
                        <p>4.Drive along an edge and not in the center of the road. Find the edge of the road, either to the left(in case of non- divided road) or to the right in case of a divided road. It's easier to see and follow as you will immediately know if you are drifiting away.</p>                       
                   </div>
                   <!--/ col -->
               </div>
               <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>