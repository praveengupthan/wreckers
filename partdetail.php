<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Part Name will be here</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-around">
                    <!-- left image gallery -->
                    <div class="col-lg-6">
                        <!-- thumbnail gallery -->
                         <div class="simplegallery gallery">
                            <div class="content">
                                <img src="img/data/pro01.jpg" class="image_1" alt="" />
                                <img src="img/data/pro02.jpg" class="image_2" style="display:none" alt="" />
                                <img src="img/data/pro03.jpg" class="image_3" style="display:none" alt="" />
                                <img src="img/data/pro04.jpg" class="image_4" style="display:none" alt="" />
                                <img src="img/data/pro05.jpg" class="image_5" style="display:none" alt="" />                                    
                            </div>          

                            <div class="thumbnail">
                                <div class="thumb">
                                    <a href="#" rel="1">
                                        <img src="img/data/pro01.jpg" id="thumb_1" alt="" />
                                    </a>
                                </div>
                                <div class="thumb">
                                    <a href="#" rel="2">
                                        <img src="img/data/pro02.jpg" id="thumb_2" alt="" />
                                    </a>
                                </div>
                                <div class="thumb">
                                    <a href="#" rel="3">
                                        <img src="img/data/pro03.jpg" id="thumb_3" alt="" />
                                    </a>
                                </div>
                                <div class="thumb">
                                    <a href="#" rel="4">
                                        <img src="img/data/pro04.jpg" id="thumb_4" alt="" />
                                    </a>
                                </div>
                                <div class="thumb">
                                    <a href="#" rel="5">
                                        <img src="img/data/pro05.jpg" id="thumb_5" alt="" />
                                    </a>
                                </div>
                                
                            </div>
                        </div> 
                        <!-- thaumbnail gallery -->
                    </div>
                    <!--/ left image gallery -->

                    <!-- right product detail -->
                    <div class="col-lg-6">
                        <!--productdescription-->
                        <div class="product-description">
                            <h2 class="h3">Part Name Will be here</h2>
                            <div class="d-flex">
                                <ul class="review-rating nav pr-4">
                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                </ul>
                                <a class="fgray" href="javascript:void(0)">14 Reviews</a>
                            </div>

                            <!-- table -->
                            <table class="detail-table">
                                <tr>
                                    <td>Availability</td>
                                    <td>:</td>
                                    <td>In Stock</td>
                                </tr>
                                <tr>
                                    <td>Product Type</td>
                                    <td>:</td>
                                    <td>Tier</td>
                                </tr>
                                <tr>
                                    <td>Vendor</td>
                                    <td>:</td>
                                    <td>SS Auto Parts, City Name, State</td>
                                </tr>
                            </table>
                            <!--/ table-->

                            <p class="price d-flex py-2 price">
                                <span class="fred fbold">$76.00</span>
                                <span class="oldprice">$126.00</span>
                            </p>

                            <p class="py-2">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore...</p>

                            <div class="d-flex py-3 align-self-center">
                                <!-- increase and decrease value -->
                                <form class="value-highanddown">
                                    <div class="value-button" id="decrease" onclick="decreaseValue()" value="Decrease Value">-</div>
                                    <input type="number" id="number" value="0" />
                                    <div class="value-button" id="increase" onclick="increaseValue()" value="Increase Value">+</div>
                                </form>
                                <!--/ increase and decraqse value -->
                                <button class="whitebtn mx-3 btn"><span class="icon-shopping-cart icomoon"></span> Add to Cart</button>
                                <button class="redbtn  btn"> Buy Now</button>
                                <button class="whitebtn btn"><span class="icon-heart"></span></button>
                            </div>

                            <!-- question -->
                            <div class="question-product py-3 align-self-center">
                                <div class="d-flex justify-content-around">
                                    <article>
                                        <h3 class="h6 fred text-uppercase">Have a Question?</h3>
                                        <p>Be the first to ask a question about this.</p>
                                    </article>
                                    <button class="whitebtn btn" id="askqstnbtn"><span class="icon-question-circle"></span>Ask a Question</button>
                                </div>
                                <form class="qstn-form" id="qstnForm">
                                    <div class="row">
                                        <div class="col-lg-6">
                                           <div class="form-group">
                                                <input type="text" placeholder="Your Name" class="form-control">
                                           </div>
                                        </div>
                                        <div class="col-lg-6">
                                           <div class="form-group">
                                                <input type="text" placeholder="Your Email" class="form-control">
                                           </div>
                                        </div>
                                        <div class="col-lg-12">
                                           <div class="form-group">
                                                <textarea class="form-control" style="height:100px;" placeholder="Write your Question"></textarea>
                                           </div>
                                        </div>
                                        <div class="col-lg-12">
                                           <div class="form-group">
                                               <input type="submit" class="btn blackbtn" value="Submit Question">
                                               <input id="hideQuestion" type="button" class="btn whitebtn" value="Cancel">
                                           </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--/ question -->
                            <figure>
                                <p>Guaranteed Safe Checkout:</p>
                                <img src="img/paymentimg.jpg" alt="">
                            </figure>
                        </div>
                        <!--/productdescription-->
                    </div>
                    <!--/ right prduct detail -->
                </div>
                <!--/ row -->  

                <!-- row for product description -->
                <div class="row tabrow">
                    <!-- col -->
                    <div class="col-lg-12">
                        <!-- tab-->
                        <div class="parentHorizontalTab">
                            <ul class="resp-tabs-list hor_1">
                                <li>Specifications</li>
                                <li>Shipping & Returns</li>
                                <li>Reviews</li>
                            </ul>
                            <div  class="resp-tabs-container hor_1">
                                <!-- specifications -->
                                <div>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora similique nesciunt nostrum possimus omnis, quos illum mollitia quae aliquid corrupti eum pariatur et sunt architecto perferendis aperiam repellendus iste temporibus rem nisi corporis molestias atque. Accusantium, nostrum! Eum rerum nam in, nesciunt et quae deleniti voluptatum quibusdam placeat, eius vel!</p>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab nulla minus nostrum possimus blanditiis eius officiis nemo nesciunt voluptatibus mollitia.</p>
                                </div>
                                <!--/ specifications -->

                                <!-- shipping and returns -->
                                <div>
                                    <p>Your order of 100$ or more gets free standard delivery.</p>
                                    <ul>
                                        <li>Standard delivered 4-5 Business Days</li>
                                        <li>Express delivered 2-4 Business Days</li>
                                    </ul>
                                    <p>Orders are processed and delivered Monday-Friday (excluding public holidays)</p>
                                    <p>eMarket members enjoy free returns.</p>
                                </div>
                                <!--/ shippng and returns -->

                                <!-- reviews -->
                                <div>
                                   <div class="border-bottom">
                                        <h6 class="h6 d-flex justify-content-between">
                                            <span>Customer Reviews</span>
                                            <a id="writereview" class="redbtn" href="javascript:void(0)">Write Review</a>
                                        </h6>
                                        <p>No reviews yet</p>
                                   </div>
                                   <!-- form -->
                                   <div class="form-review pt-4">
                                        <h6 class="h6 d-flex justify-content-between">                                             <span>Write a Review</span>
                                        </h6>
                                        <!-- row -->
                                        <div class="row">
                                          <!-- col -->
                                          <div class="col-lg-6">
                                            <!-- grom group -->
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input class="form-control" type="text" placeholder="Write your First name">
                                            </div>
                                            <!--/ form group -->
                                          </div>
                                          <!--/ col -->
                                           <!-- col -->
                                           <div class="col-lg-6">
                                            <!-- grom group -->
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input class="form-control" type="text" placeholder="Write your Last Name">
                                            </div>
                                            <!--/ form group -->
                                          </div>
                                          <!--/ col -->
                                      </div>
                                      <!--/ row -->
                                       <!-- row -->
                                       <div class="row">
                                          <!-- col -->
                                          <div class="col-lg-6">
                                            <!-- grom group -->
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input class="form-control" type="text" placeholder="Write Email">
                                            </div>
                                            <!--/ form group -->
                                          </div>
                                          <!--/ col -->
                                           <!-- col -->
                                           <div class="col-lg-6">
                                            <!-- grom group -->
                                            <div class="form-group">
                                                <label>Review Title</label>
                                                <input class="form-control" type="text" placeholder="Write Review Title">
                                            </div>
                                            <!--/ form group -->
                                          </div>
                                          <!--/ col -->
                                      </div>
                                      <!--/ row -->
                                      <div class="row">
                                          <div class="col-lg-12">
                                            <label>Select Rating</label>
                                            <div class="rating"></div>
                                          </div>
                                      </div>
                                       <!-- row -->
                                       <div class="row">
                                          <!-- col -->
                                          <div class="col-lg-12">
                                            <!-- grom group -->
                                            <div class="form-group">
                                                <label>Body of Review (1500) characters remaining</label>
                                                <textarea class="form-control" style="height:100px;" placeholder="Write Review Messsage"></textarea>
                                            </div>
                                            <!--/ form group -->
                                          </div>
                                          <!--/ col -->                                          
                                      </div>
                                      <!--/ row -->
                                      <div class="row">
                                          <div class="col-lg-12">
                                              <input type="submit" value="Submit Review" class="redbtn">
                                          </div>
                                      </div>
                                   </div>
                                   <!--/ form -->
                                </div>
                                <!--/ reviews -->
                            </div>
                        </div>
                        <!--/ tab  -->
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row for product description -->

                <!-- row related products -->
                <div class="row related-products">
                    <div class="col-lg-12">
                        <h5 class="h6 fred text-uppercase">Related Products</h5>
                    </div>
                      <!-- product col -->
                      <div class="col-lg-3">
                        <div class="product-col">
                            <figure class="position-relative border-bottom">
                                <a href="javascript:void(0)"><img src="img/data/pro07.jpg" alt="" class="img-fluid"></a>
                                <span class="discount">-25%</span>
                                <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                            </figure>
                            <article class="p-2 text-center">
                                <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                <p class="text-center pt-3 addcartbtn">
                                    <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                </p>
                                <p class="price d-flex justify-content-between py-2 price">
                                    <span class="fred">$76.00</span>
                                    <span class="oldprice">$126.00</span>
                                </p>
                            </article>
                        </div>
                    </div>
                    <!--/ product col -->

                    <!-- product col -->
                    <div class="col-lg-3">
                        <div class="product-col">
                            <figure class="position-relative border-bottom">
                                <a href="javascript:void(0)"><img src="img/data/pro08.jpg" alt="" class="img-fluid"></a>
                                <span class="discount">-25%</span>
                                <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                            </figure>
                            <article class="p-2 text-center">
                                <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                <p class="text-center pt-3 addcartbtn">
                                    <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                </p>
                                <p class="price d-flex justify-content-between py-2 price">
                                    <span class="fred">$76.00</span>
                                    <span class="oldprice">$126.00</span>
                                </p>
                            </article>
                        </div>
                    </div>
                    <!--/ product col -->

                    <!-- product col -->
                    <div class="col-lg-3">
                        <div class="product-col">
                            <figure class="position-relative border-bottom">
                                <a href="javascript:void(0)"><img src="img/data/pro09.jpg" alt="" class="img-fluid"></a>
                                <span class="discount">-25%</span>
                                <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                            </figure>
                            <article class="p-2 text-center">
                                <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                <p class="text-center pt-3 addcartbtn">
                                    <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                </p>
                                <p class="price d-flex justify-content-between py-2 price">
                                    <span class="fred">$76.00</span>
                                    <span class="oldprice">$126.00</span>
                                </p>
                            </article>
                        </div>
                    </div>
                    <!--/ product col -->
                    <!-- product col -->
                    <div class="col-lg-3">
                        <div class="product-col">
                            <figure class="position-relative border-bottom">
                                <a href="javascript:void(0)"><img src="img/data/pro07.jpg" alt="" class="img-fluid"></a>
                                <span class="discount">-25%</span>
                                <a href="javascript:void(0)" class="wish"><span class="icon-heart"></span></a>
                            </figure>
                            <article class="p-2 text-center">
                                <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                <p class="text-center pt-3 addcartbtn">
                                    <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                </p>
                                <p class="price d-flex justify-content-between py-2 price">
                                    <span class="fred">$76.00</span>
                                    <span class="oldprice">$126.00</span>
                                </p>
                            </article>
                        </div>
                    </div>
                    <!--/ product col -->
                </div>
                <!--/ row relateid products -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>