<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Monthyly Statements </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
                <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->


                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <div class="db-pagetitle d-flex justify-content-between">
                        <article>
                            <h2 class="h5 fbold">Monthly Statements</h2>
                            <p class="pb-0">My Wishlist Products and My Favourite Products</p>
                        </article>  
                        <div class="form-group">
                            <select class="form-control">
                                <option>All</option>
                                <option>This Month</option>
                                <option>Last Month</option>
                                <option>Cancel</option>
                                <option>Refunds</option>
                            </select>
                        </div>    
                        <a class="redbtn" style="height:45px;" href="javascript:void(0)">Download</a>                  
                    </div>

                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12">
                            
                        <!-- accordian -->
                        <div class="accordion cust-accord">
                            <h3 class="panel-title d-flex justify-content-between">
                                <span>12 December 2019, 6:02 PM</span>
                                <span class="d-inline-block px-3">Melbourne ...9967</span>
                            </h3>
                            <div class="panel-content">
                                <h5 class="h5">A$10.00 <span style="color:red">Cancelled</span></h5>
                                <p>Your Uber X trip with Sami Mohiuddin</p>
                                <p>100 Maddox Rd, Willioms Town, North VIC, 3016, Australia</p>
                            </div>

                            <h3 class="panel-title d-flex justify-content-between">
                                <span>12 December 2019, 6:02 PM</span>
                                <span class="d-inline-block px-3">Melbourne ...9967</span>
                            </h3>
                            <div class="panel-content">
                                <h5 class="h5">A$10.00 <span style="color:red">Cancelled</span></h5>
                                <p>Your Uber X trip with Sami Mohiuddin</p>
                                <p>100 Maddox Rd, Willioms Town, North VIC, 3016, Australia</p>
                            </div>

                            <h3 class="panel-title d-flex justify-content-between">
                                <span>12 December 2019, 6:02 PM</span>
                                <span class="d-inline-block px-3">Melbourne ...9967</span>
                            </h3>
                            <div class="panel-content">
                                <h5 class="h5">A$10.00 <span style="color:red">Cancelled</span></h5>
                                <p>Your Uber X trip with Sami Mohiuddin</p>
                                <p>100 Maddox Rd, Willioms Town, North VIC, 3016, Australia</p>
                            </div>

                            <h3 class="panel-title d-flex justify-content-between">
                                <span>12 December 2019, 6:02 PM</span>
                                <span class="d-inline-block px-3">Melbourne ...9967</span>
                            </h3>
                            <div class="panel-content">
                                <h5 class="h5">A$10.00 <span style="color:red">Cancelled</span></h5>
                                <p>Your Uber X trip with Sami Mohiuddin</p>
                                <p>100 Maddox Rd, Willioms Town, North VIC, 3016, Australia</p>
                            </div>

                            <h3 class="panel-title d-flex justify-content-between">
                                <span>12 December 2019, 6:02 PM</span>
                                <span class="d-inline-block px-3">Melbourne ...9967</span>
                            </h3>
                            <div class="panel-content">
                                <h5 class="h5">A$10.00 <span style="color:red">Cancelled</span></h5>
                                <p>Your Uber X trip with Sami Mohiuddin</p>
                                <p>100 Maddox Rd, Willioms Town, North VIC, 3016, Australia</p>
                            </div>

                            <h3 class="panel-title d-flex justify-content-between">
                                <span>12 December 2019, 6:02 PM</span>
                                <span class="d-inline-block px-3">Melbourne ...9967</span>
                            </h3>
                            <div class="panel-content">
                                <h5 class="h5">A$10.00 <span style="color:red">Cancelled</span></h5>
                                <p>Your Uber X trip with Sami Mohiuddin</p>
                                <p>100 Maddox Rd, Willioms Town, North VIC, 3016, Australia</p>
                            </div>

                            <h3 class="panel-title d-flex justify-content-between">
                                <span>12 December 2019, 6:02 PM</span>
                                <span class="d-inline-block px-3">Melbourne ...9967</span>
                            </h3>
                            <div class="panel-content">
                                <h5 class="h5">A$10.00 <span style="color:red">Cancelled</span></h5>
                                <p>Your Uber X trip with Sami Mohiuddin</p>
                                <p>100 Maddox Rd, Willioms Town, North VIC, 3016, Australia</p>
                            </div>

                            <h3 class="panel-title d-flex justify-content-between">
                                <span>12 December 2019, 6:02 PM</span>
                                <span class="d-inline-block px-3">Melbourne ...9967</span>
                            </h3>
                            <div class="panel-content">
                                <h5 class="h5">A$10.00 <span style="color:red">Cancelled</span></h5>
                                <p>Your Uber X trip with Sami Mohiuddin</p>
                                <p>100 Maddox Rd, Willioms Town, North VIC, 3016, Australia</p>
                            </div>

                            <h3 class="panel-title d-flex justify-content-between">
                                <span>12 December 2019, 6:02 PM</span>
                                <span class="d-inline-block px-3">Melbourne ...9967</span>
                            </h3>
                            <div class="panel-content">
                                <h5 class="h5">A$10.00 <span style="color:red">Cancelled</span></h5>
                                <p>Your Uber X trip with Sami Mohiuddin</p>
                                <p>100 Maddox Rd, Willioms Town, North VIC, 3016, Australia</p>
                            </div>

                            <h3 class="panel-title d-flex justify-content-between">
                                <span>12 December 2019, 6:02 PM</span>
                                <span class="d-inline-block px-3">Melbourne ...9967</span>
                            </h3>
                            <div class="panel-content">
                                <h5 class="h5">A$10.00 <span style="color:red">Cancelled</span></h5>
                                <p>Your Uber X trip with Sami Mohiuddin</p>
                                <p>100 Maddox Rd, Willioms Town, North VIC, 3016, Australia</p>
                            </div>
                            
                            
                            
                         </div>
                        <!--/ accordian -->
                           

                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>