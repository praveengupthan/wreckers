<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Part Request</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Part Request</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
            <!-- steps -->
            <div class="steps-wreckers">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6">
                        <!-- form -->
                        <form action="">                            
                            <!-- div id wizard-->
                            <div id="wizard">
                                <!-- SECTION 1 -->
                                <h4></h4>
                                <section>
                                    <h5>Part Details</h5>
                                    <div class="form-group">
                                        <label class="label">Enter Your Part Title <span>*</span></label>
                                        <input type="text" class="form-control pt-0" placeholder="Ex: Need my bumper Fixed">
                                    </div>
                                    <div class="form-group">
                                        <label class="label">Describe your Part <span>*</span></label>
                                        <textarea style="height:100px;" class="form-control" placeholder="(Minimum 25 Characters)"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Upload Images  </label>
                                        <div class="custom-file-upload mr-2">
                                            <label for="file-upload" class="custom-file-upload1">
                                                <i class="fa fa-cloud-upload"></i> Upload Image
                                            </label>
                                            <input id="file-upload" type="file" multiple/>
                                        </div>                                       
                                    </div>   
                                    <div class="form-group">
                                        <p class="pb-0">Do you need Mechanic Fix it?</p>
                                        <div class="custom-control custom-radio d-inline-block">
                                            <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio3">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio d-inline-block ml-3">
                                            <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio4">No</label>
                                        </div>
                                    </div>                              
                                </section>

                                <!--/ SECTION 1 -->
                                
                                <!-- SECTION 2 -->
                                <h4></h4>
                                <section>
                                    <h5>Mechanical Fix Part Details</h5>
                                    <div class="form-group">
                                        <label class="label">Enter Your Part Title <span>*</span></label>
                                        <input type="text" class="form-control pt-0" placeholder="Ex: Need my bumper Fixed">
                                    </div>
                                    <div class="form-group">
                                        <label class="label">Describe your Part <span>*</span></label>
                                        <textarea style="height:100px;" class="form-control" placeholder="(Minimum 25 Characters)"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Upload Images  </label>
                                        <div class="custom-file-upload mr-2">
                                            <label for="file-upload" class="custom-file-upload1">
                                                <i class="fa fa-cloud-upload"></i> Upload Image
                                            </label>
                                            <input id="file-upload" type="file" multiple/>
                                        </div>                                       
                                    </div>   
                                </section>
                                <!-- / SECTION 2-->

                                <!-- SECTION 3 -->
                                <h4></h4>
                                <section>
                                    <h5>Mobile Service</h5>
                                    <div class="form-group">
                                        <p class="pb-0">Do you want a mobile Service?</p>
                                        <div class="custom-control custom-radio d-inline-block">
                                            <input type="radio" id="customRadio8" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio8">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio d-inline-block ml-3">
                                            <input type="radio" id="customRadio9" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio9">No</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="label">What is your Location  <span>*</span></label>
                                        <input type="text" class="form-control pt-0" placeholder="Enter your Pincode / Postal Code">
                                    </div>

                                    <div class="form-group">
                                        <p class="pb-0">When do you need it?</p>
                                        <div class="custom-control custom-radio d-inline-block">
                                            <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio3">ASAP</label>
                                        </div>
                                        <div class="custom-control custom-radio d-inline-block ml-3">
                                            <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio4">Before</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="label">Select Date  <span>*</span></label>
                                        <input type="text" class="form-control pt-0" id="datepicker"  placeholder="Select Date">
                                    </div>                                  
                                </section>
                                <!-- Section 3-->

                                 <!-- SECTION 4 -->
                                 <h4></h4>
                                <section>   
                                    <h5>Waht is your Budget</h5>     
                                    <p>Please Enter the price you're comfortable to pay to get your task done.  Taskers will use as a guide for how much to offer</p>     

                                    <div class="form-group">                                       
                                        <div class="custom-control custom-radio d-inline-block">
                                            <input type="radio" id="customRadio10" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio10">Total</label>
                                        </div>
                                        <div class="custom-control custom-radio d-inline-block ml-3">
                                            <input type="radio" id="customRadio11" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio11">Hourly Rate</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">$</span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Ex:20">
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="final-budget d-flex justify-content-between">
                                        <div>
                                            <h5 class="h5 m-0 pb-1">Estimated Budget</h5>
                                            <p class="pb-0">Final Payment will be agreed later</p>
                                        </div>
                                        <div>
                                            <h4 class="h3"><small>$</small> 0</h4>
                                        </div>
                                    </div> 
                                   
                                </section>
                                <!-- Section 4-->
                            </div>
                            <!--/ div id wizard -->
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ col-->
                </div>
                <!--/ row -->
            </div>
            <!-- /steps -->
                <form>
                    <!-- row -->
                    <div class="row justify-content-around">
                        

                        <!-- right col 6 -->
                        <div class="col-lg-5">
                            
                            
                        </div>
                        <!--/ right col 6 -->
                    </div>
                    <!--/ row -->
                </form>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>