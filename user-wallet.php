<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Wallet Balance </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
                <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol wallet">
                    <div class="db-pagetitle d-flex justify-content-between">
                        <article>
                            <h2 class="h5 fbold">Wallet</h2>
                            <p class="pb-0">Update your Payment Wallet</p>
                        </article>                                           
                    </div>  
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-2">
                            <img class="img-fluid" src="img/wallet.png" alt="">
                        </div>
                        <!-- col 4-->
                        <div class="col-lg-5 align-self-center">                                                   
                            <span class="fgreen h1 flight">$250</span>
                            <p>
                                <small clas="small">Wallet Balance available - 
                                <a data-toggle="popover" title="Wallet Balance" data-content="Popovers must be initialized with jQuery: select the specified element and call the popover() method." class="fblue" href="javascript:void(0)">What is this <span class="icon-exclamation-circle"></span></a></small>
                            </p>      
                            <a class="redbtn" href="user-withdraw.php">Withdraw</a>         
                        </div>
                        <!--/ col 4-->

                        <!-- col 6-->
                        <div class="col-lg-5 align-self-center">    
                            <p>
                                <small>Link a bank account or add a credit or debit card to add money to you Wallet Balance</small>
                            </p>      
                                                          
                            <div class="dropdown">
                                <a href="javascript:void(0)"  class="dropdown-toggle fblue" data-toggle="dropdown">
                                    Add to Wallet Balance
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Link a Bank Account</a>
                                    <a class="dropdown-item" href="#">Add Credit & Debit Card</a> 
                                </div>
                            </div>
                           
                        </div>
                        <!--/ col 6-->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

    <!-- Add Payment -->
    <!-- The Modal -->
  <div class="modal fade" id="select-payment-method">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
            <h4 class="modal-title h5">Choose Payment Method</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <table class="table table-hover">            
                <tbody>
                    <tr>      
                        <td><span class="icon-paypal"></span> Paypal ....1256</td> 
                        <td><a class="select-payment select-pay" href="javascript:void(0)"><span class="icon-check-circle icomoon fgreen"></span></a></td>
                    </tr>
                    <tr>  
                        <td><span class="icon-cc-visa"></span> Visa ....1256</td> 
                        <td><a class="select-payment" href="javascript:void(0)"><span class="icon-check-circle icomoon"></span></a></td>
                    </tr> 
                </tbody>
            </table>
            <input type="submit" class="redbtn" value="Continue">            
        </div>        
        <!-- /modal body -->
    </div>
  </div>
    <!--/ Add Payment -->
</body>

</html>