<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
              <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <div class="db-pagetitle">
                        <article>
                            <h2 class="h5 fbold">Refer a Friend Program</h2>
                            <p>Keep up to date with your tasks</p>
                        </article>                        
                    </div>

                    <div class="whitebox mb-3">
                        <div class="row">
                            <div class="col-lg-4">
                                <img src="img/Illu-Gift.png" alt="" class="img-fluid">
                            </div>
                            <div class="col-lg-8 align-self-center">
                                <h4 class="h4">Get free Airtasker Credit</h4>
                                <p>Invite a friend to try Airtasker with $25 towards their first task. Once it’s completed, you’ll get $10 Airtasker Credit*.</p>
                            </div>
                        </div>
                    </div>

                    <p>Referral Coupons may be used in the purchase of a Task on the Airtasker Platform. Referred Member must become an Airtasker User and agree to the Airtasker Terms & Conditions. Minimum Task value to receive the benefit conferred by the Referral Coupon is $100. Limit of one Referral Coupon per User & valid only for the first Task Contract created by the Referred Member. Cannot be used with any other coupon or Airtasker promotion. Referral Coupons are valid for at least 30 days from issue and Airtasker may change or cancel the terms of the Referral Coupon at any time on 30 days' notice.</p>

                    <p>The Referring Member will receive a $10 Task credit towards the purchase of tasks on the Airtasker Platform, when the Referred Member has completed their first Task Contract. The Task credit is valid for 12 months from issue, and is valid for the payment of Posted Tasks on the Airtasker Platform. Posted Tasks must comply with Terms & Conditions. Limit of 15 x $10 Task credits issued per Referring Member. Airtasker may change or cancel the terms of the Task credit at any time on 30 days' notice.</p>

                   

                  


                  






                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>