<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Prducts Inventory </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">            

                <!-- dashboard right -->
                <div class="col-lg-12 user-rightcol">
                    <!-- title -->
                    <div class="db-pagetitle">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-12">
                                <article class="d-flex justify-content-between">
                                    <h2 class="h5 fbold">Inventory</h2>   
                                    <a href="dashboard2.php"><span class="icon-long-arrow-left"></span> Back to Dashboard</a>                         
                                </article>
                            </div>
                            <!--/ col --> 
                        </div>
                         <!--/ row -->  
                    </div>
                    <!--/ title -->

                    <!-- body -->
                    <div class="report-body">
                        <!-- row filter-->
                        <form class="row justify-content-between">
                            <!-- col -->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="input-group position-relative in-db">
                                        <input style="padding-right:50px;" type="text" class="form-control" placeholder="Search by Namer">
                                        <span class="icon-search position-absolute icomoon"></span>
                                    </div>
                                </div>
                            </div>
                            <!--/ col -->
                            
                          
                             <!-- col -->
                             <div class="col-lg-2">
                                <div class="form-group">
                                    <input type="button" class="redbtn" value="Export CSV">
                                </div>
                            </div>
                            <!--/ col -->
                        </form>
                        <!--/row filter -->
                        <!-- span filters row -->
                        <div class="row inventory-search-filters">
                            <!-- col -->
                            <div class="col-lg-12">
                                <h5 class="h5">Search by Filter</h5>
                                <a href="javascript:void(0)">Car</a>
                                <a href="javascript:void(0)">Car Parts</a>
                                <a href="javascript:void(0)">Price</a>
                                <a href="javascript:void(0)">Commission</a>
                                <a href="javascript:void(0)">Approved</a>
                                <a href="javascript:void(0)">Rejected</a>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ san filters row -->
                         <!-- table -->
                         <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Image</th>
                                    <th scope="col">Code</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Product Title</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Commission</th>
                                    <th scope="col">Qty</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Dt Published</th>
                                    <th scope="col">Views</th>
                                    <th scope="col">Actions</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                 <!-- row -->
                                <tr>
                                    <td>
                                        <a href="javascript:void(0)">
                                            <img width="50" src="img/data/cathome02.jpg">
                                        </a>
                                    </td>
                                    <td class="fbold"><a href="javascript:void(0)">X321</a></td>
                                    <td>CAR</td>
                                    <td class="fbold"><a href="javascript:void(0)">TOYOTA CAMRY BUMPER</a></td>
                                    <td>$50</td>
                                    <td>$5</td>
                                    <td>1</td>
                                    <td>Approved</td>
                                    <td>12-12-2019</td>
                                    <td>25</td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Actions
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#">Edit</a>
                                                <a class="dropdown-item" href="#">Delete</a>
                                                <a class="dropdown-item" href="#">Clone</a>
                                                <a class="dropdown-item" href="#">Deactive</a>
                                            </div>
                                        </div>
                                    </td>                                   
                                </tr>  
                                <!--/ row -->

                                 <!-- row -->
                                 <tr>
                                    <td>
                                        <a href="javascript:void(0)">
                                            <img width="50" src="img/data/cathome03.jpg">
                                        </a>
                                    </td>
                                    <td class="fbold"><a href="javascript:void(0)">Y324</a></td>
                                    <td>CAR PART</td>
                                    <td class="fbold"><a href="javascript:void(0)">TOYOTA CAMRY SIDE DOOR</a></td>
                                    <td>$100</td>
                                    <td>$10</td>
                                    <td>1</td>
                                    <td>Approved</td>
                                    <td>11-12-2019</td>
                                    <td>5</td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Actions
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#">Edit</a>
                                                <a class="dropdown-item" href="#">Delete</a>
                                                <a class="dropdown-item" href="#">Clone</a>
                                                <a class="dropdown-item" href="#">Deactive</a>
                                            </div>
                                        </div>
                                    </td>                                   
                                </tr>  
                                <!--/ row -->

                                  <!-- row -->
                                  <tr>
                                    <td>
                                        <a href="javascript:void(0)">
                                            <img width="50" src="img/data/cathome04.jpg">
                                        </a>
                                    </td>
                                    <td class="fbold"><a href="javascript:void(0)">Y324</a></td>
                                    <td>CAR PART</td>
                                    <td class="fbold"><a href="javascript:void(0)">TOYOTA CAMRY SIDE DOOR</a></td>
                                    <td>$100</td>
                                    <td>$10</td>
                                    <td>1</td>
                                    <td>Approved</td>
                                    <td>11-12-2019</td>
                                    <td>5</td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Actions
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#">Edit</a>
                                                <a class="dropdown-item" href="#">Delete</a>
                                                <a class="dropdown-item" href="#">Clone</a>
                                                <a class="dropdown-item" href="#">Deactive</a>
                                            </div>
                                        </div>
                                    </td>                                   
                                </tr>  
                                <!--/ row -->
                                                   
                            </tbody>
                        </table>
                        <!--/ table -->
                    </div>
                    <!--/ body -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>