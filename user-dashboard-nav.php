<div id="MainMenu">
    <div class="user-navigation">
        <a class="active-usernav" href="dashboard2.php" data-parent="#MainMenu">Dashboard</a>
        <a href="user-profile.php" data-parent="#MainMenu">My Profile</a>
        <a href="user-membershipdetails.php" data-parent="#MainMenu">Membership Details</a>  

        <a href="inventory.php" data-parent="#MainMenu">Inventory</a> 
        <a href="#demo1" class="" data-toggle="collapse" data-parent="#MainMenu">Purchases<i class="fa fa-caret-down"></i></a>
        <div class="collapse" id="demo1">           
            <a href="purchases-product-sales-report.php">Product Sales Report</a>
            <a href="purchases-order-history.php">Purchase Order History</a>   
            <a href="payments-commissions.php">Payments &amp; Commissions</a>         
        </div>


        <a href="user-myoreders.php" data-parent="#MainMenu">My Orders</a> 
        <a href="user-shipping-details.php" data-parent="#MainMenu">Shipping Details</a> 
        <a href="user-wishlist.php" data-parent="#MainMenu">My Wishlist</a> 
        <a href="user-car-service-history.php" data-parent="#MainMenu">Car Services History</a> 
        <a href="taskdetail.php" data-parent="#MainMenu">Messages</a> 
        <a href="profile.php" data-parent="#MainMenu">My public Profile</a> 

        <a href="#demo2" class="" data-toggle="collapse" data-parent="#MainMenu">Payment Info<i class="fa fa-caret-down"></i></a>
        <div class="collapse" id="demo2">
            <a href="user-wallet.php">Wallet</a>
            <!-- <a href="user-payment-withdraw.php">Withdraw</a> -->
            <a href="user-update-payment-details.php">Update Payment Details</a>
            <a href="user-monthly-statements.php">Monthly Statement</a>           
        </div>
       
        <a href="notifications.php" data-parent="#MainMenu">Notifications</a> 
        <a href="user-referafriend.php" data-parent="#MainMenu">Refer a Friend</a> 
        <a href="user-faq.php" data-parent="#MainMenu">Faqs</a> 
        <a href="user-changepassword.php" data-parent="#MainMenu">Chagne password</a> 
        <a href="index.php" data-parent="#MainMenu">Logout</a> 
    </div>
</div>