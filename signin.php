<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->

    <!--main -->
    <main>
        <!-- sign template -->
        <div class="sign-section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-5">
                        <h1 class="title-login">USER SIGNIN</h1>
                       
                        <div class="signcol">
                            <form>
                                <div class="form-group">
                                    <label>User Name</label>
                                    <input type="text" class="form-control" placeholder="Username / Email">
                                    <span class="icon-user-silhouette icomoon"></span>
                                </div>
                                <div class="form-group mb-0">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Enter Password">
                                    <span class="icon-unlock icomoon"></span>
                                </div>
                                <div class="text-right pb-2">
                                    <p><a href="javascript:void(0)">Forgot Password?</a></p>
                                </div>
                                <input type="submit" value="SIGNIN" class="w-100 redbtn">
                            </form>

                            <p class="text-center or"><span>OR</span></p>
                            <p class="small text-center">Signin with your Social Networks</p>

                            <div class="d-flex justify-content-between py-3">
                                <a class="socialbtn fbbtn" href="javascript:void(0)">
                                    <span class="icon-facebook icomoon"></span> 
                                    Facebook
                                </a>
                                <a class="socialbtn googlesocial" href="javascript:void(0)">
                                    <span class="icon-google-plus icomoon"></span> 
                                    Google Plus
                                </a>
                            </div>
                            <p class="text-center small">Don’t have Account?, Register as</p>
                            <ul class="nav justify-content-center">
                                <li class="nav-item"><a class="nav-link text-uppercase" href="javascript:void(0)">CUSTOMER</a></li>
                                <li class="nav-item"><a class="nav-link text-uppercase" href="javascript:void(0)">WRECKER</a></li>
                                <li class="nav-item"><a class="nav-link text-uppercase" href="javascript:void(0)">MECHANIC</a></li>
                            </ul>
                        </div>
                       
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sign template -->
    </main>
    <!--/ main -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>