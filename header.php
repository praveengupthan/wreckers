<header class="fixed-top">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bars icomoon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav  leftnav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">Shop</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Part Request
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="requestpart.php">Post a Request</a>
                        <a class="dropdown-item" href="javascript:void(0)">Browse Tasks</a>
                        <a class="dropdown-item" href="javascript:void(0)">My Tasks</a>
                    </div>
                </li>
               
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Fix my Car
                    </a>
                    <div class="dropdown-menu" >
                        <a class="dropdown-item" href="fixmycar.php">Post a Request</a>
                        <a class="dropdown-item" href="javascript:void(0)">Browse Tasks</a>
                        <a class="dropdown-item" href="javascript:void(0)">My Tasks</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Wreck my Car
                    </a>
                    <div class="dropdown-menu" >
                        <a class="dropdown-item" href="wreckmycar.php">Post a Request</a>
                        <a class="dropdown-item" href="javascript:void(0)">Browse Tasks</a>
                        <a class="dropdown-item" href="javascript:void(0)">My Tasks</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Company
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="javascript:void(0)">About us</a>
                        <a class="dropdown-item" href="javascript:void(0)">Policy</a>
                        <a class="dropdown-item" href="javascript:void(0)">Director</a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav  my-lg-0 rtnav">
                <li class="nav-item search">
                    <a class="nav-link" href="javascript:void(0)"> <span class="icon-search"></span> </a>
                </li>               
                <li class="nav-item">
                    <a class="nav-link" href="signin.php"> Login  </a>
                </li>                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle redbtn" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Register
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="register.php">Customer</a>
                        <a class="dropdown-item" href="register.php">Wreckers</a>
                        <a class="dropdown-item" href="register.php">Mechanic</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle whitebtn" href="javascript:void(0)" id="cart-dropdown" data-toggle="dropdown" area-haspopup="true" area-expanded="false">
                        <span class="icon-shopping-cart-of-checkered-design icomoon"></span>Cart
                        <span class="value text-center">10</span>
                    </a>
                    <div class="dropdown-menu cart-dropdown" area-labelledby="navbardropdown">                        
                        <a class="dropdown-item d-flex justify-content-between cart-nav" href="javascript:void(0)">
                            <img src="img/data/pro01.jpg" alt="">
                            <article>
                                <span class="d-block">Product name will be here</span>
                                <span class="d-block">$200</span>
                            </article>
                        </a>

                        <a class="dropdown-item d-flex justify-content-between cart-nav" href="javascript:void(0)">
                            <img src="img/data/pro02.jpg" alt="">
                            <article>
                                <span class="d-block">Product name will be here</span>
                                <span class="d-block">$200</span>
                            </article>
                        </a>

                        <a class="dropdown-item d-flex justify-content-between cart-nav" href="javascript:void(0)">
                            <img src="img/data/pro03.jpg" alt="">
                            <article>
                                <span class="d-block">Product name will be here</span>
                                <span class="d-block">$200</span>
                            </article>
                        </a>

                        <a class="dropdown-item d-flex justify-content-between cart-nav" href="javascript:void(0)">
                            <img src="img/data/pro04.jpg" alt="">
                            <article>
                                <span class="d-block">Product name will be here</span>
                                <span class="d-block">$200</span>
                            </article>
                        </a>

                        <a class="dropdown-item d-flex justify-content-between cart-nav" href="javascript:void(0)">
                            <img src="img/data/pro05.jpg" alt="">
                            <article>
                                <span class="d-block">Product name will be here</span>
                                <span class="d-block">$200</span>
                            </article>
                        </a>  
                        
                        <a class="dropdown-item text-center border-0" href="cart.php">
                            <span>View all Cart Items</span>
                        </a>  
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>