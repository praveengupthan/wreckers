<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>About</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-3">                 
                    <!-- col -->
                    <div class="col-lg-6">
                        <img src="img/about01.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6">                       
                        <p>Wreckers, a unit of Wreckers. Ltd is country’s largest online marketplace for auto components buyers. Founded in the year 2015, the company has a huge database of genuine and aftermarket replacement parts offered from over 400 suppliers across the nation. With our aim to be “Your Spare Parts Expert”, we strive to become a pioneer in the automotive parts industry. Being an unorganized sector in the country, sometimes it becomes very difficult for the car owners to find replacement parts. Our platform offers a trusted channel for both car owners and auto parts dealers to come under one roof in order to reap the maximum benefits out of it. Customers can find the required automotive components using our online catalogue and can make their search through VIN, brand or part number.</p>
                    </div>
                    <!--/ col -->                  
                </div>
                <!--/ row --> 

                 <!-- row -->
                 <div class="row py-3">                 
                    <!-- col -->
                    <div class="col-lg-6 order-lg-last align-self-center">
                        <img src="img/about02.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6">     
                        <h4 class="h5 fred">What We Offer</h4>             
                      <ul class="list-items">
                            <li>We offer over 3.2 crores unique spare parts in the catalogue, including both Genuine and Aftermarket.</li>
                            <li>We have over 2.7 crores spare parts available for purchase with prices</li>
                            <li>We have 4.8 crores spare parts offers listed from more than 400 suppliers and manufacturers throughout the globe.</li>
                            <li>We deliver products to Indian customers from Europe, USA, Japan, China, UAE and South Korea. </li>
                            <li>We have over 3000 spare parts brands to choose from.</li>
                            <li>Customer-Friendly Return Policy</li>
                            <li>Best possible price with secure payment gateway for hassle free checkout</li>
                            <li>100% guarantee of the products to be applied to your car</li>
                      </ul>
                    </div>
                    <!--/ col -->                  
                </div>
                <!--/ row --> 

                 <!-- row -->
                 <div class="row py-3">                 
                    <!-- col -->
                    <div class="col-lg-6">
                        <img src="img/about03.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">                       
                        <h4 class="h5 fred">Our Vision</h4>
                        <p>We are guided by the Vision to make Automobile Service Industry Organised where people can find the best-suited automobile parts just with the few mouse clicks.</p>
                        <p>We are driven by the aim to organise market of spare parts in India and become the #1 online destination for everybody interested in the auto service industry. We are in a process to make the spare parts market more dynamic and transparent for everybody. </p>
                    </div>
                    <!--/ col -->                  
                </div>
                <!--/ row --> 

            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>