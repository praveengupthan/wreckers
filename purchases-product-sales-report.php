<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
              <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <!-- title -->
                    <div class="db-pagetitle">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-12">
                                <article>
                                    <h2 class="h5 fbold">Products Sales Reports</h2>                            
                                </article>
                            </div>
                            <!--/ col --> 
                        </div>
                         <!--/ row -->  
                    </div>
                    <!--/ title -->

                    <!-- body -->
                    <div class="report-body">
                        <!-- table -->
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Key Points</th>
                                    <th scope="col">Today</th>
                                    <th scope="col">This Week</th>
                                    <th scope="col">This Month</th>
                                    <th scope="col">This Year</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="fbold">Number of Cars Uploaded</td>
                                    <td>10</td>
                                    <td>25</td>
                                    <td>45</td>
                                    <td>142</td>
                                </tr>  
                                <tr>
                                    <td  class="fbold">Number of Cars Parts Uploaded</td>
                                    <td>115</td>
                                    <td>25</td>
                                    <td>45</td>
                                    <td>110</td>
                                </tr>   
                                <tr>
                                    <td  class="fbold">Number of Sales</td>
                                    <td>25</td>
                                    <td>12</td>
                                    <td>10</td>
                                    <td>35</td>
                                </tr>   
                                <tr>
                                    <td  class="fbold">Total Price Earned</td>
                                    <td>$75.00</td>
                                    <td>$15.00</td>
                                    <td>$12.00</td>
                                    <td>$125.00</td>
                                </tr>  
                                <tr>
                                    <td  class="fbold">Total Commission Paid</td>
                                    <td>$75.00</td>
                                    <td>$15.00</td>
                                    <td>$18.25</td>
                                    <td>$125.56</td>
                                </tr>     
                                <tr>
                                    <td  class="fbold">No. of Cancelled</td>
                                    <td>10</td>
                                    <td>25</td>
                                    <td>45</td>
                                    <td>142</td>
                                </tr>                     
                            </tbody>
                        </table>
                        <!--/ table -->
                    </div>

                    <!--/ body -->

                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>