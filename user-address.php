<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
                <!-- left navigation -->
                <div class="col-lg-3 ">
                    <figure class="user">
                        <img src="img/data/chairmanpic.jpg">
                        <h1 class="h5">User Name will be here</h1>
                        <p class="text-center">praveennandipati@gmail.com</p>
                    </figure>

                   <?php include 'user-dashboard-nav.php' ?>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-8 user-rightcol">
                    <!-- page title -->
                    <div class="db-pagetitle d-flex justify-content-between">
                        <article>
                            <h2 class="h5 fbold">My Addresses book</h2>                            
                        </article>     
                        <a href="javascript:void(0)" class="float-right fred" data-toggle="modal" data-target="#newaddress">+ Add New Address</a>                   
                    </div>
                    <!--/ page title -->
                    <!-- whit ebox -->
                    <div class="whitebox">
                         <!-- row -->
                         <div class="row pt-3">
                            <!-- col -->
                            <div class="col-lg-6">
                                <div class="addresscol whitebox">
                                    <p class="fgreen fbold">Default - Home</p>
                                    <p>Praveen Guptha Nandipati</p>
                                    <p>Plot No:25.1, 8-3-833/25, Phase I, Opp: ICOMM Building, Kamalapuri colony, Srinagar Colony, Hyderabad, Telangana</p>
                                    <p>Phone: 9642123254</p>                                           
                                    <p class="pt-3">
                                        <a href="javascript:void(0)"><span class="icon-edit icomoon"></span>Edit address </a>
                                        <a href="javascript:void(0)"><span class="icon-bin icomoon"></span>Delete </a>
                                    </p>
                                </div>
                            </div>
                            <!--/ col -->

                            <!-- col -->
                            <div class="col-lg-6">
                                <div class="addresscol whitebox">
                                    <p class="fblue fbold">Default - Office</p>
                                    <p>Praveen Guptha Nandipati</p>
                                    <p>Plot No:25.1, 8-3-833/25, Phase I, Opp: ICOMM Building, Kamalapuri colony, Srinagar Colony, Hyderabad, Telangana</p>
                                    <p>Phone: 9642123254</p>                                           
                                    <p class="pt-3">
                                        <a href="javascript:void(0)"><span class="icon-edit icomoon"></span>Edit address </a>
                                        <a href="javascript:void(0)"><span class="icon-bin icomoon"></span>Delete </a>
                                    </p>
                                </div>
                            </div>
                            <!--/ col -->                            
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ white box -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->


    <!--/ Add New Address -->
<div class="modal fade" id="newaddress">
    <div class="modal-dialog">
      <div class="modal-content">      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add New Delivery Address</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <form class="formreview">
                 <div class="form-group">
                    <label>Select type of Address</label>
                    <select class="form-control">
                        <option>Office</option>
                        <option>Home</option>
                        <option>Others</option>                       
                    </select>
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" placeholder="Write Your Name" class="form-control">
                </div>

                <div class="form-group">
                    <label>Area Pincode</label>
                    <input type="text" placeholder="Area Pincode" class="form-control">
                </div>

                <div class="form-group">
                    <label>Address</label>
                    <input type="text" placeholder="H.No/Street Name" class="form-control">
                </div>

                <div class="form-group">
                    <label>Landmark</label>
                    <input type="text" placeholder="Land Mark (Ex: Near Hospital)" class="form-control">
                </div>

                <div class="form-group">
                    <label>Select State</label>
                    <select class="form-control">
                        <option>Telangana</option>
                        <option>Andhra Pradesh</option>
                        <option>Tamilnadu</option>
                        <option>Kerala</option>
                        <option>Maharastra</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Select City</label>
                    <select class="form-control">
                        <option>Hyderabad</option>
                        <option>Secunderabad</option>                        
                    </select>
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text" placeholder="Enter email" class="form-control">
                </div>

                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="text" placeholder="Enter Phone Number" class="form-control">
                </div>
                <div class="form-group">                    
                    <input type="checkbox">
                    <label>Default Address</label>
                </div>
                
            </form>
        </div>        
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-success">Add Address</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>          
        </div>        
      </div>
    </div>
  </div>  
</div>
<!--/ Add New Address -->
</body>

</html>