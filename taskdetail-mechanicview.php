<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task Detail</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">        
        <!-- sub page body -->
        <div class="subpage-body pt-0">

            <?php include 'taskdetail-nav.php' ?>
            <!-- container -->
            <div class="container taskdetail-container">
                <!-- row -->
                <div class="row">
                    <!-- top search filters -->
                    <div class="col-lg-12">
                                             
                
                <!-- row left and right -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-lg-4">
                        <div class="left-task"> 
                            <!-- request col -->
                            <div class="request-col mb-2 requestactive">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                            <!-- request col -->
                            <div class="request-col mb-2">
                                <h2 class="d-flex justify-content-between">
                                    <span>Request of Bumper</span>
                                    <span class="fbold fred price">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                            <!-- request col -->
                            <div class="request-col mb-2 expired-request">
                                <h2 class="d-flex justify-content-between">
                                    <span>Request of Bumper</span>
                                    <span class="fbold fred price">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fred align-self-center">Closed</span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> 10-01-2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                            <!-- request col -->
                            <div class="request-col mb-2 expired-request" disabled>
                                <h2 class="d-flex justify-content-between">
                                    <span>Request of Bumper</span>
                                    <span class="fbold fred price">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fred align-self-center">Expired</span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> 10-01-2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                             <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                             <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                             <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                            <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                             <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                             <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->
                           
                        </div>
                    </div>
                    <!--/ left col -->

                    <!-- right col -->
                    <div class="col-lg-8">
                        <!-- task detail right -->
                       <div class="task-right task-detail">
                          <!-- white block -->
                          <div class="whitebox">
                              <!-- steps row -->
                              <div class="row">
                                  <!-- col 8 -->
                                  <div class="col-lg-8 align-self-center">
                                    <!-- progress bar -->
                                    <ul class="progressbar">
                                        <li class="active">Opened</li>
                                        <li class="active">Assigned</li>
                                        <li>Closed</li>                                            
                                    </ul>
                                    <!--/ progress bar -->
                                    <div class="d-flex p-3 mt-3">
                                        <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                        <article class="px-4 align-self-center">
                                            <h3 class="h6 mb-0 pb-0">
                                                <a href="javascript:void(0)">User Name will be here</a>
                                            </h3>
                                            <ul class="review-rating nav">
                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                            </ul>
                                            <p class="fgray pb-0">Member Since 1925</p>
                                        </article>
                                    </div>
                                  </div>
                                  <!--/ col 8-->
                                  <!-- col 4 -->
                                  <div class="col-lg-4 text-center align-self-center">
                                    <div class="budgetblock">
                                        <h2 class="h6 fgray text-uppercase"><span class="icon-unlock-alt"></span> Payment Secured <span class="fred fbold h2 d-block">$45</span></h2>

                                        <!--table for specifications -->
                                        <table class="table table-borderless">
                                            <tr>
                                                <td>Service Fee</td>
                                                <td>:</td>
                                                <td>-$1.00</td>
                                            </tr>
                                            <tr>
                                                <td>GST</td>
                                                <td>:</td>
                                                <td>-$0.10</td>
                                            </tr>
                                            <tr>
                                                <td>You'll Receive</td>
                                                <td>:</td>
                                                <td><span class="fbold fblue">$3.10</span></td>
                                            </tr>

                                        </table>
                                        <!--/ table for specifications -->

                                        <a data-toggle="modal" data-target="#increase-price" href="javavscript:void(0)" class="d-block text-center w-100 pb-2"><small><span class="icon-plus-circle"></span> Increase Price</small></a>
                                        <a data-toggle="modal" data-target="#carservice-report" class="redbtn btnxl d-inline-block" href="javascript:void(0)">Request Payment </a>
                                        <a data-toggle="modal" data-target="#request-sent" href="javascript:void(0)">Sent Request</a>
                                    </div>
                                  </div>
                                  <!--/ col 4 -->
                              </div>
                              <!--/ steps row -->
                             

                              <!-- social row -->
                              <div class="row social-row py-3 border-top">
                                <!-- col 6-->
                                <div class="col-lg-6 ">
                                    <div class="d-flex justify-content-center">
                                        <p class="pr-2 align-self-center pb-0">Share Now</p>                                       
                                        <a href="javascript:void(0)" target="_blank"><span class="icon-facebook icomoon"></span></a>
                                        <a href="javascript:void(0)" target="_blank"><span class="icon-twitter icomoon"></span></a>
                                        <a href="javascript:void(0)" target="_blank"><span class="icon-linkedin icomoon"></span></a>
                                        <a href="javascript:void(0)" target="_blank"><span class="icon-copy icomoon"></span></a>
                                       
                                    </div>
                                </div>
                                <!--/ col 6 -->                               
                                <!-- col 4-->
                                <div class="col-lg-2">
                                    <div class="d-flex justify-content-center">
                                        <p class="pr-2 align-self-center pb-0">Follow</p>                                        
                                        <a class="align-self-center pb-0" href="javascript:void(0)" target="_blank"><span class="icon-heart icomoon"></span></a>        
                                    </div>
                                </div>
                                <!--/ col 4 -->

                                <!-- col 4-->
                                <div class="col-lg-4 text-center">
                                    <a class="fmed" href="javascript:void(0)">Post a Smilar Task</a>
                                </div>
                                <!--/ col 4-->
                              </div>
                              <!--/ social row -->
                          </div>
                          <!--/ white block -->

                          <!-- white block -->
                          <div class="whitebox">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <h4 class="h4 pb-0 fbold">Need a Bumper</h4>
                                        <p><small class="small">Location Name, Mobile</small></p>
                                    </div>

                                    <span class="fgray">Due date: ASAP</span>
                                </div>
                                <div class="py-3">
                                    <h5 class=" h6 fred fbold">Description</h5>
                                    <p class="pb-0">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Velit sapiente nihil possimus laudantium soluta vero molestiae nisi et inventore nobis ipsa </p>
                                    <p><a href="javascript:void(0)" class="fgray">more > </a></p>
                                </div>

                                <!-- slick slider -->
                                <div class="task-slick custom-slick toptaskslick">
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part01.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part02.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part03.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part04.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part05.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part06.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                </div>
                                <!--/ slick slider -->                               
                          </div>
                          <!--/ white block -->

                          <!-- questions block -->
                          <div class="whitebox qandans cust-tab">

                                <!-- tab-->
                                <div class="parentHorizontalTab">
                                    <ul class="resp-tabs-list hor_1 nav justify-content-center">
                                        <li>Offers</li>
                                        <li>Questions (20)</li>
                                        <li>Messages</li>
                                    </ul>
                                    <!-- tab container -->
                                    <div class="resp-tabs-container hor_1">
                                        <!-- offers -->
                                        <div>
                                            <h4 class="h5">Offers (20)</h4>
                                            <!-- chat message -->
                                            <div class="row justify-content-end py-2 border-bottom">
                                                <div class="col-lg-12">
                                                    <div class="d-flex p-3">
                                                        <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                        <article class="px-4 align-self-center">
                                                            <h3 class="h6 mb-0 pb-0">
                                                                <a href="javascript:void(0)">User Name will be here</a>
                                                            </h3>
                                                            <ul class="review-rating nav">
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                            </ul>
                                                            <p class="fgray pb-0">Task Completed (20)</p>
                                                        </article>
                                                    </div>
                                                    <p class="mb-0 pb-0"><span class="fbold">Description: </span>Lorem ipsum dolor sit amet consectetur adipisicing elit Lorem, ipsum.sit amet consectetur adipisicing elit. </p>

                                                    <p><a href="javascript:void(0)" class="fgray">More ></a></p>

                                                     <!-- slick slider -->
                                                    <div class="task-slick custom-slick">
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part01.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part02.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part03.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part04.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part05.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part06.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                    </div>
                                                    <!--/ slick slider -->

                                                    <p class="d-flex justify-content-between">
                                                        <span class="fgray">10 Min Ago</span>
                                                        <span><a href="javascript:void(0)" id="replybtn">Reply</a></span>
                                                    </p>

                                                    <!-- write message -->
                                                    <form id="showreply">
                                                        <div class="form-group">
                                                            <label>Write Message</label>
                                                            <textarea class="form-control mb-2" style="height:100px;"></textarea>
                                                            <input type="submit" class="btn redbtn" value="Submit">
                                                        </div>
                                                    </form>
                                                    <!--/ write mesage -->
                                                </div>
                                            </div>
                                        <!--/ chat message -->
                                        </div>
                                        <!--/ offers -->

                                        <!-- questioins -->
                                        <div>
                                        <h4 class="h5">Questions (20)</h4>

                                            <!-- question -->
                                            <div class="question-block py-2 border-bottom">
                                                <h6 class="h6 fgray">Q: Question name by writer will be here</h6>
                                                <p>A: Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veritatis est dolorem minus aperiam ad possimus praesentium ullam velit tempore voluptatibus.</p>
                                                <div class="row">
                                                    <!-- col 8-->
                                                    <div class="col-lg-8">
                                                        <div class="d-flex p-3">
                                                            <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                            <article class="px-4 align-self-center">
                                                                <h3 class="h6 mb-0 pb-0">
                                                                    <a href="javascript:void(0)">User Name will be here</a>
                                                                </h3>
                                                                <ul class="review-rating nav">
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <!--/ col 8 -->
                                                    <!-- col 4-->
                                                    <div class="col-lg-4 text-right align-self-center">
                                                        <p class="fgray pb-0 mb-0">Posted on 2 Oct 2019</p>
                                                    </div>
                                                    <!--/ col 4-->
                                                </div>
                                            </div>
                                            <!-- question -->

                                            <!-- question -->
                                            <div class="question-block py-2 border-bottom">
                                                <h6 class="h6 fgray">Q: Question name by writer will be here</h6>
                                                <p>A: Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veritatis est dolorem minus aperiam ad possimus praesentium ullam velit tempore voluptatibus.</p>
                                                <div class="row">
                                                    <!-- col 8-->
                                                    <div class="col-lg-8">
                                                        <div class="d-flex p-3">
                                                            <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                            <article class="px-4 align-self-center">
                                                                <h3 class="h6 mb-0 pb-0">
                                                                    <a href="javascript:void(0)">User Name will be here</a>
                                                                </h3>
                                                                <ul class="review-rating nav">
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <!--/ col 8 -->
                                                    <!-- col 4-->
                                                    <div class="col-lg-4 text-right align-self-center">
                                                        <p class="fgray pb-0 mb-0">Posted on 2 Oct 2019</p>
                                                    </div>
                                                    <!--/ col 4-->
                                                </div>
                                            </div>
                                            <!-- question -->

                                            <!-- question -->
                                            <div class="question-block py-2 border-bottom">
                                                <h6 class="h6 fgray">Q: Question name by writer will be here</h6>
                                                <p>A: Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veritatis est dolorem minus aperiam ad possimus praesentium ullam velit tempore voluptatibus.</p>
                                                <div class="row">
                                                    <!-- col 8-->
                                                    <div class="col-lg-8">
                                                        <div class="d-flex p-3">
                                                            <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                            <article class="px-4 align-self-center">
                                                                <h3 class="h6 mb-0 pb-0">
                                                                    <a href="javascript:void(0)">User Name will be here</a>
                                                                </h3>
                                                                <ul class="review-rating nav">
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <!--/ col 8 -->
                                                    <!-- col 4-->
                                                    <div class="col-lg-4 text-right align-self-center">
                                                        <p class="fgray pb-0 mb-0">Posted on 2 Oct 2019</p>
                                                    </div>
                                                    <!--/ col 4-->
                                                </div>
                                            </div>
                                            <!-- question -->

                                            <!-- question -->
                                            <div class="question-block py-2 border-bottom">
                                                <h6 class="h6 fgray">Q: Question name by writer will be here</h6>
                                                <p>A: Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veritatis est dolorem minus aperiam ad possimus praesentium ullam velit tempore voluptatibus.</p>
                                                <div class="row">
                                                    <!-- col 8-->
                                                    <div class="col-lg-8">
                                                        <div class="d-flex p-3">
                                                            <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                            <article class="px-4 align-self-center">
                                                                <h3 class="h6 mb-0 pb-0">
                                                                    <a href="javascript:void(0)">User Name will be here</a>
                                                                </h3>
                                                                <ul class="review-rating nav">
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <!--/ col 8 -->
                                                    <!-- col 4-->
                                                    <div class="col-lg-4 text-right align-self-center">
                                                        <p class="fgray pb-0 mb-0">Posted on 2 Oct 2019</p>
                                                    </div>
                                                    <!--/ col 4-->
                                                </div>
                                            </div>
                                            <!-- question -->

                                            <!-- question -->
                                            <div class="question-block py-2">
                                                <h6 class="h6 fgray">Q: Question name by writer will be here</h6>
                                                <p>A: Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veritatis est dolorem minus aperiam ad possimus praesentium ullam velit tempore voluptatibus.</p>
                                                <div class="row">
                                                    <!-- col 8-->
                                                    <div class="col-lg-8">
                                                        <div class="d-flex p-3">
                                                            <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                            <article class="px-4 align-self-center">
                                                                <h3 class="h6 mb-0 pb-0">
                                                                    <a href="javascript:void(0)">User Name will be here</a>
                                                                </h3>
                                                                <ul class="review-rating nav">
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <!--/ col 8 -->
                                                    <!-- col 4-->
                                                    <div class="col-lg-4 text-right align-self-center">
                                                        <p class="fgray pb-0 mb-0">Posted on 2 Oct 2019</p>
                                                    </div>
                                                    <!--/ col 4-->
                                                </div>
                                            </div>
                                            <!-- question -->
                                        </div>
                                        <!--/ questions -->
                                        <!-- messages -->
                                        <div>
                                            <p>Private Messages here are only seen by you and Heater W</p>

                                            <!-- chat section -->
                                            <div class="chatsection">
                                                <!-- right chat -->
                                                <div class="row justify-content-end">
                                                     <div class="col-lg-2 text-right align-self-center">
                                                         <a href="javascript:void(0)"><img class="thumbimg" src="img/data/custimg01.jpg" alt=""></a>
                                                     </div>
                                                     <div class="col-lg-10 ">
                                                        <div class="graychat">
                                                            <p class="d-flex justify-content-between p-2 border-bottom">
                                                                <span class="fbold">Chaitanya (Poster)</span>
                                                                <span class="small">5 Nov 2019 - 21:57</span>      
                                                            </p>
                                                            <p class="p-2">The Address is 123 fordview cres bell post hill</p>
                                                        </div>
                                                     </div>
                                                </div>
                                                <!--/ right chat -->
                                                <!-- left chat -->
                                                <div class="row justify-content-end">
                                                     <div class="col-lg-2 align-self-center order-last">
                                                         <a href="javascript:void(0)"><img class="thumbimg" src="img/data/custimg01.jpg" alt=""></a>
                                                     </div>
                                                     <div class="col-lg-10 ">
                                                        <div class="whitechat">
                                                            <p class="d-flex justify-content-between p-2 border-bottom">
                                                                <span class="fbold">Heater W</span>
                                                                <span class="small">5 Nov 2019 - 21:57</span>      
                                                            </p>
                                                            <p class="p-2 text-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium voluptates delectus eligendi excepturi molestias magnam, nisi rem assumenda adipisci. Aut beatae vel omnis quis corporis!</p>
                                                        </div>
                                                     </div>
                                                </div>
                                                <!--/ left chat -->
                                                <!-- right chat -->
                                                <div class="row justify-content-end">
                                                     <div class="col-lg-2 text-right align-self-center">
                                                         <a href="javascript:void(0)"><img class="thumbimg" src="img/data/custimg01.jpg" alt=""></a>
                                                     </div>
                                                     <div class="col-lg-10 ">
                                                        <div class="graychat">
                                                            <p class="d-flex justify-content-between p-2 border-bottom">
                                                                <span class="fbold">Chaitanya (Poster)</span>
                                                                <span class="small">5 Nov 2019 - 21:57</span>      
                                                            </p>
                                                            <p class="p-2">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, porro!</p>
                                                        </div>
                                                     </div>
                                                </div>
                                                <!--/ right chat -->
                                                <!-- left chat -->
                                                <div class="row justify-content-end">
                                                     <div class="col-lg-2 align-self-center order-last">
                                                         <a href="javascript:void(0)"><img class="thumbimg" src="img/data/custimg01.jpg" alt=""></a>
                                                     </div>
                                                     <div class="col-lg-10 ">
                                                        <div class="whitechat">
                                                            <p class="d-flex justify-content-between p-2 border-bottom">
                                                                <span class="fbold">Heater W</span>
                                                                <span class="small">5 Nov 2019 - 21:57</span>      
                                                            </p>
                                                            <p class="p-2 text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi incidunt in veritatis porro necessitatibus, deleniti cupiditate consequatur nulla aspernatur temporibus blanditiis molestias quis magni ab dolor quam voluptatem aliquid praesentium!</p>
                                                        </div>
                                                     </div>
                                                </div>
                                                <!--/ left chat -->
                                                <!-- right chat -->
                                                <div class="row justify-content-end">
                                                     <div class="col-lg-2 text-right align-self-center">
                                                         <a href="javascript:void(0)"><img class="thumbimg" src="img/data/custimg01.jpg" alt=""></a>
                                                     </div>
                                                     <div class="col-lg-10 ">
                                                        <div class="graychat">
                                                            <p class="d-flex justify-content-between p-2 border-bottom">
                                                                <span class="fbold">Chaitanya (Poster)</span>
                                                                <span class="small">5 Nov 2019 - 21:57</span>      
                                                            </p>
                                                            <p class="p-2">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Beatae, porro!</p>
                                                        </div>
                                                     </div>
                                                </div>
                                                <!--/ right chat -->
                                                <!-- left chat -->
                                                <div class="row justify-content-end">
                                                     <div class="col-lg-2 align-self-center order-last">
                                                         <a href="javascript:void(0)"><img class="thumbimg" src="img/data/custimg01.jpg" alt=""></a>
                                                     </div>
                                                     <div class="col-lg-10 ">
                                                        <div class="whitechat">
                                                            <p class="d-flex justify-content-between p-2 border-bottom">
                                                                <span class="fbold">Heater W</span>
                                                                <span class="small">5 Nov 2019 - 21:57</span>      
                                                            </p>
                                                            <p class="p-2 text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi incidunt in veritatis porro necessitatibus, deleniti cupiditate consequatur nulla aspernatur temporibus blanditiis molestias quis magni ab dolor quam voluptatem aliquid praesentium!</p>
                                                        </div>
                                                     </div>
                                                </div>
                                                <!--/ left chat -->

                                                <!-- chat message-->
                                                <div class="textarea-chat">
                                                     <div class="row">
                                                            <div class="col-lg-2 text-right">
                                                                <a href="javascript:void(0)"><img class="thumbimg" src="img/data/custimg01.jpg" alt=""></a>
                                                            </div>
                                                            <div class="col-lg-10">
                                                                <div class="form-group">  
                                                                <div class="input-group position-relative">
                                                                    <textarea class="form-control mb-2" style="height:100px;" placeholder="Send W Heater Private message"></textarea>
                                                                    <!-- attachment -->
                                                                    <a class="attach" href="javascript:void(0)"><span class="icon-upload icomoon"></span></a>
                                                                    <!--/ attachment -->
                                                                </div>
                                                                <input type="submit" class="btn redbtn" value="Send Private Message">
                                                            </div>
                                                         </div>
                                                     </div>
                                                </div>
                                                <!--/ chat message -->
                                            </div>
                                            <!--/ chat section -->
                                        </div>
                                        <!--/ messages -->
                                    </div>
                                    <!--/ tab container -->
                                </div>
                                <!--/ tab -->                               
                          </div>
                          <!--/ quetions block-->                       

                       </div>
                       <!-- task detail right -->
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row left and right -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
    

    <!-- Increqase price -->
<!-- Modal -->
<div class="modal fade" id="increase-price">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title h5">Increase Price</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
        
        <p>Need more done or task more complicated than you first thought? This is how you increase the task price.  And just so you know, the price can only be increased 3 timers (to a max of %500 overall) </p>
        
       <div class="w-75">
            <h4 class="h5">How much would like to add?</h4>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">$</span>
                </div>
                <input type="text" class="form-control" placeholder="Enter Amount" aria-describedby="basic-addon1">
            </div>
            <p>Let Emily D. KNow why</p>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="eg: The task is going to take longer because">
            </div>
       </div>

      </div>
      <div class="modal-footer">        
        <button type="button" class="redbtn w-100">Increase</button>
      </div>
    </div>
  </div>
</div>
<!--/ increase price -->

<!-- request payment sent modal -->
<!-- Modal -->
<div class="modal fade" id="request-sent">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title h5">Your Request has now  been sent</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <h2 class="text-center"><span style="font-size:100px;" class="icon-list-alt h1"></span></h2>       
        
        
        <p class="text-center">Great! You've marked this task as complete and we've notified Mounika C. to release payment</p>
        <p class="text-center"><span class="fbold">Please note:</span> Once payment is released by Mounika it will take 2-5 business days for payment to reach your account</p>
      </div>
      <div class="modal-footer">        
        <button type="button" class="redbtn w-100">Close</button>
      </div>
    </div>
  </div>
</div>
<!--/ request payment sent modal -->

<!-- request payment modal -->
<!-- Modal -->
<div class="modal fade" id="request-payment">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title h5">Request Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!--div-->
        <div class="d-flex justify-content-between">
            <!-- user div -->
            <div class="d-flex p-3 mt-3 w-75 align-self-center">
                <img class="thumbimg" src="img/data/chairmanpic.jpg"/>
                <article class="px-4 align-self-center">
                    <h3 class="h6 mb-0 pb-0">
                        <a href="javascript:void(0)">User Name will be here</a>
                    </h3>                
                    <p class="fgray pb-0">Member Since 1925</p>
                </article>
            </div>
            <!--/ user div -->
            <div class="text-center align-self-center">
                <p>Total</p>
                <h2 class="h2 fbold">$45.00</h2>
            </div>
        </div>
        <!--/ div -->
        <!--table for specifications -->
        <div class="budgetblock">
            <table class="table table-borderless w-75 mx-auto">
                <tr>
                    <td>Service Fee</td>
                    <td>:</td>
                    <td>-$1.00</td>
                </tr>
                <tr>
                    <td>GST</td>
                    <td>:</td>
                    <td>-$0.10</td>
                </tr>
                <tr>
                    <td>You'll Receive</td>
                    <td>:</td>
                    <td><span class="fbold fblue h4">$3.10</span></td>
                </tr>
            </table>
        </div>
        <!--/ table for specifications -->
        <small class="small">Service fee includes insurance and transaction costs.</small>
        <p >To be deposited into account ending in 8866 <a class="fgray" href="javascript:void(0)">Change</a></p>
        <p>You've requested payment from Mounika form this task.  They will be notified to release payment</p>
      </div>
      <div class="modal-footer">        
        <button type="button" class="redbtn w-100">Request Payment</button>
      </div>
    </div>
  </div>
</div>
<!--/ request payment modal -->


<!-- Car Service Report -->
<!-- Modal -->
<div class="modal fade" id="carservice-report">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title h5">Car Service Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
        <div class="form-group">
            <label>Enter VIN Number</label>
            <div class="input-group">
                <input type="text" placeholder="VIN Number" class="form-control">
            </div>
        </div>
        <!--table for specifications -->
        <div class="budgetblock">
            <table class="table table-borderless w-75 mx-auto">
                <tr>
                    <td>Car Make</td>
                    <td>:</td>
                    <td>Car Make Name</td>
                </tr>
                <tr>
                    <td>Model & year</td>
                    <td>:</td>
                    <td>VX & 2019</td>
                </tr>
                <tr>
                    <td>Engine Type</td>
                    <td>:</td>
                    <td>Diesel</td>
                </tr>
                <tr>
                    <td>No. of Doors</td>
                    <td>:</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>Colour</td>
                    <td>:</td>
                    <td>Black</td>
                </tr>                
            </table>            
        </div>
        <!--/ table for specifications -->
        <div class="form-group">
            <label>Service Performed by</label>
            <div class="input-group">
                <input type="text" placeholder="Mechanic Name" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label>Odomel</label>
            <input type="text" placeholder="Odomel" class="form-control">
        </div>
        <div class="form-group">
            <label>Date of Service:</label>
            <input type="text" placeholder="Date of Service" class="form-control">
        </div>

        <div class="form-group">
            <label>Service Performed Description</label>
            <div class="input-group">
                <textarea class="form-control" placeholder="Enter Description" style="height:100px;"></textarea>
            </div>
        </div>
        <div>
            <p>Total Amount Spent: $250.00</p>
            <div class="form-group">
                <label>Next Service Due Date</label>
                <input type="text" placeholder="Due Date" class="form-control">
            </div>
            
        </div>
        <div class="form-group">
            <label>Things due for the Next Service</label>
            <div class="input-group">
                <textarea class="form-control" placeholder="Things for next Service" style="height:100px;"></textarea>
            </div>
        </div>       
       
      </div>
      <div class="modal-footer">        
        <button id="requestpaymentbtn" type="button" class="redbtn w-100">Request Payment</button>
      </div>
    </div>
  </div>
</div>
<!--/ Car Service Report -->


<script>
//set button id on click to hide first modal
$("#requestpaymentbtn").on("click", function(){
    $("#carservice-report").modal('hide');
});

$("#requestpaymentbtn").on("click", function(){
    $("#request-payment").modal('show');
});
</script>

</body>
</html>