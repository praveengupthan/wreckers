<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fix My Car</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Fix My Car</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
            <!-- steps -->
            <div class="steps-wreckers">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6">
                        <!-- form -->
                        <form action="">                            
                            <!-- div id wizard-->
                            <div id="wizard">
                                <!-- SECTION 1 -->
                                <h4></h4>
                                <section>
                                    <h5>Part Details</h5>
                                    <div class="form-group">
                                        <label class="label">Enter Your Part Title <span>*</span></label>
                                        <input type="text" class="form-control pt-0" placeholder="Ex: Need my bumper Fixed">
                                    </div>
                                    <div class="form-group">
                                        <label class="label">Describe your Part <span>*</span></label>
                                        <textarea style="height:100px;" class="form-control" placeholder="(Minimum 25 Characters)"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Upload Images  </label>
                                        <div class="custom-file-upload float-left mr-2">
                                            <label for="file-upload" class="custom-file-upload1">
                                                <i class="fa fa-cloud-upload"></i> Upload Image
                                            </label>
                                            <input id="file-upload" type="file" multiple/>
                                        </div>                                       
                                    </div>                                 
                                </section>

                                <!--/ SECTION 1 -->
                                
                                <!-- SECTION 2 -->
                                <h4></h4>
                                <section>
                                    <h5>Mobile Service</h5>
                                    <div class="form-group">
                                        <p class="pb-0">Do you want Mobile Service?</p>
                                        <div class="custom-control custom-radio d-inline-block ">
                                            <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio2">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio d-inline-block ml-3">
                                            <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio1">No</label>
                                        </div>                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="label">What is your Location  <span>*</span></label>
                                        <input type="text" class="form-control pt-0" placeholder="Enter your Pincode / Postal Code">
                                    </div>
                                    <div class="form-group">
                                        <p class="pb-0">When do you need it</p>
                                        <div class="custom-control custom-radio d-inline-block">
                                            <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio3">ASAP</label>
                                        </div>
                                        <div class="custom-control custom-radio d-inline-block ml-3">
                                            <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio4">Before</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="label">Select Date  <span>*</span></label>
                                        <input type="text" class="form-control pt-0" id="datepicker"  placeholder="Select Date">
                                    </div>	
                                </section>
                                <!-- / SECTION 2-->

                                <!-- SECTION 3 -->
                                <h4></h4>
                                <section>
                                    <h5>Budget</h5>
                                    <div class="form-group">
                                        <p>What is your Budget</p>
                                        <p class="pb-0">Do you want us to find a Cheap spare part to fix your car?</p>
                                        <div class="custom-control custom-radio d-inline-block">
                                            <input type="radio" id="customRadio6" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio6">YES</label>
                                        </div>
                                        <div class="custom-control custom-radio d-inline-block ml-3">
                                            <input type="radio" id="customRadio5" name="customRadio" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio5">NO</label>
                                        </div>
                                        
                                        <div class="form-group mt-3">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ex:20">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">.00</span>
                                                </div>
                                            </div>
                                         </div>
                                    </div>  

                                    <p class="hightlate p-2">Get 10% off on your part by buying it through us</p>                                    
                                   
                                </section>
                                <!-- Section 3-->

                                 <!-- SECTION 4 -->
                                 <h4></h4>
                                <section>   
                                    <h5>Part Details</h5>                                
                                    <div class="form-group">
                                        <label class="label">Enter your title  <span>*</span></label>
                                        <input type="text" class="form-control pt-0" placeholder="Ex: Need Bumper">
                                        <small class="small">(Minimum 10 Characters)</small>
                                    </div>
                                        <div class="form-group">
                                        <label class="label">Describe the Service <span>*</span></label>
                                        <textarea style="height:100px;" class="form-control" placeholder="(Minimum 25 Characters)"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label class="d-block">Upload Images  </label>
                                        <div class="custom-file-upload mr-2">
                                            <label for="file-upload" class="custom-file-upload1">
                                                <i class="fa fa-cloud-upload"></i> Upload Image
                                            </label>
                                            <input id="file-upload" type="file" multiple/>
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <p class="hightlate p-2">Your post will be expired in 7 days</p>
                                    </div>
                                   
                                </section>
                                <!-- Section 4-->
                            </div>
                            <!--/ div id wizard -->
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ col-->
                </div>
                <!--/ row -->
            </div>
            <!-- /steps -->
                <form>
                    <!-- row -->
                    <div class="row justify-content-around">
                        

                        <!-- right col 6 -->
                        <div class="col-lg-5">
                            
                            
                        </div>
                        <!--/ right col 6 -->
                    </div>
                    <!--/ row -->
                </form>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>