<header class="fixed-top">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bars icomoon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav  leftnav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">Shop</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Part Request
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="javascript:void(0)">Post a Request</a>
                        <a class="dropdown-item" href="javascript:void(0)">Browse Tasks</a>
                        <a class="dropdown-item" href="javascript:void(0)">My Tasks</a>
                    </div>
                </li>
               
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Fix my Car
                    </a>
                    <div class="dropdown-menu" >
                        <a class="dropdown-item" href="javascript:void(0)">Post a Request</a>
                        <a class="dropdown-item" href="javascript:void(0)">Browse Tasks</a>
                        <a class="dropdown-item" href="javascript:void(0)">My Tasks</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Wreck my Car
                    </a>
                    <div class="dropdown-menu" >
                        <a class="dropdown-item" href="javascript:void(0)">Post a Request</a>
                        <a class="dropdown-item" href="javascript:void(0)">Browse Tasks</a>
                        <a class="dropdown-item" href="javascript:void(0)">My Tasks</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Company
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="javascript:void(0)">About us</a>
                        <a class="dropdown-item" href="javascript:void(0)">Policy</a>
                        <a class="dropdown-item" href="javascript:void(0)">Director</a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav  my-lg-0 rtnav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle whitebtn" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="icon-search"></span> Search
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                       <!-- search block -->
                       <div class="search-header">
                            <form>
                                <div class="d-flex">
                                    <input type="text"  placeholder="Enter Part Details">
                                    <input type="submit" value="Search" class="redbtn">
                                </div>
                            </form>
                            <a href="javascript:void(0)" class="fred">Advance Search</a>
                       </div>
                       <!--/ search block -->
                       
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link whitebtn" href="javascript:void(0)">
                        <span class="icon-shopping-cart-of-checkered-design icomoon"></span>
                        <span class="value text-center">10</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle whitebtn" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="icon-bell-o"></span>
                        <span class="value text-center">10</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="javascript:void(0)">Recent</a>
                        <a class="dropdown-item" href="javascript:void(0)">Mechanic Sent a Quote</a>
                        <a class="dropdown-item" href="javascript:void(0)">XX Item is on the way</a>
                       
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Jack Prime
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="dashboard2.php">Dashboard</a>
                        <a class="dropdown-item" href="register.php">FAQs</a>  
                        <a class="dropdown-item" href="register.php">Switch Account</a>
                        <a class="dropdown-item" href="register.php">Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>