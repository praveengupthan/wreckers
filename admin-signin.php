<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!--main -->
    <main>
        <!-- sign template -->
        <div class="sign-section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-5">
                        <h1 class="title-login">ADMIN SIGNIN</h1>
                       
                        <div class="signcol">
                            <form>
                                <div class="form-group">
                                    <label>User Name</label>
                                    <input type="text" class="form-control" placeholder="Username / Email">
                                    <span class="icon-user-silhouette icomoon"></span>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Enter Password">
                                    <span class="icon-unlock icomoon"></span>
                                </div>                                
                                <input type="submit" value="SIGNIN" class="w-100 redbtn">
                            </form>                                     
                        </div>
                       
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sign template -->
    </main>
    <!--/ main -->

    <!-- script files -->
    <?php include 'footer-scripts.php' ?>

</body>

</html>