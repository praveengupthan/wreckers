<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profile </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body profile">          
           <div class="profile-coverbg"></div>
            <!-- profile header -->
            <div class="profile-header">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- left col 4 -->
                        <div class="col-lg-4">
                            <div class="profile-left">
                                <img class="profile-pic" src="img/db-profile.jpg" alt="">

                                <div class="details">
                                    <p>Member since: <span>28-12-2019</span></p>
                                    <p>Online status: <span>Online 2 hours ago</span></p>
                                    <p>Location: <span>Bell Post Hill, Victoria</span></p>
                                    <p>Membership: <span>Gold Membership</span></p>
                                    <p>BADGES: <span>MECHANIC BADGE</span></p>
                                </div>
                            </div>
                        </div>
                        <!--/ left col 4-->
                        <!-- right col 8-->
                        <div class="col-lg-8 profile-right">
                            <article>
                                <h2 class="text-uppercase">Chaitanya Bade</h2>
                                <h3 class="h4 flight">Mechanic</h3>
                            </article>
                            <!-- profile content -->
                            <div class="profile-content cust-tab">
                                    <!-- tab articles -->
                                <div class="parentHorizontalTab">
                                    <ul class="resp-tabs-list hor_1 nav justify-content-center">
                                        <li>Timeline</li>
                                        <li>Completed Tasks as a Mechanic</li>
                                        <li>Current Tasks as a Customer</li>                                   
                                    </ul>
                                    <!-- responsive tab container -->
                                    <div class="resp-tabs-container hor_1">
                                        <!-- time line -->
                                        <div>
                                            <p class="text-right">
                                                <a data-toggle="modal" data-target="#edit-profile" href="javascript:void(0)"><span class="icon-edit"></span> Edit Profile</a>
                                            </p>
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td style="width:150px"><span class="fbold">About me</span></td>
                                                    <td>
                                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui commodi nesciunt illum tempore dolorum excepturi harum, explicabo iste ducimus deserunt ea, eligendi eaque, doloremque aspernatur non est necessitatibus quidem officia reprehenderit alias soluta amet. Nobis quos ducimus autem totam quam et dolorum repellendus mollitia! Reiciendis corrupti similique quibusdam dicta earum?</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="fbold">Date of Birth</span></td>
                                                    <td>
                                                        <p>04th August 1981</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="fbold">Portfolio</span></td>
                                                    <td>
                                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur quae porro consequuntur reiciendis ut doloribus, ipsa distinctio placeat itaque ipsam.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="fbold">Skills</span></td>
                                                    <td>
                                                        <p>
                                                            <span class="tag">Photoshop</span>
                                                            <span class="tag">HTML</span>
                                                            <span class="tag">Web Development</span>
                                                            <span class="tag">Date base</span>
                                                            <span class="tag">Human Resource Management</span>
                                                            <span class="tag">Recruting</span>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--/ time line -->
                                        <!-- Completed Tasks a Mechanic-->
                                        <div>
                                           
                                        <!-- request col -->
                                        <div class="request-col mb-2">
                                            <h3 class="d-flex justify-content-between">
                                                <span>Request of Bumper</span>
                                                <span class="fbold fred price">$ 45</span>
                                            </h3>
                                            <div class="row address-block">
                                                <div class="col-lg-8 align-self-center">
                                                    <p><span class="fbold">Mobile</span></p>
                                                </div>
                                                <div class="col-lg-4 align-self-center">
                                                    <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                            <h3 class="d-flex justify-content-between">
                                                <span class="fgreen align-self-center">Closed </span>
                                               
                                            </h3>
                                        </div>
                                        <!--/ request col -->

                                         <!-- request col -->
                                         <div class="request-col mb-2">
                                            <h3 class="d-flex justify-content-between">
                                                <span>Request of Bumper</span>
                                                <span class="fbold fred price">$ 45</span>
                                            </h3>
                                            <div class="row address-block">
                                                <div class="col-lg-8 align-self-center">
                                                    <p><span class="fbold">Mobile</span></p>
                                                </div>
                                                <div class="col-lg-4 align-self-center">
                                                    <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                            <h3 class="d-flex justify-content-between">
                                            <span class="fgreen align-self-center">Closed </span>
                                                <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                            </h3>
                                        </div>
                                        <!--/ request col -->

                                         <!-- request col -->
                                         <div class="request-col mb-2">
                                            <h3 class="d-flex justify-content-between">
                                                <span>Request of Bumper</span>
                                                <span class="fbold fred price">$ 45</span>
                                            </h3>
                                            <div class="row address-block">
                                                <div class="col-lg-8 align-self-center">
                                                    <p><span class="fbold">Mobile</span></p>
                                                </div>
                                                <div class="col-lg-4 align-self-center">
                                                    <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                            <h3 class="d-flex justify-content-between">
                                            <span class="fgreen align-self-center">Closed </span>
                                                <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                            </h3>
                                        </div>
                                        <!--/ request col -->

                                         <!-- request col -->
                                         <div class="request-col mb-2">
                                            <h3 class="d-flex justify-content-between">
                                                <span>Request of Bumper</span>
                                                <span class="fbold fred price">$ 45</span>
                                            </h3>
                                            <div class="row address-block">
                                                <div class="col-lg-8 align-self-center">
                                                    <p><span class="fbold">Mobile</span></p>
                                                </div>
                                                <div class="col-lg-4 align-self-center">
                                                    <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                            <h3 class="d-flex justify-content-between">
                                            <span class="fgreen align-self-center">Closed </span>
                                                <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                            </h3>
                                        </div>
                                        <!--/ request col -->

                                         <!-- request col -->
                                         <div class="request-col mb-2">
                                            <h3 class="d-flex justify-content-between">
                                                <span>Request of Bumper</span>
                                                <span class="fbold fred price">$ 45</span>
                                            </h3>
                                            <div class="row address-block">
                                                <div class="col-lg-8 align-self-center">
                                                    <p><span class="fbold">Mobile</span></p>
                                                </div>
                                                <div class="col-lg-4 align-self-center">
                                                    <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                </div>
                                            </div>
                                            <h3 class="d-flex justify-content-between">
                                            <span class="fgreen align-self-center">Closed </span>
                                                <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                            </h3>
                                        </div>
                                        <!--/ request col -->
                                            

                                        </div>
                                        <!--/ Completed Tasks a Mechanic-->
                                        <!-- My Tasks as a Customer-->
                                        <div>
                                            <!-- request col -->
                                            <div class="request-col mb-2">
                                                <h3 class="d-flex justify-content-between">
                                                    <span>Customer Task</span>
                                                    <span class="fbold fred price">$ 45</span>
                                                </h3>
                                                <div class="row address-block">
                                                    <div class="col-lg-8 align-self-center">
                                                        <p><span class="fbold">Mobile</span></p>
                                                    </div>
                                                    <div class="col-lg-4 align-self-center">
                                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                    </div>
                                                </div>
                                                <h3 class="d-flex justify-content-between">
                                                <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                                </h3>
                                            </div>
                                            <!--/ request col -->

                                            <!-- request col -->
                                            <div class="request-col mb-2">
                                                <h3 class="d-flex justify-content-between">
                                                    <span>Customer Task</span>
                                                    <span class="fbold fred price">$ 45</span>
                                                </h3>
                                                <div class="row address-block">
                                                    <div class="col-lg-8 align-self-center">
                                                        <p><span class="fbold">Mobile</span></p>
                                                    </div>
                                                    <div class="col-lg-4 align-self-center">
                                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                    </div>
                                                </div>
                                                <h3 class="d-flex justify-content-between">
                                                <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                                </h3>
                                            </div>
                                            <!--/ request col -->

                                            <!-- request col -->
                                            <div class="request-col mb-2">
                                                <h3 class="d-flex justify-content-between">
                                                    <span>Customer Task</span>
                                                    <span class="fbold fred price">$ 45</span>
                                                </h3>
                                                <div class="row address-block">
                                                    <div class="col-lg-8 align-self-center">
                                                        <p><span class="fbold">Mobile</span></p>
                                                    </div>
                                                    <div class="col-lg-4 align-self-center">
                                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                    </div>
                                                </div>
                                                <h3 class="d-flex justify-content-between">
                                                <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                                </h3>
                                            </div>
                                            <!--/ request col -->

                                            <!-- request col -->
                                            <div class="request-col mb-2">
                                                <h3 class="d-flex justify-content-between">
                                                    <span>Customer Task</span>
                                                    <span class="fbold fred price">$ 45</span>
                                                </h3>
                                                <div class="row address-block">
                                                    <div class="col-lg-8 align-self-center">
                                                        <p><span class="fbold">Mobile</span></p>
                                                    </div>
                                                    <div class="col-lg-4 align-self-center">
                                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                    </div>
                                                </div>
                                                <h3 class="d-flex justify-content-between">
                                                <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                                </h3>
                                            </div>
                                            <!--/ request col -->

                                            <!-- request col -->
                                            <div class="request-col mb-2">
                                                <h3 class="d-flex justify-content-between">
                                                    <span>Customer Task</span>
                                                    <span class="fbold fred price">$ 45</span>
                                                </h3>
                                                <div class="row address-block">
                                                    <div class="col-lg-8 align-self-center">
                                                        <p><span class="fbold">Mobile</span></p>
                                                    </div>
                                                    <div class="col-lg-4 align-self-center">
                                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                    </div>
                                                </div>
                                                <h3 class="d-flex justify-content-between">
                                                <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                                </h3>
                                            </div>
                                            <!--/ request col -->

                                            <!-- request col -->
                                            <div class="request-col mb-2">
                                                <h3 class="d-flex justify-content-between">
                                                    <span>Customer Task</span>
                                                    <span class="fbold fred price">$ 45</span>
                                                </h3>
                                                <div class="row address-block">
                                                    <div class="col-lg-8 align-self-center">
                                                        <p><span class="fbold">Mobile</span></p>
                                                    </div>
                                                    <div class="col-lg-4 align-self-center">
                                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                                    </div>
                                                </div>
                                                <h3 class="d-flex justify-content-between">
                                                <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                                </h3>
                                            </div>
                                            <!--/ request col -->
                                        </div>
                                        <!--/ My Tasks as a Customer-->
                                    </div>
                                    <!--/ responsive tab container -->
                                </div>
                                <!--/ tab articles -->

                                <!-- reviews -->
                                <div class="profile-reviews">
                                    <h5 class="h3 text-center">Reviews</h5>
                                    <!-- custom tab -->
                                    <div class="profile-content cust-tab pt-0">
                                        <div class="parentHorizontalTab">
                                            <ul class="resp-tabs-list hor_1 nav justify-content-center">
                                                <li>As a Mechanic</li>
                                                <li>As a Customer</li>           
                                            </ul>
                                             <!-- responsive tab container -->
                                            <div class="resp-tabs-container hor_1">
                                                <!-- as a mechanic -->
                                                <div>
                                                    <!-- review col -->
                                                    <div class="d-flex p-3 review-col">
                                                        <img class="user-pic align-self-center" src="img/data/chairmanpic.jpg"/>
                                                        <article class="px-4 pt-0 align-self-center">
                                                            <h3 class="h6 mb-0 pb-0">
                                                                <a href="javascript:void(0)">User Name will be here</a>
                                                            </h3>
                                                            <ul class="review-rating nav">
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                            </ul>
                                                            <p><span class="fbold">Review:</span> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo, consectetur!</p>
                                                        </article>
                                                    </div>
                                                    <!-- review col -->
                                                    <!-- review col -->
                                                    <div class="d-flex p-3 review-col">
                                                        <img class="user-pic align-self-center" src="img/data/chairmanpic.jpg"/>
                                                        <article class="px-4 pt-0 align-self-center">
                                                            <h3 class="h6 mb-0 pb-0">
                                                                <a href="javascript:void(0)">User Name will be here</a>
                                                            </h3>
                                                            <ul class="review-rating nav">
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                            </ul>
                                                            <p><span class="fbold">Review:</span> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo, consectetur!</p>
                                                        </article>
                                                    </div>
                                                    <!-- review col -->
                                                    <!-- review col -->
                                                    <div class="d-flex p-3 review-col">
                                                        <img class="user-pic align-self-center" src="img/data/chairmanpic.jpg"/>
                                                        <article class="px-4 pt-0 align-self-center">
                                                            <h3 class="h6 mb-0 pb-0">
                                                                <a href="javascript:void(0)">User Name will be here</a>
                                                            </h3>
                                                            <ul class="review-rating nav">
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                            </ul>
                                                            <p><span class="fbold">Review:</span> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo, consectetur!</p>
                                                        </article>
                                                    </div>
                                                    <!-- review col -->
                                                </div>
                                                <!--/ as a mechanic -->
                                                <!-- as a customer -->
                                                <div>

                                                <!-- review col -->
                                                <div class="d-flex p-3 review-col">
                                                    <img class="user-pic align-self-center" src="img/data/chairmanpic.jpg"/>
                                                    <article class="px-4 pt-0 align-self-center">
                                                        <h3 class="h6 mb-0 pb-0">
                                                            <a href="javascript:void(0)">Customer Name will be here</a>
                                                        </h3>
                                                        <ul class="review-rating nav">
                                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                        </ul>
                                                        <p><span class="fbold">Review:</span> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo, consectetur!</p>
                                                    </article>
                                                </div>
                                                <!-- review col -->

                                                <!-- review col -->
                                                <div class="d-flex p-3 review-col">
                                                    <img class="user-pic align-self-center" src="img/data/chairmanpic.jpg"/>
                                                    <article class="px-4 pt-0 align-self-center">
                                                        <h3 class="h6 mb-0 pb-0">
                                                            <a href="javascript:void(0)">Customer Name will be here</a>
                                                        </h3>
                                                        <ul class="review-rating nav">
                                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                        </ul>
                                                        <p><span class="fbold">Review:</span> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo, consectetur!</p>
                                                    </article>
                                                </div>
                                                <!-- review col -->


                                                <!-- review col -->
                                                <div class="d-flex p-3 review-col">
                                                    <img class="user-pic align-self-center" src="img/data/chairmanpic.jpg"/>
                                                    <article class="px-4 pt-0 align-self-center">
                                                        <h3 class="h6 mb-0 pb-0">
                                                            <a href="javascript:void(0)">Customer Name will be here</a>
                                                        </h3>
                                                        <ul class="review-rating nav">
                                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                        </ul>
                                                        <p><span class="fbold">Review:</span> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo, consectetur!</p>
                                                    </article>
                                                </div>
                                                <!-- review col -->


                                                </div>
                                                <!--/ as a customer -->
                                            </div>
                                            <!--/ responsive tab container -->
                                        </div>
                                    </div>
                                    <!--/ custom tab -->
                                </div>
                                <!--/ revies -->
                            </div>
                            <!--/ profile content -->
                        </div>
                        <!--/ right col 8-->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ profile header -->
           
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

     
    <!--Edit Profile -->
    <!-- The Modal -->
  <div class="modal fade" id="edit-profile">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title h5">Edit Profile</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body"> 

           <!-- form group -->
            <div class="form-group">
                <label>About me</label>
                <textarea class="form-control" style="height:100px;" placeholder="Describe your self"></textarea>
            </div>
            <!--/ form group -->

            <!-- form group -->
            <div class="form-group">
                <label>Change Profile Picture</label>
                <input type="File" placeholder="Select Picture" class="form-control">
            </div>
            <!--/ form group -->

            <!-- form group -->
            <div class="form-group">
                <label>Date Birth</label>
                <input id="datepicker" type="text" placeholder="Select Date" class="form-control">
            </div>
            <!--/ form group -->

            <!-- form group -->
            <div class="form-group">
                <label>Port Folio</label>
                <textarea class="form-control" style="height:100px;" placeholder="Describe your Portfolio"></textarea>
            </div>
            <!--/ form group -->

            <!-- form group -->
            <div class="form-group">
                <label>Skills</label>
                <textarea class="form-control" style="height:50px;" placeholder="Write your skills by seperating comma"></textarea>
            </div>
            <!--/ form group -->
            
            <!--/raging -->
        </div>
        
        <div class="modal-footer">        
            <button type="button" class="redbtn w-100">Save Profile</button>
        </div>
    </div>
  </div>
    <!--/ Edit Profile -->
</body>

</html>