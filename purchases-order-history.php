<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
              <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <!-- title -->
                    <div class="db-pagetitle">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-12">
                                <article>
                                    <h2 class="h5 fbold">Purchase Order History</h2>                            
                                </article>
                            </div>
                            <!--/ col --> 
                        </div>
                         <!--/ row -->  
                    </div>
                    <!--/ title -->

                    <!-- body -->
                    <div class="report-body cust-tab">
                       <!-- tab -->
                       <div class="parentHorizontalTab">
                            <ul class="resp-tabs-list hor_1 nav justify-content-center">
                                <li>Current Orders</li>
                                <li>Need Approval</li>
                                <li>All Orders</li>                                
                            </ul>
                            <!-- tab container -->
                            <div class="resp-tabs-container hor_1">

                                <!-- current order s-->
                                <div>
                                    <!-- table -->
                                    <table class="table table-striped">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">Order Number</th>
                                                <th scope="col">Item</th>
                                                <th scope="col">Customer</th>
                                                <th scope="col">Delivery Type</th>
                                                <th scope="col">Shipping Status</th>
                                                <th scope="col">Price after Commission</th>
                                                <th scope="col">Delivery Address</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="fbold">1234</td>
                                                <td>Bumper</td>
                                                <td>Tim</td>
                                                <td>Pick up from Store</td>
                                                <td>Drop down</td>
                                                <td>$50</td>
                                                <td>Geelong</td>
                                            </tr>  

                                            <tr>
                                                <td class="fbold">5462</td>
                                                <td>Tyre</td>
                                                <td>Jack</td>
                                                <td>Express Delivery</td>
                                                <td>Order Received</td>
                                                <td>$130</td>
                                                <td>Werribee</td>
                                            </tr>

                                            <tr>
                                                <td class="fbold">1234</td>
                                                <td>Bumper</td>
                                                <td>Tim</td>
                                                <td>Pick up from Store</td>
                                                <td>Drop down</td>
                                                <td>$50</td>
                                                <td>Geelong</td>
                                            </tr>
                                                            
                                        </tbody>
                                    </table>
                                    <!--/ table -->
                                </div>
                                <!--/ current orders -->

                                 <!-- Need Approval -->
                                 <div>
                                     <!-- table -->
                                     <table class="table table-striped">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">Order Number</th>
                                                <th scope="col">Item</th>
                                                <th scope="col">Customer</th>
                                                <th scope="col">Delivery Type</th>
                                                <th scope="col">Shipping Status</th>
                                                <th scope="col">Price after Commission</th>
                                                <th scope="col">Delivery Address</th>
                                                <th scope="col">Approve Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="fbold">1234</td>
                                                <td>Bumper</td>
                                                <td>Tim</td>
                                                <td>Pick up from Store</td>
                                                <td>Drop down</td>
                                                <td>$50</td>
                                                <td>Geelong</td>
                                                <td>Approved</td>
                                            </tr>  

                                            <tr>
                                                <td class="fbold">5462</td>
                                                <td>Tyre</td>
                                                <td>Jack</td>
                                                <td>Express Delivery</td>
                                                <td>Order Received</td>
                                                <td>$130</td>
                                                <td>Werribee</td>
                                                <td>Cancelled</td>
                                            </tr>

                                            <tr>
                                                <td class="fbold">1234</td>
                                                <td>Bumper</td>
                                                <td>Tim</td>
                                                <td>Pick up from Store</td>
                                                <td>Drop down</td>
                                                <td>$50</td>
                                                <td>Geelong</td>
                                                <td>Rejected</td>
                                            </tr>
                                                            
                                        </tbody>
                                    </table>
                                    <!--/ table -->
                                 </div>
                                <!--/ Need Approval -->

                                 <!-- All Orders -->
                                 <div>
                                    <!-- table -->
                                    <table class="table table-striped">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">Order Number</th>
                                                <th scope="col">Item</th>
                                                <th scope="col">Customer</th>
                                                <th scope="col">Delivery Type</th>
                                                <th scope="col">Shipping Status</th>
                                                <th scope="col">Price after Commission</th>
                                                <th scope="col">Delivery Address</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="fbold">1234</td>
                                                <td>Bumper</td>
                                                <td>Tim</td>
                                                <td>Pick up from Store</td>
                                                <td>Drop down</td>
                                                <td>$50</td>
                                                <td>Geelong</td>                                                
                                            </tr>  

                                            <tr>
                                                <td class="fbold">5462</td>
                                                <td>Tyre</td>
                                                <td>Jack</td>
                                                <td>Express Delivery</td>
                                                <td>Order Received</td>
                                                <td>$130</td>
                                                <td>Werribee</td>                                               
                                            </tr>

                                            <tr>
                                                <td class="fbold">1234</td>
                                                <td>Bumper</td>
                                                <td>Tim</td>
                                                <td>Pick up from Store</td>
                                                <td>Drop down</td>
                                                <td>$50</td>
                                                <td>Geelong</td>                                               
                                            </tr>                                                            
                                        </tbody>
                                    </table>
                                    <!--/ table -->
                                 </div>
                                <!--/ All Orders -->

                            </div>
                            <!--/ tab container -->
                       </div>
                       <!--/ tab -->
                    </div>
                    <!--/ body -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>