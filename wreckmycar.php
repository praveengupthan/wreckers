<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreck My Car</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Wreck my Car</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
            <!-- steps -->
            <div class="steps-wreckers">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-6">
                        <!-- form -->
                        <form action="">                            
                            <!-- div id wizard-->
                            <div id="wizard" class="wreckmycar">
                                <!-- SECTION 1 -->
                                <h4></h4>
                                <section>
                                    <h5>Rego Details</h5>
                                    <div class="form-group mb-1">
                                        <label class="label">Enter Your Rego <span>*</span></label>
                                        <input type="text" class="form-control pt-0" placeholder="Ex: ZRX 222">  
                                    </div>                                    
                                    <a data-toggle="modal" data-target="#getrego" class="redbtn" href="javascript:void(0)">Get Details</a>                                       
                               
                                    <div class="form-group mt-3">
                                        <label class="label">Description <span>*</span></label>
                                        <textarea style="height:100px;" class="form-control" placeholder="(Minimum 25 Characters)"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="d-block">Upload Images <span>*</span>  </label>
                                        <div class="custom-file-upload float-left mr-2">
                                            <label for="file-upload" class="custom-file-upload1">
                                                <i class="fa fa-cloud-upload"></i> Upload Image
                                            </label>
                                            <input id="file-upload" type="file" multiple/>
                                        </div>                                       
                                    </div>                                 
                                </section>

                                <!--/ SECTION 1 -->
                                
                                <!-- SECTION 2 -->
                                <h4></h4>
                                <section>
                                    <h5>Price Details</h5>
                                   
                                    <div class="form-group">
                                        <label class="label">Your Post Code <span>*</span></label>
                                        <input type="text" class="form-control pt-0" placeholder="Enter your Pincode / Postal Code">
                                    </div>

                                   <div class="form-group mt-3">
                                        <p >How much do you want to sell this vehicle for?</p>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">$</span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Ex:20">
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <p class="hightlate p-2">Remember, Your post will be expired in 7 days</p>
                                    </div>
                                    
                                </section>
                                <!-- / SECTION 2-->

                               
                            </div>
                            <!--/ div id wizard -->
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ col-->
                </div>
                <!--/ row -->
            </div>
            <!-- /steps -->               
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

    <!-- The Modal get vehicle details -->
<div class="modal" id="getrego">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Registration Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <table id="displayCarDetails" class="table table-striped">
            <tr>
                <td>Make</td>
                <td>:</td>
                <td>Toyota</td>
            </tr>
            <tr>
                <td>Model</td>
                <td>:</td>
                <td>Model Number</td>
            </tr>
            <tr>
                <td>Year of Model</td>
                <td>:</td>
                <td>2008</td>
            </tr>
            <tr>
                <td>Transmission</td>
                <td>:</td>
                <td>Manual</td>
            </tr>
            <tr>
                <td>Engine Type</td>
                <td>:</td>
                <td>Diesel Engine</td>
            </tr>
        </table>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

</body>

</html>