<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Payments and Commissions </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
              <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <!-- title -->
                    <div class="db-pagetitle">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-12">
                                <article>
                                    <h2 class="h5 fbold">Payments &amp; Commissions</h2>                            
                                </article>
                            </div>
                            <!--/ col --> 
                        </div>
                         <!--/ row -->  
                    </div>
                    <!--/ title -->

                    <!-- body -->
                    <div class="report-body">
                         <!-- accordian -->
                         <div class="cust-accord">
                                <!-- custom accordian -->
                                <div class="accordion">
                                    <!-- card -->
                                    <div class="card">
                                        <!-- header -->
                                        <h3 class="panel-title card-header">
                                            <span style="width:30.3%" class="d-inline-block px-3">Order No:1234, </span>
                                            <span style="width:30.3%" class="d-inline-block px-3">Sold Price: $123</span>
                                            <span style="width:30.3%" class="d-inline-block px-3">Net Price: $123</span>
                                        </h3>
                                        <!--/ header -->
                                        <!-- panel content -->
                                        <div class="panel-content card-body">
                                            <!--  card details -->
                                            <div class="card-details">
                                                <!-- row -->
                                                <div class="row justify-content-around">
                                                    <!-- col 3-->
                                                    <div class="col-lg-3">
                                                        <a href="javascript:void(0)">
                                                            <img src="img/data/cathome02.jpg" class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <!--/ col 3-->
                                                    <!-- col 9-->
                                                    <div class="col-lg-9">
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Order No:</dt>
                                                           <dd class="col-lg-8">123456</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Item</dt>
                                                           <dd class="col-lg-8">Back Bumper</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Price Sold</dt>
                                                           <dd class="col-lg-8">$126.00</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Commission</dt>
                                                           <dd class="col-lg-8">$5.00</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Net Price</dt>
                                                           <dd class="col-lg-8">$120.00</dd>
                                                       </dl>
                                                    </div>
                                                </div>
                                                <!--/ row -->
                                                <div class="text-right">   
                                                    <a class="redbtn" href="javascript:void(0)"><span class="icon-download"></span> Download Invoice</a>
                                                </div>
                                            </div>
                                            <!--/ card details -->
                                        </div>
                                        <!-- panel content -->
                                    </div>
                                    <!--/ card -->

                                     <!-- card -->
                                     <div class="card">
                                         <!-- header -->
                                        <h3 class="panel-title card-header">
                                            <span style="width:30.3%" class="d-inline-block px-3">Order No:1234, </span>
                                            <span style="width:30.3%" class="d-inline-block px-3">Sold Price: $123</span>
                                            <span style="width:30.3%" class="d-inline-block px-3">Net Price: $123</span>
                                        </h3>
                                        <!--/ header -->
                                       <!-- panel content -->
                                       <div class="panel-content card-body">
                                            <!--  card details -->
                                            <div class="card-details">
                                                <!-- row -->
                                                <div class="row justify-content-around">
                                                    <!-- col 3-->
                                                    <div class="col-lg-3">
                                                        <a href="javascript:void(0)">
                                                            <img src="img/data/cathome03.jpg" class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <!--/ col 3-->
                                                    <!-- col 9-->
                                                    <div class="col-lg-9">
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Order No:</dt>
                                                           <dd class="col-lg-8">123456</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Item</dt>
                                                           <dd class="col-lg-8">Back Bumper</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Price Sold</dt>
                                                           <dd class="col-lg-8">$126.00</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Commission</dt>
                                                           <dd class="col-lg-8">$5.00</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Net Price</dt>
                                                           <dd class="col-lg-8">$120.00</dd>
                                                       </dl>
                                                    </div>
                                                </div>
                                                <!--/ row -->
                                                <div class="text-right">   
                                                    <a class="redbtn" href="javascript:void(0)"><span class="icon-download"></span> Download Invoice</a>
                                                </div>
                                            </div>
                                            <!--/ card details -->
                                        </div>
                                        <!-- panel content -->
                                    </div>
                                    <!--/ card -->

                                     <!-- card -->
                                     <div class="card">
                                         <!-- header -->
                                        <h3 class="panel-title card-header">
                                            <span style="width:30.3%" class="d-inline-block px-3">Order No:1234, </span>
                                            <span style="width:30.3%" class="d-inline-block px-3">Sold Price: $123</span>
                                            <span style="width:30.3%" class="d-inline-block px-3">Net Price: $123</span>
                                        </h3>
                                        <!--/ header -->
                                        <!-- panel content -->
                                        <div class="panel-content card-body">
                                            <!--  card details -->
                                            <div class="card-details">
                                                <!-- row -->
                                                <div class="row justify-content-around">
                                                    <!-- col 3-->
                                                    <div class="col-lg-3">
                                                        <a href="javascript:void(0)">
                                                            <img src="img/data/cathome04.jpg" class="img-fluid">
                                                        </a>
                                                    </div>
                                                    <!--/ col 3-->
                                                    <!-- col 9-->
                                                    <div class="col-lg-9">
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Order No:</dt>
                                                           <dd class="col-lg-8">123456</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Item</dt>
                                                           <dd class="col-lg-8">Back Bumper</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Price Sold</dt>
                                                           <dd class="col-lg-8">$126.00</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Commission</dt>
                                                           <dd class="col-lg-8">$5.00</dd>
                                                       </dl>
                                                       <dl class="row">
                                                           <dt class="col-lg-4">Net Price</dt>
                                                           <dd class="col-lg-8">$120.00</dd>
                                                       </dl>
                                                    </div>
                                                </div>
                                                <!--/ row -->
                                                <div class="text-right">   
                                                    <a class="redbtn" href="javascript:void(0)"><span class="icon-download"></span> Download Invoice</a>
                                                </div>
                                            </div>
                                            <!--/ card details -->
                                        </div>
                                        <!-- panel content -->
                                    </div>
                                    <!--/ card -->
                                </div>
                                <!--/ custom acccordian -->
                           </div>
                           <!--/ accordian -->
                    </div>
                    <!--/ body -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>