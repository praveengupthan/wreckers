<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Terms of Service</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-6">
                        <h4 class="h5 fred">Terms of Service</h4>
                        <p>This document is an electronic record in terms of Information Technology Act, 2000 and rules there under as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures.</p>
                        <p>This document is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries guidelines) Rules, 2011 that require publishing the rules and regulations, privacy policy and Terms of Use for access or usage of www.partsBig Boss.in website.</p>
                        <p>The domain name www.partsbigboss.in (hereinafter referred to as "Website") is owned by PARTSBIGBOSS LIMITED  with its registered office at Regd Off - Plot-90, Pocket-O, Sector-3, DSIIDC Industrial Area, Bawana, Delhi - 110039 India (hereinafter referred to as "PartsBigBoss").</p>
                        <p>Your use of the Website and services and tools are governed by the following terms and conditions ("Terms of Use") as applicable to the Website including the applicable policies which are incorporated herein by way of reference. If You transact on the Website, You shall be subject to the policies that are applicable to the Website for such transaction. By mere use of the Website, You shall be contracting with PARTSBIGBOSS LIMITED and these terms and conditions including the policies constitute Your binding obligations, with PartsBigBoss.</p>
                   </div>

                   <div class="col-lg-6">                       
                        <p>This document is an electronic record in terms of Information Technology Act, 2000 and rules there under as applicable and the amended provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures.</p>
                        <p>This document is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries guidelines) Rules, 2011 that require publishing the rules and regulations, privacy policy and Terms of Use for access or usage of www.partsBig Boss.in website.</p>
                        <p>The domain name www.partsbigboss.in (hereinafter referred to as "Website") is owned by PARTSBIGBOSS LIMITED  with its registered office at Regd Off - Plot-90, Pocket-O, Sector-3, DSIIDC Industrial Area, Bawana, Delhi - 110039 India (hereinafter referred to as "PartsBigBoss").</p>
                        <p>Your use of the Website and services and tools are governed by the following terms and conditions ("Terms of Use") as applicable to the Website including the applicable policies which are incorporated herein by way of reference. If You transact on the Website, You shall be subject to the policies that are applicable to the Website for such transaction. By mere use of the Website, You shall be contracting with PARTSBIGBOSS LIMITED and these terms and conditions including the policies constitute Your binding obligations, with PartsBigBoss.</p>
                   </div>
                
                
               </div>
               <!--/ row -->

            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>