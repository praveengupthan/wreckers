<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body">
          <!-- dashbaord sliders -->
          <div class="db-slider">
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img class="d-block w-100" src="img/dbslider01.jpg" alt="">
                </div>
                <div class="carousel-item">
                <img class="d-block w-100" src="img/dbslider02.jpg" alt="">
                </div>
                <div class="carousel-item">
                <img class="d-block w-100" src="img/dbslider03.jpg" alt="">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            </div>
          </div>
          <!--/ dashboard sliders -->

          <!-- container -->
          <div class="container py-4">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="title-row">
                                    <h2 class="d-flex justify-content-between">
                                        <span>Our Brands</span> 
                                        <a href="javascript:void(0)">View All</a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <!--/ title row -->
                        <!-- row -->
                        <div class="row dblogos">
                            <div class="col-lg-3">
                                <img src="img/data/brand01.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand02.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand03.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand04.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand05.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand06.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand07.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand08.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand09.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand10.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand01.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand02.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand01.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand02.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand03.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand04.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand05.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand06.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand07.jpg" alt="" title="" class="img-fluid">
                            </div>
                            <div class="col-lg-3">
                                <img src="img/data/brand08.jpg" alt="" title="" class="img-fluid">
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-6">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="title-row">
                                    <h2 class="d-flex justify-content-between">
                                        <span>Recent Orders </span>
                                        <a href="javascript:void(0)">View All</a>
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <!--/ title row -->
                        <!-- order row -->
                        <div class="row orderrow">
                            <!-- col -->
                            <div class="col-lg-5">
                                <a href="javascriptp:void(0)"><img src="img/data/dbpart01.jpg" class="img-fluid"></a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-7">
                                <h2><a href="javascript:void(0)">Product Name will be here</a></h2>
                                <p class="fgray">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cumque, perspiciatis?</p>
                                <p>Order Date: <span>18-05-2019 </span> <span class="fgreen d-inline-block pl-3"> In Process</span> </p>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ order row -->

                         <!-- order row -->
                         <div class="row orderrow">
                            <!-- col -->
                            <div class="col-lg-5">
                                <a href="javascriptp:void(0)"><img src="img/data/dbpart01.jpg" class="img-fluid"></a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-7">
                                <h2><a href="javascript:void(0)">Product Name will be here</a></h2>
                                <p class="fgray">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cumque, perspiciatis?</p>
                                <p>Order Date: <span>18-05-2019 </span> <span class="fred d-inline-block pl-3"> Cancelled</span> </p>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ order row -->

                         <!-- order row -->
                         <div class="row orderrow">
                            <!-- col -->
                            <div class="col-lg-5">
                                <a href="javascriptp:void(0)"><img src="img/data/dbpart01.jpg" class="img-fluid"></a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-7">
                                <h2><a href="javascript:void(0)">Product Name will be here</a></h2>
                                <p class="fgray">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cumque, perspiciatis?</p>
                                <p>Order Date: <span>18-05-2019 </span> <span class="fgreen d-inline-block pl-3"> Delivered</span> </p>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ order row -->
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
          </div>
          <!--/ container -->

            <!-- popular categories -->
            <div class="container">
                <!-- title row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="title-row">
                            <h2>Popular Categories <a href="javascript:void(0)">View All</a></h2>
                        </div>
                    </div>
                </div>
                <!--/ title row -->

                <!-- slick slider row -->
                <div class="catgories-home custom-slick">
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome01.jpg" alt="" class="img-fluid">
                            <span class="d-block sltitle">Head Lights</span>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome02.jpg" alt="" class="img-fluid">
                            <span class="d-block sltitle">Head Lights</span>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome03.jpg" alt="" class="img-fluid">
                            <span class="d-block sltitle">Car Breaks</span>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome04.jpg" alt="" class="img-fluid">
                            <span class="d-block sltitle">Seat Covers</span>
                        </a>
                    </div>
                    <!--/ slide -->

                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome05.jpg" alt="" class="img-fluid">
                            <span class="d-block sltitle">Car Wheels</span>
                        </a>
                    </div>
                    <!--/ slide -->

                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome06.jpg" alt="" class="img-fluid">
                            <span class="d-block sltitle">Head Lights</span>
                        </a>
                    </div>
                    <!--/ slide -->

                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome07.jpg" alt="" class="img-fluid">
                            <span class="d-block sltitle">Head Lights</span>
                        </a>
                    </div>
                    <!--/ slide -->

                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome08.jpg" alt="" class="img-fluid">
                            <span class="d-block sltitle">Head Lights</span>
                        </a>
                    </div>
                    <!--/ slide -->
                </div>
                <!--/ slick slider row -->
            </div>
            <!--/ popular categories -->

            <!-- recommendations -->           
            <div class="container">
                <!-- title row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="title-row">
                            <h2>Recommendations <a href="javascript:void(0)">View All</a></h2>
                        </div>
                    </div>
                </div>
                <!--/ title row -->
                <!-- slick slider row -->
                <div class="catgories-home custom-slick">
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part01.jpg" alt="" class="img-fluid">                            
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>                           
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part02.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>                            
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part03.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part04.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part05.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part06.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part07.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part08.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                     <!-- slide -->
                     <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part09.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                     <!-- slide -->
                     <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part10.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part11.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part12.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/part13.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                </div>
                <!--/ slick slider row -->
            </div>
            <!--/ recommendations -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>