<!-- task detail nav -->
            <div class="navsearch">
                <!-- container -->
                <div class="container">
                    <!-- nav bar -->
                    <nav class="navbar navbar-expand-lg navbar-light  sticky-top">                           
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent2">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Distance
                                    </a>
                                    <!-- dropdown -->
                                    <div class="dropdown-menu p-3" style="width:200px;">

                                        <div class="form-group">
                                            <label>SUPERB</label>
                                            <input type="text" placeholder="SUBURB" class="form-control">
                                        </div>    
                                            
                                        <div class="form-group">
                                            <label>Distance</label>
                                            <input type="text" placeholder="Distance in KM" class="form-control">
                                        </div>  

                                        <p class="d-flex justify-content-between">
                                            <a class="align-self-center" href=javascript:void(0)>Canel</a>
                                            <a class="redbtn btnsmall" href=javascript:void(0)>Apply </a>
                                        </p>

                                    </div>
                                    <!--/ dropdown -->
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Repair Price
                                    </a>
                                    <div class="dropdown-menu p-3">
                                        <section class="range-slider">
                                            <span class="rangeValues"></span>
                                            <input value="500" min="500" max="50000" step="500" type="range">
                                            <input value="50000" min="500" max="50000" step="500" type="range">
                                        </section>
                                        <div>
                                            <p class="d-flex justify-content-between">
                                                <a class="align-self-center" href=javascript:void(0)>Canel</a>
                                                <a class="redbtn btnsmall" href=javascript:void(0)>Apply </a>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Type
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">Mobile</a>
                                        <a class="dropdown-item" href="#">Instore</a>
                                        <a class="dropdown-item" href="#">Any</a>
                                    </div>
                                </li>                                
                            </ul>

                            <div class="my-lg-0 d-flex justify-content-between div-search">
                                <form class="form-group mb-0 save-search-form">
                                    <div class="input-group position-relative">
                                        <input class="form-control" type="search" placeholder="Search" aria-label="Search" id="search-input"> 
                                        <span class="icon-search position-absolute icomoon"></span>
                                    </div>

                                    <!-- search results auto populate-->
                                    <div class="search-auto" id="search-auto">
                                        <h5 class="h6 text-uppercase d-flex justify-content-between">
                                            <span>Recent Searches</span>
                                            <a id="close-search" href="javascript:void(0)"><span class="icon-close"></span></a>
                                        </h5>                                      
                                       
                                        <ul class="recent-searches">
                                            <li class="d-flex justify-content-between">
                                                <div>
                                                    <a class="titleli" href="javascript:void(0)">Gippsland Stays</a>
                                                    <p class="small fgray">27 Dec- 3 guests 1 filter</p>
                                                </div>
                                                <div class="align-self-center">
                                                    <a href="javascript:void(0)" class="whitebtn small">Notify me</a>
                                                </div>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <div>
                                                    <a class="titleli" href="javascript:void(0)">Gippsland Stays</a>
                                                    <p class="small fgray">27 Dec- 3 guests 1 filter</p>
                                                </div>
                                                <div class="align-self-center">
                                                    <a href="javascript:void(0)" class="whitebtn small">Notify me</a>
                                                </div>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                                <div>
                                                    <a class="titleli" href="javascript:void(0)">Gippsland Stays</a>
                                                    <p class="small fgray">27 Dec- 3 guests 1 filter</p>
                                                </div>
                                                <div class="align-self-center">
                                                    <a href="javascript:void(0)" class="whitebtn small">Notify me</a>
                                                </div>
                                            </li>
                                            <li class="d-flex justify-content-between">
                                            <div class="align-self-center">
                                                    <a class="titleli" href="javascript:void(0)">Gippsland Stays</a>
                                                    <p class="small fgray">27 Dec- 3 guests 1 filter</p>
                                                </div>
                                                <div class="align-self-center">
                                                    <a href="javascript:void(0)" class="whitebtn small">Notify me</a>
                                                </div>
                                            </li>                                            
                                           
                                        </ul>
                                    </div>
                                    <!--/ search results auto populate-->
                                </form>
                                <a class="small text-center align-self-center px-2 fbold" href="javascript:void(0)" data-toggle="modal" data-target="#savesearch">Save Search</a>
                            </div> 
                        </div>
                    </nav>
                    <!--/ nav bar --> 
                </div>
                <!--/ container -->
            </div>
            <!--/ task detail nav -->

            
            