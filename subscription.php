<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Subscription</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>User Subscription</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-4">
                        <div class="subscription-col">
                            <span class="icon-tag icomoon"></span> 
                            <h2 class="subscribe-title">Business</h2> 
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, voluptatibus hic iusto distinctio </p>

                            <h3>Top Advantages</h3>                          

                            <table>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Access to 300+ ready email templates;</td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Advanced drag 'n' drop aditor and HTML email creator for advanced users;</td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Sending test emails while designing;</td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Sharing through a public link;</td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Direct export as HTML or to any integrated ESP with  one click;</td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Support via chat, phone, and email.</td>
                                </tr>
                            </table>

                            <a href="javascript:void(0)" class="redbtn">Learn More</a>
                        </div>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4">
                        <div class="subscription-col">
                        <span class="icon-angellist icomoon"></span>
                            <h2 class="subscribe-title">Agency</h2> 
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, voluptatibus hic iusto distinctio </p>

                            <h3>Top Advantages Same as Business</h3>                          

                            <table>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Unlimited projects in your organization;</td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>User and role management within Projectws</td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Trcking emails template versioning; </td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Simultaneous editing and annotation (soon)</td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Custom branding configuration of your account (soon);</td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Unlimited exports and templates</td>
                                </tr>
                            </table>

                            <a href="javascript:void(0)" class="redbtn">Learn More</a>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-4">
                        <div class="subscription-col">
                        <span class="icon-fire icomoon"></span>
                            <h2 class="subscribe-title">Plugin</h2> 
                            <p>Boost your application with drag and drop email editor enabling your customer to create beautiful responsive emails fast </p>

                            <h3 class="pb-3">Embed Stripo Builder into Saas</h3>    
                            
                            <p>Boost your application with drag and drop email editor enabling your customer to create beautiful responsive emails fast</p>

                            <table>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Save yoru money and time on development</td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>2-minutes integration; </td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Full Customization; </td>
                                </tr>
                                <tr>
                                    <td><span class="icon-cogs icomoon"></span></td>
                                    <td>Keeping assets on own servers</td>
                                </tr>                                                         
                            </table>

                            <a href="javascript:void(0)" class="redbtn">Learn More</a>
                        </div>
                    </div>
                    <!--/ col -->


                </div>
                <!--/ row -->             
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>