<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Payment Details </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
                <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->


                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <div class="db-pagetitle d-flex justify-content-between">
                        <article>
                            <h2 class="h5 fbold">Your Credit and Debit Cards</h2>
                            <p class="pb-0">Update and Add Payment Methods</p>
                        </article>                                           
                    </div>

                    <!-- row -->
                    <div class="row payment-details">
                        <div class="col-lg-12">
                           <!-- accordian -->
                           <div class=" cust-accord">
                                <!-- custom accordian -->
                                <div class="accordion">
                                    <!-- card -->
                                    <div class="card">
                                        <h3 class="panel-title card-header">
                                            <span style="width:5%" class="icon-cc-mastercard icomoon cardicon"></span>
                                            <span style="width:60%" class="d-inline-block px-3">Master Card ending in 9345</span>
                                            <span style="width:35%">Expires 01/2021</span>
                                        </h3>
                                        <div class="panel-content card-body">
                                            <!--  card details -->
                                            <div class="card-details" id="card-details">
                                                <div class="d-flex justify-content-around">
                                                    <div style="width:65%">
                                                        <h6 class="h6 fbold">Name on Card</h6>
                                                        <p>Chaitanya Bade</p>
                                                    </div>
                                                    <div style="width:35%">
                                                        <h6 class="h6 fbold">Billing Address</h6>
                                                        <h6 class="h6 fbold">Chaitanya Bade</h6>
                                                        <p>9/9, Finchaven st, Herne Hill, VIC , 3218, Australia, +6145033858</p>
                                                    </div>
                                                </div>
                                                <div class="text-right">
                                                    <a class="redbtn" href="javascript:void(0)">Remove</a>
                                                    <a id="editcard"  class="redbtn" href="javascript:void(0)">Edit</a>
                                                </div>
                                            </div>
                                            <!--/ card details -->

                                            <!-- edit card details -->
                                            <div class="editcard-details" id="editcard-details">
                                                <h5 class="h5 py2">Edit Card Details</h5>
                                                <!-- row-->
                                                <div class="row">
                                                    <div class="form-group col-lg-6">
                                                        <label>Name on Card</label>
                                                        <input type="text" placeholder="Chaitanya Bade" class="form-control w-100">
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="d-block">Expiration Date</label>
                                                        <div class="input-group w-50 float-left">
                                                            <select class="form-control">
                                                                <option>01</option>
                                                                <option>02</option>
                                                                <option>03</option>
                                                                <option>04</option>
                                                                <option>05</option>
                                                                <option>06</option>
                                                                <option>07</option>
                                                                <option>08</option>
                                                                <option>09</option>
                                                                <option>10</option>
                                                                <option>11</option>
                                                                <option>12</option>
                                                            </select>
                                                        </div>
                                                        <div class="input-group w-50 float-left">
                                                            <select class="form-control">
                                                                <option>2019</option>
                                                                <option>2020</option>
                                                                <option>2021</option>
                                                                <option>2022</option>
                                                                <option>2023</option>
                                                                <option>2024</option>
                                                                <option>2025</option>
                                                                <option>2026</option>
                                                                <option>2027</option>
                                                                <option>2028</option>
                                                                <option>2029</option>
                                                                <option>2030</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <!--/ row -->
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h6 class="h5 fbold">Billing Address</h6>

                                                        <h6 class="h6">Chaitanya Bade</h6>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" placeholder="9/9, Finchaven st, Herne Hill, VIC , 3218, Australia, +6145033858" class="form-control" disabled>
                                                            </div>
                                                            <a class="fblue" href="javascript:void(0)">Edit Address</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-right">
                                                    <a class="redbtn" href="javascript:void(0)">Cancel</a>
                                                    <a id="savecard"  class="redbtn" href="javascript:void(0)">Save</a>
                                                </div>
                                            </div>
                                            <!--/ edit card details -->
                                        </div>
                                    </div>
                                    <!--/ card -->

                                     <!-- card -->
                                     <div class="card">
                                        <h3 class="panel-title card-header">
                                            <span style="width:5%" class="icon-cc-mastercard icomoon cardicon"></span>
                                            <span style="width:60%" class="d-inline-block px-3">Master Card ending in 3867</span>
                                            <span class="tred" style="width:35%">Expires 01/2019</span>
                                        </h3>
                                        <div class="panel-content card-body">
                                            <div class="d-flex justify-content-around">
                                                <div style="width:65%">
                                                    <h6 class="h6 fbold">Name on Card</h6>
                                                    <p>Chaitanya Bade</p>
                                                </div>
                                                <div style="width:35%">
                                                    <h6 class="h6 fbold">Billing Address</h6>
                                                    <h6 class="h6 fbold">Chaitanya Bade</h6>
                                                    <p>9/9, Finchaven st, Herne Hill, VIC , 3218, Australia, +6145033858</p>
                                                </div>
                                            </div>
                                            <div class="text-right">
                                                <a class="redbtn" href="javascript:void(0)">Remove</a>
                                                <a  class="redbtn" href="javascript:void(0)">Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ card -->

                                     <!-- card -->
                                     <div class="card">
                                        <h3 class="panel-title card-header">
                                            <span style="width:5%" class="icon-automobile icomoon cardicon"></span>
                                            <span style="width:60%" class="d-inline-block px-3">getnFix Wallet </span>
                                            <span style="width:35%">$0.00</span>
                                        </h3>
                                        <div class="panel-content card-body">
                                            <div class="d-flex justify-content-around">
                                                <div style="width:65%">
                                                    <h6 class="h6 fbold">Name on Card</h6>
                                                    <p>Chaitanya Bade</p>
                                                </div>
                                                <div style="width:35%">
                                                    <h6 class="h6 fbold">Billing Address</h6>
                                                    <h6 class="h6 fbold">Chaitanya Bade</h6>
                                                    <p>9/9, Finchaven st, Herne Hill, VIC , 3218, Australia, +6145033858</p>
                                                </div>
                                            </div>
                                            <div class="text-right">
                                                <a class="redbtn" href="javascript:void(0)">Remove</a>
                                                <a  class="redbtn" href="javascript:void(0)">Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ card -->

                                    <!-- card -->
                                    <div class="card">
                                        <h3 class="panel-title card-header">  
                                            <span style="width:60%" class="d-inline-block px-3">Add a new Payment       Method 
                                            </span>                                            
                                        </h3>
                                        <div class="panel-content card-body">
                                        <div>
                                            <h5 class="h5">Credit and Debit Cards</h5>
                                            <p>Wreckers Accepts all Major Credit and Debit Cards</p>
                                            <p>Enter your Card Information</p>
                                            <form class="form-addcardinfo">
                                                <div class="row">
                                                    <!-- col -->
                                                    <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <label>Name on Card</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="Name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/ col -->
                                                    <!-- col -->
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label>Card Number</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="Card Number">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/ col -->
                                                    <!-- col -->
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label class="d-block">Expiration Date</label>
                                                            <div class="input-group w-50 float-left">
                                                                <select class="form-control">
                                                                    <option>01</option>
                                                                    <option>02</option>
                                                                    <option>03</option>
                                                                    <option>04</option>
                                                                    <option>05</option>
                                                                    <option>06</option>
                                                                    <option>07</option>
                                                                    <option>08</option>
                                                                    <option>09</option>
                                                                    <option>10</option>
                                                                    <option>11</option>
                                                                    <option>12</option>
                                                                </select>
                                                            </div>
                                                            <div class="input-group w-50 float-left">
                                                                <select class="form-control">
                                                                    <option>2019</option>
                                                                    <option>2020</option>
                                                                    <option>2021</option>
                                                                    <option>2022</option>
                                                                    <option>2023</option>
                                                                    <option>2024</option>
                                                                    <option>2025</option>
                                                                    <option>2026</option>
                                                                    <option>2027</option>
                                                                    <option>2028</option>
                                                                    <option>2029</option>
                                                                    <option>2030</option>
                                                                </select>
                                                            </div>
                                                        </div>                                                
                                                    </div>
                                                    <!--/ col -->
                                                    <!-- col -->
                                                    <div class="col-lg-2">
                                                        <div class="form-group">
                                                            <label>CVV</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="CVV">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/ col -->
                                                    <!-- col -->
                                                    <div class="col-lg-12">
                                                        <button class="redbtn">Add your Card</button>
                                                    </div>
                                                    <!--/ col -->
                                                </div>
                                            </form>                                   
                                        </div>
                                        </div>
                                    </div>
                                    <!--/ card -->

                                </div>
                                <!--/ custom acccordian -->
                           </div>
                           <!--/ accordian -->
                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

    

    <!-- Add Payment method -->
<!-- The Modal -->
<div class="modal fade" id="addpaymentmethod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">        
            <!-- Modal Header -->
            <div class="modal-header">
            <h4 class="modal-title h5">Add Payment Method</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">           
                <ul class="list-group select-payment">
                    <li class="list-group-item select-card"><a href="javascript:void(0)"><span class="icon-credit-card"></span> Credit Card</a></li>
                    <li class="list-group-item"><a href="javascript:void(0)"><span class="icon-credit-card-alt"></span> Debit Card</a></li>
                    <li class="list-group-item"><a href="javascript:void(0)"><span class="icon-paypal"></span> Paypal</a></li>
                </ul>    
                <input id="selectpayment" type="submit" class="redbtn" value="Select Payment">              
            </div>        
            <!-- /modal body --> 
        </div>  
    </div>
</div>
<!--/ Add Payment method   -->




<!-- Add Payment -->
<!-- The Modal -->
<div class="modal fade" id="addpayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title h5">Add Credit Card or Debit Card</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">           
                <div class="form-group">
                    <label>Card Number</label>
                    <input type="text" class="form-control" placeholder="Card Number">
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Exp. Date</label>
                            <input type="text" class="form-control" placeholder="MM/YY">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Security Code</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Country</label>
                    <select class="form-control">
                        <option>Select Country</option>
                        <option>India</option>
                        <option>USA</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>ZIP Code</label>
                    <input type="text" class="form-control" placeholder="Zip Code">
                </div>

                <input type="submit" class="redbtn" value="Add Payment">            
            </div>        
            <!-- /modal body --> 
        </div>
    </div>
</div>
<!--/ Add Payment -->

<script>
    
//set button id on click to hide first modal
$("#selectpayment").on("click", function () {
    $('#addpaymentmethod').modal('hide');
});
//trigger next modal
$("#selectpayment").on("click", function () {
    $('#addpayment').modal('show');
});

</script>







</body>

</html>