<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Monthyly Statements </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
                <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->


                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <div class="db-pagetitle d-flex justify-content-between">
                        <article>
                            <h2 class="h5 fbold">Payment Widhdraws</h2>
                            <p class="pb-0">Lorem ipsum dolor sit amet.</p>
                        </article>                                           
                    </div>

                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="text-right"><a data-toggle="modal" data-target="#addpayment" href="javascript:void(0)" class="redbtn"> Request Payment</a></p>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Method</th>
                                <th scope="col">Status</th>
                                <th scope="col">Payment Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">17178</th>
                                    <td> $51</td>
                                    <td>Paypal</td>
                                    <td><span style="color:red;">Cancelled</span></td>
                                    <td> 02 Dec 2019 </td>
                                </tr>
                                <tr>
                                    <th scope="row">17178</th>
                                    <td> $51</td>
                                    <td>Paypal</td>
                                    <td><span class="fgreem">Success</span></td>
                                    <td> 02 Dec 2019 </td>
                                </tr>
                                <tr>
                                    <th scope="row">17178</th>
                                    <td> $51</td>
                                    <td>Paypal</td>
                                    <td><span style="color:red;">Cancelled</span></td>
                                    <td> 02 Dec 2019 </td>
                                </tr>
                                
                            </tbody>
                        </table>
                     
                           

                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

    <!-- Add Payment -->
    <!-- The Modal -->
  <div class="modal fade" id="addpayment">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title h5">Add Credit Card or Debit Card</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div class="form-group">
                <label>Card Number</label>
                <input type="text" class="form-control" placeholder="Card Number">
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Exp. Date</label>
                        <input type="text" class="form-control" placeholder="MM/YY">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Security Code</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Country</label>
                <select class="form-control">
                    <option>Select Country</option>
                    <option>India</option>
                    <option>USA</option>
                </select>
            </div>
            <div class="form-group">
                <label>ZIP Code</label>
                <input type="text" class="form-control" placeholder="Zip Code">
            </div>

            <input type="submit" class="redbtn" value="Add Payment">            
        </div>
        
        <!-- /modal body -->

      

    </div>
  </div>
    <!--/ Add Payment   -->


</body>

</html>