<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts Cart Items</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Cart Items</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-around">                 
                    
                    <!-- cart table -->
                    <div class="col-lg-12 table-cart" id="no-more-tables">
                        <table class="table table-striped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th style="width:8%" scope="col">Product</th>
                                    <th style="width:30%" scope="col">Description</th>
                                    <th style="width:25%" scope="col">Quantity</th>
                                    <th scope="col">Price</th>                                    
                                    <th scope="col">&nbsp;</th>        				
                                </tr>
                            </thead>
                                <tbody>
                                    <tr>
                                        <td data-title="Part Image"><img class="thumb" src="img/data/part01.jpg"></td>
                                        <td data-title="Part Name">
                                            <h5 class="h5 fbold">Part Name will be here</h5>
                                            <p class="pr-2 text-left">
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </small>
                                            </p>
                                            
                                        </td>
                                        <td data-title="Quantity" class="numeric">
                                            <!-- increase and decrease value -->
                                            <form class="value-highanddown">
                                                <div class="value-button" id="decrease" onclick="decreaseValue()" value="Decrease Value">-</div>
                                                <input type="number" id="number" value="0" />
                                                <div class="value-button" id="increase" onclick="increaseValue()" value="Increase Value">+</div>
                                            </form>
                                            <!--/ increase and decraqse value -->
                                            
                                        </td>
                                        <td data-title="Price" class="numeric">
                                            <p><span>$112.38</span></p>                                           
                                        </td>
                                        
                                        <td data-title="Total" class="numeric">
                                            <p class="pb-0 mb-0"><a class="fblue" href="javascript:void(0)"><span class="icon-trash-o"></span> Remove</a></p>
                                            <p><a class="fblue" href="javascript:void(0)"><span class="icon-floppy-o"></span> Save for later</a></p>
                                        </td>        				
                                    </tr>

                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <h5 class="h6 fblue">Shipping</h5>
                                            <ul class="cartlist-shipping">
                                                <li>
                                                    <input type="radio"> Delivery to my address
                                                </li>
                                                <li>
                                                    <input type="radio"> Deliver to Mechanic
                                                </li>
                                                <li>
                                                    <input type="radio"> Still looking for Mechanic
                                                </li>
                                                <li>
                                                    <small class="small">(Pay 50% and hold the part for 7 days)</small>
                                                </li>
                                            </ul>
                                        </td>
                                        <td>
                                            <div>                                                
                                                <div class="form-group">
                                                    <select class="form-control">
                                                        <option>Select Delivery Address</option>
                                                        <option>91, Allwyn Colony, Hydearabad (Home)</option>
                                                        <option>10-92, Hitech city road, Mahdapur(Office )</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </td>  
                                        <td class="text-center">  
                                            <p class="pb-0">Delivery Charges</p>                                           
                                            <p>$50.00</p>                                        
                                        </td>                                      
                                        <td class="text-center">
                                            <p class="pb-0">Total</p> 
                                            <p><span class="fbold h5">$200.00</span></p>
                                        
                                        </td>
                                    </tr>

                                    <tr>
                                        <td data-title="Part Image"><img class="thumb" src="img/data/part01.jpg"></td>
                                        <td data-title="Part Name">
                                            <h5 class="h5 fbold">Part Name will be here</h5>
                                            <p class="pr-2 text-left">
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </small>
                                            </p>
                                            
                                        </td>
                                        <td data-title="Quantity" class="numeric">
                                            <!-- increase and decrease value -->
                                            <form class="value-highanddown">
                                                <div class="value-button" id="decrease" onclick="decreaseValue()" value="Decrease Value">-</div>
                                                <input type="number" id="number" value="0" />
                                                <div class="value-button" id="increase" onclick="increaseValue()" value="Increase Value">+</div>
                                            </form>
                                            <!--/ increase and decraqse value -->
                                            
                                        </td>
                                        <td data-title="Price" class="numeric">
                                            <p><span>$112.38</span></p>                                           
                                        </td>
                                        
                                        <td data-title="Total" class="numeric">
                                            <p class="pb-0 mb-0"><a class="fblue" href="javascript:void(0)"><span class="icon-trash-o"></span> Remove</a></p>
                                            <p><a class="fblue" href="javascript:void(0)"><span class="icon-floppy-o"></span> Save for later</a></p>
                                        </td>        				
                                    </tr>

                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <h5 class="h6 fblue">Shipping</h5>
                                            <ul class="cartlist-shipping">
                                                <li>
                                                    <input type="radio"> Delivery to my address
                                                </li>
                                                <li>
                                                    <input type="radio"> Deliver to Mechanic
                                                </li>
                                                <li>
                                                    <input type="radio"> Still looking for Mechanic
                                                </li>
                                                <li>
                                                    <small class="small">(Pay 50% and hold the part for 7 days)</small>
                                                </li>
                                            </ul>
                                        </td>
                                        <td>
                                            <div>                                                
                                                <div class="form-group">
                                                    <select class="form-control">
                                                        <option>Select Delivery Address</option>
                                                        <option>91, Allwyn Colony, Hydearabad (Home)</option>
                                                        <option>10-92, Hitech city road, Mahdapur(Office )</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </td>  
                                        <td class="text-center">  
                                            <p class="pb-0">Delivery Charges</p>                                           
                                            <p>$50.00</p>                                        
                                        </td>                                      
                                        <td class="text-center">
                                            <p class="pb-0">Total</p> 
                                            <p><span class="fbold h5">$200.00</span></p>
                                        
                                        </td>
                                    </tr>
                                        			
                                </tbody>                                
                            </table>
                            <div class="row justify-content-end">
                                <div class="col-lg-8 text-right ">
                                    <article class="p-3 border mt-3">
                                        <h4 class="h4">Sub Total <span class="fbold fred">$400.00</span></h4>
                                        <p class="text-right h6 mb-0 pb-1">Tax: $25</p>
                                        <p class="text-right h6 mb-0">Delivery fee: $5</p>
                                        <h4 class="h4 fbold border-top py-2">Total <span class="fbold fred">$425.00</span></h4>
                                        
                                    </article>
                                    <div class="text-right pt-3">
                                        <p class="text-right"><input type="checkbox"> I Agree to all terms & conditions</p>
                                        <p class="text-right p-2 hightlate">Would you like us to find a cheapest mechanic to fix these parts for you? 
                                            <label class="radio-inline px-2"><input type="radio" name="optradio" checked>Yes</label>
                                            <label class="radio-inline"><input type="radio" name="optradio">No</label></p>
                                        </p>
                                       
                                           
                                        <a href="javascript:void(0)" class="redbtn">Continue Shopping</a>
                                        <a href="javascript:void(0)" class="redbtn">Checkout</a>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!--/ cart table -->
                </div>
                <!--/ row --> 

                <hr>

                <!--  save later row -->
                <div class="row">
                    <div class="col-lg-12 pb-3">
                        <h5 class="h4">Saved for later (4)</h5>
                    </div>
                   
                    <!-- product col -->
                    <div class="col-lg-3">
                        <div class="product-col">
                            <figure class="position-relative border-bottom">
                                <a href="javascript:void(0)"><img src="img/data/pro01.jpg" alt="" class="img-fluid"></a>
                                <span class="discount">-25%</span>                                
                            </figure>
                            <article class="p-2 text-center">
                                <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                <p class="text-center pt-3 addcartbtn">
                                    <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                    <a class="redbtn" href="javascript:void(0)">Remove</a>
                                </p>
                                <p class="price d-flex justify-content-between py-2 price">
                                    <span class="fred">$76.00</span>
                                    <span class="oldprice">$126.00</span>
                                </p>
                            </article>
                        </div>
                    </div>
                    <!--/ product col -->

                    <!-- product col -->
                    <div class="col-lg-3">
                        <div class="product-col">
                            <figure class="position-relative border-bottom">
                                <a href="javascript:void(0)"><img src="img/data/pro02.jpg" alt="" class="img-fluid"></a>
                                <span class="discount">-25%</span>                                
                            </figure>
                            <article class="p-2 text-center">
                                <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                <p class="text-center pt-3 addcartbtn">
                                    <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                    <a class="redbtn" href="javascript:void(0)">Remove</a>
                                </p>
                                <p class="price d-flex justify-content-between py-2 price">
                                    <span class="fred">$76.00</span>
                                    <span class="oldprice">$126.00</span>
                                </p>
                            </article>
                        </div>
                    </div>
                    <!--/ product col -->

                    <!-- product col -->
                    <div class="col-lg-3">
                        <div class="product-col">
                            <figure class="position-relative border-bottom">
                                <a href="javascript:void(0)"><img src="img/data/pro03.jpg" alt="" class="img-fluid"></a>
                                <span class="discount">-25%</span>                                
                            </figure>
                            <article class="p-2 text-center">
                                <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                <p class="text-center pt-3 addcartbtn">
                                    <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                    <a class="redbtn" href="javascript:void(0)">Remove</a>
                                </p>
                                <p class="price d-flex justify-content-between py-2 price">
                                    <span class="fred">$76.00</span>
                                    <span class="oldprice">$126.00</span>
                                </p>
                            </article>
                        </div>
                    </div>
                    <!--/ product col -->

                    <!-- product col -->
                    <div class="col-lg-3">
                        <div class="product-col">
                            <figure class="position-relative border-bottom">
                                <a href="javascript:void(0)"><img src="img/data/pro04.jpg" alt="" class="img-fluid"></a>
                                <span class="discount">-25%</span>                                
                            </figure>
                            <article class="p-2 text-center">
                                <h5 class="h6"><a href="javascript:void(0)">Chunk Cupim spare</a></h5>
                                <p class="text-center pt-3 addcartbtn">
                                    <a class="redbtn" href="javascript:void(0)">Add to Cart</a>
                                    <a class="redbtn" href="javascript:void(0)">Remove</a>
                                </p>
                                <p class="price d-flex justify-content-between py-2 price">
                                    <span class="fred">$76.00</span>
                                    <span class="oldprice">$126.00</span>
                                </p>
                            </article>
                        </div>
                    </div>
                    <!--/ product col -->
                   
                </div>
                <!--/ save later row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>