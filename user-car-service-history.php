<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
                <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->


                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <div class="db-pagetitle">
                        <article>
                            <h2 class="h5 fbold">My Car Service History</h2>
                            <p class="pb-0">My Wishlist Products and My Favourite Products</p>
                        </article>                        
                    </div>

                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <form>
                                <div class="form-group w-50">
                                    <label>VIN Number API</label>
                                    <input type="text" class="form-control" placeholder="Enter VIN Number">
                                </div>
                                <!-- <input type="submit" class="redbtn" value="Get Details"> -->
                            </form>
                            <div class="get-cardetails mt-2" id="getcardetails">
                                <dl class="row">
                                    <dt class="col-sm-3">Car Make:</dt>
                                    <dd class="col-sm-9">Car make Brand and Details</dd>

                                    <dt class="col-sm-3 text-truncate">Model & Year:</dt>
                                    <dd class="col-sm-9">Model Number here & 2018</dd>

                                    <dt class="col-sm-3">Engine Type:</dt>
                                    <dd class="col-sm-9">Petrol</dd>

                                    <dt class="col-sm-3">Number of Doors:</dt>
                                    <dd class="col-sm-9">4</dd>

                                    <dt class="col-sm-3">Colour:</dt>
                                    <dd class="col-sm-9">Black</dd>
                                </dl>
                            </div>

                            <!--table -->
                            <p class="text-right features-table">
                                <a class="d-inline-block px-2" href="javascript:void(0)"><span class="icon-share-alt"></span> Share</a>
                                <a class="d-inline-block px-2" href="javascript:void(0)"><span class="icon-copy"></span> Copy</a>
                                <a class="d-inline-block px-2" href="javascript:void(0)"><span class="icon-download"></span> Download</a>
                            </p>
                            <table class="table mt-3">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">S.No:</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Odo Meter Reading</th>
                                        <th scope="col">Service Performed</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Service  by</th>
                                        <th scope="col">Service Due Dt</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>12-12-2019</td>
                                        <td>1225005</td>
                                        <td>Good</td>
                                        <td>$250</td>
                                        <td>Mechanic Name</td>
                                        <td>12-01-2020</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>12-12-2019</td>
                                        <td>1225005</td>
                                        <td>Good</td>
                                        <td>$250</td>
                                        <td>Mechanic Name</td>
                                        <td>12-01-2020</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>12-12-2019</td>
                                        <td>1225005</td>
                                        <td>Good</td>
                                        <td>$250</td>
                                        <td>Mechanic Name</td>
                                        <td>12-01-2020</td>
                                    </tr>   
                                    <tr>
                                        <th scope="row">4</th>
                                        <td>12-12-2019</td>
                                        <td>1225005</td>
                                        <td>Good</td>
                                        <td>$250</td>
                                        <td>Mechanic Name</td>
                                        <td>12-01-2020</td>
                                    </tr>                               
                                </tbody>
                            </table>
                            <!--/ table -->
                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>