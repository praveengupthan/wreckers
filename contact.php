<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Contact</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row justify-content-between">
                   <!-- col -->
                   <div class="col-lg-4">
                       <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>

                       <table class="table-contact">
                           <tr>
                               <td><span class="icon-telephone icomoon"></span></td>
                               <td>0123456789</td>
                           </tr>
                           <tr>
                               <td><span class="icon-sent-mail icomoon"></span></td>
                               <td>support@wreckers.com</td>
                           </tr>
                           <tr>
                               <td><span class="icon-pin icomoon"></span></td>
                               <td>My Company, 42 avenue des Champs Elysées 75000 Paris Francess</td>
                           </tr>
                       </table>
                   </div>
                   <!--/ col -->

                   <!-- col -->
                   <div class="col-lg-7">
                        <h4 class="h5 fred">Reach Us</h4>
                        <form class="contact-form">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Write Your Name</label>
                                        <input type="text" class="form-control" placeholder="Your Name">
                                    </div>
                                </div>
                                <!--/col -->
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input type="text" class="form-control" placeholder="Write Your Phone Number">
                                    </div>
                                </div>
                                <!--/col -->
                            </div>
                            <!--/ row -->
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" placeholder="Your Email Address">
                                    </div>
                                </div>
                                <!--/col -->
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <input type="text" class="form-control" placeholder="Ex: About Spare Parts">
                                    </div>
                                </div>
                                <!--/col -->
                            </div>
                            <!--/ row -->
                             <!-- row -->
                             <div class="row">
                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Write Message</label>
                                        <textarea style="height:100px;" class="form-control" placeholder="Write Your Message"></textarea>
                                    </div>
                                </div>
                                <!--/col -->                               
                            </div>
                            <!--/ row -->
                             <!-- row -->
                             <div class="row">
                                <!-- col -->
                                <div class="col-lg-12">
                                    <input type="Submit" class="redbtn" value="Submit">
                                </div>
                                <!--/col -->                               
                            </div>
                            <!--/ row -->
                        </form>
                   </div>
                   <!--/ col -->
               </div>
               <!--/ row -->

            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>