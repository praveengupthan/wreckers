//steps




$(function () {
    $("#wizard").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        onStepChanging: function (event, currentIndex, newIndex) {
            if (newIndex === 1) {
                $('.wizard > .steps ul').addClass('step-2');
            } else {
                $('.wizard > .steps ul').removeClass('step-2');
            }

            if (newIndex === 2) {
                $('.wizard > .steps ul').addClass('step-3');
            } else {
                $('.wizard > .steps ul').removeClass('step-3');
            }

            if (newIndex === 3) {
                $('.wizard > .steps ul').addClass('step-4');
            } else {
                $('.wizard > .steps ul').removeClass('step-4');
            }

            return true;
        },
        labels: {
            finish: "Submit",
            next: "Continue",
            previous: "Back"
        }
    });
    // Custom Button Jquery Steps
    $('.forward').click(function () {
        $("#wizard").steps('next');
    })
    $('.backward').click(function () {
        $("#wizard").steps('previous');
    })
})
//steps ends 




//marquee logos 
$(function () {
    $('#marquee-vertical').marquee();
    $('#marquee-horizontal').marquee({
        direction: 'horizontal',
        delay: 0,
        timing: 30
    });
});
//marquee logos ends
//home categories slick
$('.catgories-home').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    centerMode: false,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 766,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 574,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

//home categories slick
$('.brands-home').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 8,
    slidesToScroll: 1,
    autoplay: true,
    centerMode: false,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 8,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 766,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 574,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

//home testimonials slick 

//home categories slick
$('.testimonial-slick').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    centerMode: false,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 766,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 574,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

//Task Detail slick
$('.task-slick').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    centerMode: false,
    responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 766,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 574,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});




//horizontal tab and vertical tab
$(document).ready(function () {
    $(document).ready(function () {
        //Horizontal Tab
        $('.parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
        // Child Tab
        $('#ChildVerticalTab_1').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true,
            tabidentify: 'ver_1', // The tab groups identifier
            activetab_bg: '#fff', // background color for active tabs in this group
            inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
            active_border_color: '#c1c1c1', // border color for active tabs heads in this group
            active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
        });
        //Vertical Tab
        $('.parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function (event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });

    //on scroll add class to header 

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('.fixed-top').addClass('fixed-theme');
        } else {
            $('.fixed-top').removeClass('fixed-theme');
        }
    });

    //on click top navigatgion window scroll top

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#movetop').fadeIn();
        } else {
            $('#movetop').fadeOut();
        }
    });

    //Click event to scroll to top
    $('#movetop').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    //bootstrap date picker 
    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4'
    });

    //show car details
    $('#getDetails').click(function () {
        $('#displayCarDetails').show();
    });
});


//simple gallery 

$(document).ready(function () {
    $('.gallery').simplegallery({
        galltime: 400,
        gallcontent: '.content',
        gallthumbnail: '.thumbnail',
        gallthumb: '.thumb'
    });

    //input file 
    document.querySelector("html").classList.add('js');
    var fileInput = document.querySelector(".input-file"),
        button = document.querySelector(".input-file-trigger"),
        the_return = document.querySelector(".file-return");
    button.addEventListener("keydown", function (event) {
        if (event.keyCode == 13 || event.keyCode == 32) {
            fileInput.focus();
        }
    });
    button.addEventListener("click", function (event) {
        fileInput.focus();
        return false;
    });
    fileInput.addEventListener("change", function (event) {
        the_return.innerHTML = this.value;
    });

});


//increase and descreae value 
function increaseValue() {
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('number').value = value;
}

function decreaseValue() {
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    value < 1 ? value = 1 : '';
    value--;
    document.getElementById('number').value = value;
}

//ask question toggle
$('#askqstnbtn').click(function () {
    $('#qstnForm').slideDown();
});

$('#hideQuestion').click(function () {
    $('#qstnForm').slideToggle();
});

$('#writereview').click(function () {
    $('.form-review').slideDown();
});


//jquery star rating
(function ($) {
    $.fn.addRating = function (options) {
        var obj = this;
        var settings = $.extend({
            max: 5,
            half: true,
            fieldName: 'rating',
            fieldId: 'rating',
            icon: 'star',
            selectedRatings: 0
        }, options);
        this.settings = settings;

        // create the stars
        for (var i = 1; i <= settings.max; i++) {
            var star = $('<i/>').addClass('material-icons').html(this.settings.icon + '_border').data('rating', i).appendTo(this).click(
                function () {
                    obj.setRating($(this).data('rating'));
                }
            ).hover(
                function (e) {
                    obj.showRating($(this).data('rating'), false);
                },
                function () {
                    obj.showRating(0, false);
                }
            );

        }
        $(this).append('<input type="hidden" name="' + settings.fieldName + '" id="' + settings.fieldId + '" value="' + settings.selectedRatings + '" />');

        obj.showRating(settings.selectedRatings, true);
    };

    $.fn.setRating = function (numRating) {
        var obj = this;
        $('#' + obj.settings.fieldId).val(numRating);
        obj.showRating(numRating, true);
    };

    $.fn.showRating = function (numRating, force) {
        var obj = this;
        if ($('#' + obj.settings.fieldId).val() == '' || force) {
            $(obj).find('i').each(function () {
                var icon = obj.settings.icon + '_border';
                $(this).removeClass('selected');

                if ($(this).data('rating') <= numRating) {
                    icon = obj.settings.icon;
                    $(this).addClass('selected');
                }
                $(this).html(icon);
            })
        }
    }

}(jQuery));

$(document).ready(function () {
    $('.rating').addRating();
})

//jquery star rating ends 



//accordian
// Hiding the panel content. If JS is inactive, content will be displayed
$('.panel-content').hide();

// Preparing the DOM

// -- Update the markup of accordion container 
$('.accordion').attr({
    role: 'tablist',
    multiselectable: 'true'
});

// -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
$('.panel-content').attr('id', function (IDcount) {
    return 'panel-' + IDcount;
});
$('.panel-content').attr('aria-labelledby', function (IDcount) {
    return 'control-panel-' + IDcount;
});
$('.panel-content').attr('aria-hidden', 'true');
// ---- Only for accordion, add role tabpanel
$('.accordion .panel-content').attr('role', 'tabpanel');

// -- Wrapping panel title content with a <a href="">
$('.panel-title').each(function (i) {

    // ---- Need to identify the target, easy it's the immediate brother
    $target = $(this).next('.panel-content')[0].id;

    // ---- Creating the link with aria and link it to the panel content
    $link = $('<a>', {
        'href': '#' + $target,
        'aria-expanded': 'false',
        'aria-controls': $target,
        'id': 'control-' + $target
    });

    // ---- Output the link
    $(this).wrapInner($link);

});

// Optional : include an icon. Better in JS because without JS it have non-sense.
$('.panel-title a').append('<span class="icon">+</span>');

// Now we can play with it
$('.panel-title a').click(function () {

    if ($(this).attr('aria-expanded') == 'false') { //If aria expanded is false then it's not opened and we want it opened !

        // -- Only for accordion effect (2 options) : comment or uncomment the one you want

        // ---- Option 1 : close only opened panel in the same accordion
        //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
        $(this).parents('.accordion').find('[aria-expanded=true]').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200).attr('aria-hidden', 'true');

        // Option 2 : close all opened panels in all accordion container
        //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);

        // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
        $(this).attr('aria-expanded', true).addClass('active').parent().next('.panel-content').slideDown(200).attr('aria-hidden', 'false');

    } else { // The current panel is opened and we want to close it

        $(this).attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200).attr('aria-hidden', 'true');;

    }
    // No Boing Boing
    return false;
});
//accordian ends



//canvas for dashboard
window.onload = function () {

    var options = {
        animationEnabled: true,
        title: {
            text: "Monthly Sales - 2017"
        },
        axisX: {
            valueFormatString: "MMM"
        },
        axisY: {
            title: "Sales (in USD)",
            prefix: "$",
            includeZero: false
        },
        data: [{
            yValueFormatString: "$#,###",
            xValueFormatString: "MMMM",
            type: "spline",
            dataPoints: [{
                    x: new Date(2017, 0),
                    y: 25060
                },
                {
                    x: new Date(2017, 1),
                    y: 27980
                },
                {
                    x: new Date(2017, 2),
                    y: 33800
                },
                {
                    x: new Date(2017, 3),
                    y: 49400
                },
                {
                    x: new Date(2017, 4),
                    y: 40260
                },
                {
                    x: new Date(2017, 5),
                    y: 33900
                },
                {
                    x: new Date(2017, 6),
                    y: 48000
                },
                {
                    x: new Date(2017, 7),
                    y: 31500
                },
                {
                    x: new Date(2017, 8),
                    y: 32300
                },
                {
                    x: new Date(2017, 9),
                    y: 42000
                },
                {
                    x: new Date(2017, 10),
                    y: 52160
                },
                {
                    x: new Date(2017, 11),
                    y: 49400
                }
            ]
        }]
    };
    $("#chartContainer").CanvasJSChart(options);

}
//canvas for dashboard ends 


$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});


//navbar dropdown on hover
// const $dropdown = $(".dropdown");
// const $dropdownToggle = $(".dropdown-toggle");
// const $dropdownMenu = $(".dropdown-menu");
// const showClass = "show";

// $(window).on("load resize", function () {
//     if (this.matchMedia("(min-width: 768px)").matches) {
//         $dropdown.hover(
//             function () {
//                 const $this = $(this);
//                 $this.addClass(showClass);
//                 $this.find($dropdownToggle).attr("aria-expanded", "true");
//                 $this.find($dropdownMenu).addClass(showClass);
//             },
//             function () {
//                 const $this = $(this);
//                 $this.removeClass(showClass);
//                 $this.find($dropdownToggle).attr("aria-expanded", "false");
//                 $this.find($dropdownMenu).removeClass(showClass);
//             }
//         );
//     } else {
//         $dropdown.off("mouseenter mouseleave");
//     }
// });


//home page logos slider



//range slider
function getVals() {
    // Get slider values
    var parent = this.parentNode;
    var slides = parent.getElementsByTagName("input");
    var slide1 = parseFloat(slides[0].value);
    var slide2 = parseFloat(slides[1].value);
    // Neither slider will clip the other, so make sure we determine which is larger
    if (slide1 > slide2) {
        var tmp = slide2;
        slide2 = slide1;
        slide1 = tmp;
    }

    var displayElement = parent.getElementsByClassName("rangeValues")[0];
    displayElement.innerHTML = "$ " + slide1 + "k - $" + slide2 + "k";
}

window.onload = function () {
    // Initialize Sliders
    var sliderSections = document.getElementsByClassName("range-slider");
    for (var x = 0; x < sliderSections.length; x++) {
        var sliders = sliderSections[x].getElementsByTagName("input");
        for (var y = 0; y < sliders.length; y++) {
            if (sliders[y].type === "range") {
                sliders[y].oninput = getVals;
                // Manually trigger event first time to display values
                sliders[y].oninput();
            }
        }
    }
}


$('#replybtn').click(function () {
    $('#showreply').slideToggle();
});


$(function () {
    $('#combostar').on('change', function () {
        $('#starcount').text($(this).val());
    });
    $('#combostar').combostars();
});


$(document).ready(function(){
    $("#editcard").click(function(){
        $("#editcard-details").show();
        $("#card-details").hide();
    });
    $("#savecard").click(function(){
        $("#editcard-details").hide();
        $("#card-details").show();
    });

    //on focus search

    $("#search-input").focus(function(){
        $("#search-auto").show();
    });

    $("#close-search").click(function(){
        $("#search-auto").hide();
    });


});



