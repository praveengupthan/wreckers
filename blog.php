<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Blogs</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row blog-row border-bottom pb-2 mb-2">
                   <!-- col -->
                   <div class="col-lg-3">
                       <a href="blog-detail.php"><img src="img/data/blog01.jpg" alt="" class="img-fluid"></a>
                   </div>
                   <!--/ col -->
                   <!-- col -->
                   <div class="col-lg-9 align-self-center">                       
                        <h2 class="h5 fred"><a class="fred" href="blog-detail.php">8 Things to keep in mind while driving in Fog</a></h2>
                        <p>It is that time of the year again, especially for the people in the northern part of India, when the visibility drops suddenly with all the dust and smoke staying close to the ground, making the days very very gloomy. </p>

                        <p class="blog-extra">
                            <span class="extraspan"><span class="icon-calendar icomoon"></span> Oct 26, 2015 5:37:57 PM</span>

                            <span class="extraspan"><span class="icon-user-silhouette icomoon"></span> By Manish</span>

                            <span class="extraspan"><span class="icon-wechat icomoon"></span> <a href="javascript:void(0)">20 Comments</a> </span>
                        </p>
                   </div>
                   <!--/ col -->
               </div>
               <!--/ row -->

                <!-- row -->
                <div class="row blog-row border-bottom pb-2 mb-2">
                   <!-- col -->
                   <div class="col-lg-3">
                       <a href="blog-detail.php"><img src="img/data/blog02.jpg" alt="" class="img-fluid"></a>
                   </div>
                   <!--/ col -->
                   <!-- col -->
                   <div class="col-lg-9 align-self-center">                       
                        <h2 class="h5 fred"><a class="fred" href="blog-detail.php">Journey So Far</a></h2>
                        <p>So it’s been about 15 months since we started hatching the idea of Parts Big Boss; and about 3 months since the website is fully operational, and it feels good! It’s been hard work and tiring work but all of it is so much fun that we can’t stop doing it....We’d love to hear from you on whether these features are helping you or not. You can tell us anything and everything about your experience with us so far. </p>

                        <p class="blog-extra">
                            <span class="extraspan"><span class="icon-calendar icomoon"></span> Oct 26, 2015 5:37:57 PM</span>

                            <span class="extraspan"><span class="icon-user-silhouette icomoon"></span> By Manish</span>

                            <span class="extraspan"><span class="icon-wechat icomoon"></span> <a href="javascript:void(0)">20 Comments</a> </span>
                        </p>
                   </div>
                   <!--/ col -->
               </div>
               <!--/ row -->

                <!-- row -->
                <div class="row blog-row border-bottom pb-2 mb-2">
                   <!-- col -->
                   <div class="col-lg-3">
                       <a href="blog-detail.php"><img src="img/data/blog01.jpg" alt="" class="img-fluid"></a>
                   </div>
                   <!--/ col -->
                   <!-- col -->
                   <div class="col-lg-9 align-self-center">                       
                        <h2 class="h5 fred"><a class="fred" href="blog-detail.php">8 Emergency Items you should keep in your car for your next road trip!</a></h2>
                        <p>Travelling by road is fun and most memorable as compared to other modes of transport......I would advice you to keep these emergency items handy for your next road trip and save yourself from the hassles of getting stuck in the middle of nowhere! </p>

                        <p class="blog-extra">
                            <span class="extraspan"><span class="icon-calendar icomoon"></span> Oct 26, 2015 5:37:57 PM</span>

                            <span class="extraspan"><span class="icon-user-silhouette icomoon"></span> By Manish</span>

                            <span class="extraspan"><span class="icon-wechat icomoon"></span> <a href="javascript:void(0)">20 Comments</a> </span>
                        </p>
                   </div>
                   <!--/ col -->
               </div>
               <!--/ row -->

                <!-- row -->
                <div class="row blog-row border-bottom pb-2 mb-2">
                   <!-- col -->
                   <div class="col-lg-3">
                       <a href="blog-detail.php"><img src="img/data/blog02.jpg" alt="" class="img-fluid"></a>
                   </div>
                   <!--/ col -->
                   <!-- col -->
                   <div class="col-lg-9 align-self-center">                       
                        <h2 class="h5 fred"><a class="fred" href="blog-detail.php">Meet Rahul and His Car Repair Woes</a></h2>
                        <p>Meet Rahul and his Car, which is more than just a vehicle for him.  Somebody damaged his car while he was out to get coffee. Find out what he did to resolve the problem. Watch the video now! </p>

                        <p class="blog-extra">
                            <span class="extraspan"><span class="icon-calendar icomoon"></span> Oct 26, 2015 5:37:57 PM</span>

                            <span class="extraspan"><span class="icon-user-silhouette icomoon"></span> By Manish</span>

                            <span class="extraspan"><span class="icon-wechat icomoon"></span> <a href="javascript:void(0)">20 Comments</a> </span>
                        </p>
                   </div>
                   <!--/ col -->
               </div>
               <!--/ row -->

                <!-- row -->
                <div class="row blog-row border-bottom pb-2 mb-2">
                   <!-- col -->
                   <div class="col-lg-3">
                       <a href="blog-detail.php"><img src="img/data/blog01.jpg" alt="" class="img-fluid"></a>
                   </div>
                   <!--/ col -->
                   <!-- col -->
                   <div class="col-lg-9 align-self-center">                       
                        <h2 class="h5 fred"><a class="fred" href="blog-detail.php">Put an end to your Car-repair woes. Parts Big Boss is here!!</a></h2>
                        <p>We all buy the best cars thinking it will solve all our commuting troubles, provide us with pleasurable journeys and a life time of memories. But that joy very soon converts into misery, when we exhaust our free service with the service centres.The story repeats almost every single time.</p>

                        <p class="blog-extra">
                            <span class="extraspan"><span class="icon-calendar icomoon"></span> Oct 26, 2015 5:37:57 PM</span>

                            <span class="extraspan"><span class="icon-user-silhouette icomoon"></span> By Manish</span>

                            <span class="extraspan"><span class="icon-wechat icomoon"></span> <a href="javascript:void(0)">20 Comments</a> </span>
                        </p>
                   </div>
                   <!--/ col -->
               </div>
               <!--/ row -->

            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>