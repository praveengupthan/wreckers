<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shipping Details </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
                <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->


                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <div class="db-pagetitle d-flex justify-content-between">
                        <article>
                            <h2 class="h5 fbold">Shipping Details</h2>                           
                        </article>                                           
                    </div>

                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12">
                           <div class="whitebox mb-2"> 
                                <p>Tick one or more from the following</p>
                                <h6 class="h6"><input type="checkbox"> Do not wish to do self-delivery.  (user third party shipment)</h6>

                           </div>
                         <div class="whitebox mb-2">
                         <h6 class="h6"><input type="checkbox"> Wish to provide delivery in some regions.  (Recommended to get more sales)</h6>
                                <p>(Fill in the radius with price where you wish to offer free or small amount of charge)</p>
                               <!-- row -->
                           <div class="row">
                               <!-- col -->
                               <div class="col-lg-12">
                                    <h6 class="h6"><input type="checkbox"> Tick if you want to offer free Delivery (High recommended)</h6>  
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Enter the Distance</th>
                                                <th>Enter Shipping Charges</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input type="text" placeholder="Ex: 0.5Km">
                                                </td>
                                                <td>
                                                    <input type="text" placeholder="Ex: $20">
                                                </td>
                                                <td>
                                                    <input type="checkbox"> Offer free
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)"><span class="icon-plus-circle"></span> Add</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" placeholder="Ex: 0.5Km">
                                                </td>
                                                <td>
                                                    <input type="text" placeholder="Ex: $20">
                                                </td>
                                                <td>
                                                    <input type="checkbox"> Offer free
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)"><span class="icon-plus-circle"></span> Add</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" placeholder="Ex: 0.5Km">
                                                </td>
                                                <td>
                                                    <input type="text" placeholder="Ex: $20">
                                                </td>
                                                <td>
                                                    <input type="checkbox"> Offer free
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)"><span class="icon-plus-circle"></span> Add</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <form>                                       
                                        <p>Third party shipping over 
                                            <span><input type="text" class="form-control d-inline" style="width:50px; text-align:center;"></span>
                                            Kms</p>
                                    </form>

                               </div>
                               <!--/ col -->

                             
                           </div>
                           <!--/ row -->   
                         </div>
                           
                         <div class="whitebox">                           
                         <h6 class="h6"><input type="checkbox"> Offer Pickup if, Yes</h6>
                           <p>Mention your store timings for pickup</p>
                           <p>Provide drop downs form timings frm 0-24 hrs and closed options</p>

                           <table class="table table-bordered">
                               <tr>
                                   <td>Week Name</td>
                                   <td>From Hours</td>
                                   <td>To Hours</td>
                               </tr>
                                <tr>
                                    <td>Monday</td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 AM</option>
                                                <option>01.00 AM</option>
                                                <option>02.00 AM</option>
                                                <option>03.00 AM</option>
                                                <option>04.00 AM</option>
                                                <option>05.00 AM</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 PM</option>
                                                <option>01.00 PM</option>
                                                <option>02.00 PM</option>
                                                <option>03.00 PM</option>
                                                <option>04.00 PM</option>
                                                <option>05.00 PM</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tuesday</td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 AM</option>
                                                <option>01.00 AM</option>
                                                <option>02.00 AM</option>
                                                <option>03.00 AM</option>
                                                <option>04.00 AM</option>
                                                <option>05.00 AM</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 PM</option>
                                                <option>01.00 PM</option>
                                                <option>02.00 PM</option>
                                                <option>03.00 PM</option>
                                                <option>04.00 PM</option>
                                                <option>05.00 PM</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Wednesday</td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 AM</option>
                                                <option>01.00 AM</option>
                                                <option>02.00 AM</option>
                                                <option>03.00 AM</option>
                                                <option>04.00 AM</option>
                                                <option>05.00 AM</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 PM</option>
                                                <option>01.00 PM</option>
                                                <option>02.00 PM</option>
                                                <option>03.00 PM</option>
                                                <option>04.00 PM</option>
                                                <option>05.00 PM</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Thursday</td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 AM</option>
                                                <option>01.00 AM</option>
                                                <option>02.00 AM</option>
                                                <option>03.00 AM</option>
                                                <option>04.00 AM</option>
                                                <option>05.00 AM</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 PM</option>
                                                <option>01.00 PM</option>
                                                <option>02.00 PM</option>
                                                <option>03.00 PM</option>
                                                <option>04.00 PM</option>
                                                <option>05.00 PM</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Friday</td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 AM</option>
                                                <option>01.00 AM</option>
                                                <option>02.00 AM</option>
                                                <option>03.00 AM</option>
                                                <option>04.00 AM</option>
                                                <option>05.00 AM</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 PM</option>
                                                <option>01.00 PM</option>
                                                <option>02.00 PM</option>
                                                <option>03.00 PM</option>
                                                <option>04.00 PM</option>
                                                <option>05.00 PM</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Saturday</td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 AM</option>
                                                <option>01.00 AM</option>
                                                <option>02.00 AM</option>
                                                <option>03.00 AM</option>
                                                <option>04.00 AM</option>
                                                <option>05.00 AM</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option>12.00 PM</option>
                                                <option>01.00 PM</option>
                                                <option>02.00 PM</option>
                                                <option>03.00 PM</option>
                                                <option>04.00 PM</option>
                                                <option>05.00 PM</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sudnday</td>
                                    <td>Closed</td>
                                    <td>&nbsp;</td>
                                </tr>
                           </table>
                         </div>
                     
                           

                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->   

</body>

</html>