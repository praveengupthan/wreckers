<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chagne Your Membership Plan </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row">
                <!-- left navigation -->
                <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <!-- page title -->
                    <div class="db-pagetitle">
                        <article>
                            <h2 class="h5 fbold">Chagne Your Membership Plan</h2>
                            <p>Your Current Membership <span class="fblue fbold h5">Gold</span></p>
                        </article>                        
                    </div>
                    <!--/ page title -->

                    <!-- page body -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-6 align-self-center">
                            <h4 class="h5 fbold">Current Plan Details </h4>
                            <ul class="list-items">
                                <li>Commission: 100%</li>
                                <li>Eligible for constant discounts</li>
                                <li>Subscription fee paid: <span class="fblue fbold">$120</span></li>
                            </ul>
                            <!--/ page body -->
                            <h4 class="h6 fblue mb-3">You can upgrade your plan at any time </h4>
                            <a href="javascript:void(0)" class="redbtn">Change Your Membership plan</a>
                        </div>
                        <!--/ col -->

                        <!-- col -->
                        <div class="col-lg-6">
                            <table class="table mt-3">
                                <tbody>
                                    <tr>
                                        <td>Gold Membership</td>
                                        <td>$120.00</td>                                        
                                    </tr>
                                    <tr>
                                        <td>Commission</td>
                                        <td>10%</td>                                        
                                    </tr>
                                    <tr>
                                        <td>Tax GST</td>
                                        <td>$12</td>                                        
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            Estimated Total
                                            <p>Auto Renewal on December 22, 2019</p>
                                        </th>
                                        <td>$132</td>                                        
                                    </tr>
                                    <tr>
                                        <td>Visa Ending in 8908 expires 12/22 <a class="fblue fbold" href="javascript:void(0)"> Edit</a></td>
                                        <td><span class="icon-cc-visa"></span></td>                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--/ col -->
                    </div>

                 
                   
                       

                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>