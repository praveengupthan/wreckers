<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row">
                <!-- left navigation -->
                <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <!-- page title -->
                    <div class="db-pagetitle">
                        <article>
                            <h2 class="h5 fbold">User Name will be here</h2>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Est, itaque?</p>
                        </article>                        
                    </div>
                    <!--/ page title -->

                    <div class="whitebox">
                        <!-- row -->
                        <div class="row justify-content-center">
                            <div class="col-lg-4 pic text-center">
                                <img  src="img/data/custimg01.jpg" class="img-fluid profilepic">
                                <div class="upload-btn-wrapper">
                                    <button class="whitebtn mx-auto">Upload a Picture</button>
                                    <input type="file" name="myfile" />
                                </div>
                            </div>
                        </div>
                        <!--/ row -->

                        <!-- form -->
                        <form>
                            <!-- row -->
                            <div class="row">
                                <!-- col 6-->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" placeholder="Your First Name" class="form-control">
                                    </div>
                                </div>
                                <!--/ col 6-->
                                 <!-- col 6-->
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" placeholder="Your Last Name" class="form-control">
                                    </div>
                                </div>
                                <!--/ col 6-->
                            </div>
                            <!--/ row -->
                            <!-- row -->
                            <div class="row">                                
                                 <!-- col 6-->
                                 <div class="col-lg-6 align-self-center">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label class="container-new">Male
                                            <input type="radio" checked="checked" name="radio">
                                            <span class="checkmark"></span>
                                            </label>                                   
                                         </div>
                                        <div class="col-lg-5">
                                            <label class="container-new">Fe-Male
                                                <input type="radio" checked="checked" name="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col 6-->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Date of Birth</label>
                                        <input id="datepicker" type="text" placeholder="Select Date" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <!--/ row -->

                            <!-- row -->
                            <div class="row">
                                <!-- col 6-->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="text" placeholder="Phone Number" class="form-control">
                                    </div>
                                </div>
                                <!--/ col 6-->
                                 <!-- col 6-->
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input type="text" placeholder="Email Address" class="form-control">
                                    </div>
                                </div>
                                <!--/ col 6-->
                            </div>
                            <!--/ row -->

                            <!-- row -->
                            <div class="row">
                                <!-- col 6-->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Address Line 1</label>
                                        <input type="text" placeholder="House Number, Street Number" class="form-control">
                                    </div>
                                </div>
                                <!--/ col 6-->
                                 <!-- col 6-->
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Address Line 2</label>
                                        <input type="text" placeholder="Location and Land mark" class="form-control">
                                    </div>
                                </div>
                                <!--/ col 6-->
                            </div>
                            <!--/ row -->

                             <!-- row -->
                             <div class="row">
                                <!-- col 6-->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Country</label>
                                       <select class="form-control">
                                            <option>Country</option>
                                       </select>
                                    </div>
                                </div>
                                <!--/ col 6-->
                                 <!-- col 6-->
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>State</label>
                                        <select class="form-control">
                                            <option>State</option>
                                       </select>
                                    </div>
                                </div>
                                <!--/ col 6-->
                            </div>
                            <!--/ row -->

                             <!-- row -->
                             <div class="row">
                                <!-- col 6-->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>City</label>
                                       <select class="form-control">
                                            <option>City</option>
                                       </select>
                                    </div>
                                </div>
                                <!--/ col 6-->
                                 <!-- col 6-->
                                 <div class="col-lg-6">
                                     <div class="form-group">
                                        <label>Pin Number</label>
                                        <input type="text" placeholder="Ex:522265" class="form-control">
                                    </div>
                                </div>                               
                                <!--/ col 6-->
                                </div>
                                <!--/ row -->  
                                <input type="submit" class="whitebtn" value="Submit">
                                <input type="submit" class="whitebtn" value="Cancel">
                            </div>



                        </form>
                        <!-- form -->
                       

                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>