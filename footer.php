<footer class="position-relative">
        <!-- top container -->
        <div class="container">
            <!-- row -->
            <div class="row py-4">
                <!-- col -->
                <div class="col-lg-3">
                    <h6> Quick Links </h6>
                    <ul class="footer-links">
                        <li><a href="javascript:void(0)">Blog</a></li>
                        <li><a href="javascript:void(0)">FAQs</a></li>
                        <li><a href="javascript:void(0)">Payment</a></li>
                        <li><a href="javascript:void(0)">Shipment</a></li>
                        <li><a href="javascript:void(0)">Where is my order?</a></li>
                        <li><a href="javascript:void(0)">Return policy</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3">
                    <h6> My Account</h6>
                    <ul class="footer-links">
                        <li><a href="javascript:void(0)">Your Account</a></li>
                        <li><a href="javascript:void(0)">Information</a></li>
                        <li><a href="javascript:void(0)">Addresses</a></li>
                        <li><a href="javascript:void(0)">Discount</a></li>
                        <li><a href="javascript:void(0)">Orders History</a></li>
                        <li><a href="javascript:void(0)">Order Tracking</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3">
                    <h6> Information</h6>
                    <ul class="footer-links">
                        <li><a href="javascript:void(0)">Site Map</a></li>
                        <li><a href="javascript:void(0)">Search Terms</a></li>
                        <li><a href="javascript:void(0)">Advanced Search</a></li>
                        <li><a href="javascript:void(0)">About Us</a></li>
                        <li><a href="javascript:void(0)">Contact Us</a></li>
                        <li><a href="javascript:void(0)">Suppliers</a></li>
                    </ul>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-3">
                    <h6> Get Mobile App</h6>
                    <div class="map-thumb">
                        <a href="javascript:void(0)">
                            <img src="img/Google-Play-Store-Logo.png">
                        </a>
                        <a href="javascript:void(0)">
                            <img src="img/itunes-app-store-logo.png">
                        </a>                        
                    </div>
                    <p>We Accept all Credit / Debit Cards</p>
                    <img src="img/Essential-Minimal-Payment-Icons.png" class="img-fluid">                   
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ top container -->

        <!-- bottom footer -->
        <div class="bottom-footer">

        
            <!-- container -->
            <div class="container">
                <div class="row">
                    <!-- col -->
                    <div class="col-sm-6 col-12">
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="javascript:void(0)" target="_blank" class="nav-link fb"><span
                                        class="icon-facebook icomoon"></span></a>
                            </li>
                            <li class="nav-item">
                                <a href="javascript:void(0)" target="_blank" class="nav-link twi"><span
                                        class="icon-twitter icomoon"></span></a>
                            </li>
                            <li class="nav-item"s>
                                <a href="javascript:void(0)" target="_blank" class="nav-link link"><span
                                        class="icon-linkedin icomoon"></span></a>
                            </li>
                        </ul>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-sm-6 col-12 text-right align-self-center xs-text-center">
                        <p>All Rirhgs Reserved © Wreckers Parts 2020</p>
                    </div>
                    <!--/ col -->
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ bottom footer -->
        <a id="movetop" class="movetop" href="javascript:void(0)"><span class="icon-arrow-up"></span></a>
    </footer>

     <!-- script files -->
    <?php include 'footer-scripts.php' ?>

    <!-- save search -->
    <!-- The Modal -->
  <div class="modal fade" id="savesearch">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title h5">Let's get Started</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">      

            <div class="form-group">
                <label>Name your Search</label>
                <input type="text" class="form-control" data-role="tagsinput" value="Toyota,Tata,Hyndai">            
            </div>
            
            <!-- div -->                   
                <div class="text-right">
                    <p>Notify me</p>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline1">Yes</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline2">No</label>
                    </div>
                    <input type="submit" class="redbtn" value="Submit">
                </div>           
            <!--/ div -->

            <!-- saved searches -->
            <div class="saved-searches bordered">
                <p>Saved Searches</p>

                <ul class="savedarticles">
                    <li>Toyota <span class="icon-close icomoon"></span></li>
                    <li>Honda <span class="icon-close icomoon"></span></li>
                    <li>Tata <span class="icon-close icomoon"></span></li>
                </ul>

                <!-- div -->                   
                <div class="text-right">
                    <p>Notify me</p>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline1">Yes</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline2">No</label>
                    </div>                   
                </div>           
            <!--/ div -->


            
            </div>
            <!--/ saved searches -->
        </div>
    </div>
  </div>
  </div>
    <!--/ save search -->




    
    