<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Task Detail</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">        
        <!-- sub page body -->
        <div class="subpage-body pt-0">

           <?php include 'taskdetail-nav.php' ?>
            <!-- container -->
            <div class="container taskdetail-container">
                <!-- row -->
                <div class="row">
                    <!-- top search filters -->
                    <div class="col-lg-12">
                                             
                
                <!-- row left and right -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-lg-4">
                        <div class="left-task"> 
                            <!-- request col -->
                            <div class="request-col mb-2 requestactive">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                            <!-- request col -->
                            <div class="request-col mb-2">
                                <h2 class="d-flex justify-content-between">
                                    <span>Request of Bumper</span>
                                    <span class="fbold fred price">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> ASAP</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                            <!-- request col -->
                            <div class="request-col mb-2 expired-request">
                                <h2 class="d-flex justify-content-between">
                                    <span>Request of Bumper</span>
                                    <span class="fbold fred price">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fred align-self-center">Closed</span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> 10-01-2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                            <!-- request col -->
                            <div class="request-col mb-2 expired-request" disabled>
                                <h2 class="d-flex justify-content-between">
                                    <span>Request of Bumper</span>
                                    <span class="fbold fred price">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fred align-self-center">Expired</span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> 10-01-2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                             <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                             <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                             <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                            <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                             <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                             <!-- request col -->
                             <div class="request-col mb-2 ">
                                <h2 class="d-flex justify-content-between">
                                    <span>Fix my Bumper</span>
                                    <span class="fbold fred price h5">$ 45</span>
                                </h2>
                                <div class="row address-block">
                                    <div class="col-lg-8 align-self-center">
                                        <p><span class="fbold">Mobile</span></p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open <small class="small fgray">3 Offers</small></span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> Mon 5, Dec 2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->
                           
                        </div>
                    </div>
                    <!--/ left col -->

                    <!-- right col -->
                    <div class="col-lg-8">
                        <!-- task detail right -->
                       <div class="task-right task-detail">
                          <!-- white block -->
                          <div class="whitebox">
                              <!-- steps row -->
                              <div class="row">
                                  <!-- col 8 -->
                                  <div class="col-lg-8 align-self-center">
                                    <!-- progress bar -->
                                    <ul class="progressbar">
                                        <li class="active">Opened</li>
                                        <li>Assigned</li>
                                        <li>Closed</li>                                            
                                    </ul>
                                    <!--=/ progress bar -->
                                  </div>
                                  <!--/ col 8-->
                                  <!-- col 4 -->
                                  <div class="col-lg-4 text-center align-self-center">
                                    <div class="budgetblock">
                                        <h2 class="h6 fgray text-uppercase">Task Budget <span class="fred fbold h2 d-block">$45</span></h2>
                                        <a class="redbtn btnxl d-inline-block" href="taskdetail-makeanoffer.php">Make an Offer</a>
                                    </div>
                                  </div>
                                  <!--/ col 4 -->
                              </div>
                              <!--/ steps row -->

                              <!-- user row -->
                              <div class="row py-3">
                                  <!-- col 8-->
                                  <div class="col-lg-8">
                                    <div class="d-flex p-3">
                                        <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                        <article class="px-4 align-self-center">
                                            <h3 class="h6 mb-0 pb-0">
                                                <a href="javascript:void(0)">User Name will be here</a>
                                            </h3>
                                            <ul class="review-rating nav">
                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                            </ul>
                                            <p class="fgray pb-0">Member Since 1925</p>
                                        </article>
                                    </div>
                                  </div>
                                  <!--/ col 8-->
                                  <!-- col 4-->
                                  <div class="col-lg-4 text-right align-self-center">
                                     
                                  </div>
                                  <!--/ col 4-->
                              </div>
                              <!--/ user row -->

                              <!-- social row -->
                              <div class="row social-row py-3 border-top">
                                <!-- col 4-->
                                <div class="col-lg-4 ">
                                    <div class="d-flex justify-content-center">
                                        <p class="pr-2 align-self-center pb-0">Share Now</p>                                       
                                        <a href="javascript:void(0)" target="_blank"><span class="icon-facebook icomoon"></span></a>
                                        <a href="javascript:void(0)" target="_blank"><span class="icon-twitter icomoon"></span></a>
                                        <a href="javascript:void(0)" target="_blank"><span class="icon-linkedin icomoon"></span></a>
                                       
                                    </div>
                                </div>
                                <!--/ col 4 -->
                                <!-- col 4-->
                                <div class="col-lg-4">
                                    <div class="d-flex justify-content-center">
                                        <p class="pr-2 align-self-center pb-0">Follow</p>                                        
                                        <a class="align-self-center pb-0" href="javascript:void(0)" target="_blank"><span class="icon-heart icomoon"></span></a>        
                                    </div>
                                </div>
                                <!--/ col 4 -->

                                <!-- col 4-->
                                <div class="col-lg-4 text-center">
                                    <a class="fmed" href="javascript:void(0)">Post a Smilar Task</a>
                                </div>
                                <!--/ col 4-->
                              </div>
                              <!--/ social row -->
                          </div>
                          <!--/ white block -->

                          <!-- white block -->
                          <div class="whitebox">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <h4 class="h4 pb-0 fbold">Need a Bumper</h4>
                                        <p><small class="small">Location Name, Mobile</small></p>
                                    </div>

                                    <span class="fgray">Due date: ASAP</span>
                                </div>
                                <div class="py-3">
                                    <h5 class=" h6 fred fbold">Description</h5>
                                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Velit sapiente nihil possimus laudantium soluta vero molestiae nisi et inventore nobis ipsa <a href="javascript:void(0)" class="fgray">more > </a></p>
                                </div>

                                <!-- slick slider -->
                                <div class="task-slick custom-slick toptaskslick">
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part01.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part02.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part03.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part04.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part05.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                    <!-- slide -->
                                    <div class="slider">
                                        <figure>
                                            <img src="img/data/part06.jpg" alt="">
                                        </figure>
                                    </div>
                                    <!--/ slide-->
                                </div>
                                <!--/ slick slider -->                               
                          </div>
                          <!--/ white block -->

                          <!-- questions block -->
                          <div class="whitebox qandans cust-tab">

                                <!-- tab-->
                                <div class="parentHorizontalTab">
                                    <ul class="resp-tabs-list hor_1 nav justify-content-center">
                                        <li>Offers</li>
                                        <li>Questions (20)</li>
                                    </ul>
                                    <!-- tab container -->
                                    <div class="resp-tabs-container hor_1">
                                        <!-- offers -->
                                        <div>
                                            <h4 class="h5">Offers (20)</h4>
                                            <!-- chat message -->
                                            <div class="row justify-content-end py-2 border-bottom">
                                                <div class="col-lg-12">
                                                    <div class="d-flex p-3">
                                                        <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                        <article class="px-4 align-self-center">
                                                            <h3 class="h6 mb-0 pb-0">
                                                                <a href="javascript:void(0)">User Name will be here</a>
                                                            </h3>
                                                            <ul class="review-rating nav">
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                            </ul>
                                                            <p class="fgray pb-0">Task Completed (20)</p>
                                                        </article>
                                                    </div>
                                                    <p class="mb-0 pb-0"><span class="fbold">Description: </span>Lorem ipsum dolor sit amet consectetur adipisicing elit Lorem, ipsum.sit amet consectetur adipisicing elit. <a href="javascript:void(0)" class="fgray">More ></a></p>

                                                     <!-- slick slider -->
                                                    <div class="task-slick custom-slick">
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part01.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part02.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part03.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part04.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part05.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                        <!-- slide -->
                                                        <div class="slider">
                                                            <figure>
                                                                <img src="img/data/part06.jpg" alt="">
                                                            </figure>
                                                        </div>
                                                        <!--/ slide-->
                                                    </div>
                                                    <!--/ slick slider -->

                                                    <p class="d-flex justify-content-between">
                                                        <span class="fgray">10 Min Ago</span>
                                                        <span><a href="javascript:void(0)" id="replybtn">Reply</a></span>
                                                    </p>

                                                    <!-- write message -->
                                                    <form id="showreply">
                                                        <div class="form-group ">
                                                            <label>Write Message</label>
                                                            <div class="input-group attach-textearea">
                                                                <textarea class="form-control mb-2" style="height:100px;"></textarea>
                                                                <a href="javascript:void(0)" class="attachicon"><span class="icon-paperclip icomoon"></span></a>
                                                            </div>
                                                            
                                                            <input type="submit" class="btn redbtn" value="Submit">
                                                        </div>
                                                    </form>
                                                    <!--/ write mesage -->
                                                </div>
                                            </div>
                                        <!--/ chat message -->
                                        </div>
                                        <!--/ offers -->

                                        <!-- questioins -->
                                        <div>
                                        <h4 class="h5">Questions (20)</h4>

                                            <!-- question -->
                                            <div class="question-block py-2 border-bottom">
                                                <h6 class="h6 fgray">Q: Question name by writer will be here</h6>
                                                <p>A: Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veritatis est dolorem minus aperiam ad possimus praesentium ullam velit tempore voluptatibus.</p>
                                                <div class="row">
                                                    <!-- col 8-->
                                                    <div class="col-lg-8">
                                                        <div class="d-flex p-3">
                                                            <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                            <article class="px-4 align-self-center">
                                                                <h3 class="h6 mb-0 pb-0">
                                                                    <a href="javascript:void(0)">User Name will be here</a>
                                                                </h3>
                                                                <ul class="review-rating nav">
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <!--/ col 8 -->
                                                    <!-- col 4-->
                                                    <div class="col-lg-4 text-right align-self-center">
                                                        <p class="fgray pb-0 mb-0">Posted on 2 Oct 2019</p>
                                                    </div>
                                                    <!--/ col 4-->
                                                </div>
                                            </div>
                                            <!-- question -->

                                            <!-- question -->
                                            <div class="question-block py-2 border-bottom">
                                                <h6 class="h6 fgray">Q: Question name by writer will be here</h6>
                                                <p>A: Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veritatis est dolorem minus aperiam ad possimus praesentium ullam velit tempore voluptatibus.</p>
                                                <div class="row">
                                                    <!-- col 8-->
                                                    <div class="col-lg-8">
                                                        <div class="d-flex p-3">
                                                            <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                            <article class="px-4 align-self-center">
                                                                <h3 class="h6 mb-0 pb-0">
                                                                    <a href="javascript:void(0)">User Name will be here</a>
                                                                </h3>
                                                                <ul class="review-rating nav">
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <!--/ col 8 -->
                                                    <!-- col 4-->
                                                    <div class="col-lg-4 text-right align-self-center">
                                                        <p class="fgray pb-0 mb-0">Posted on 2 Oct 2019</p>
                                                    </div>
                                                    <!--/ col 4-->
                                                </div>
                                            </div>
                                            <!-- question -->

                                            <!-- question -->
                                            <div class="question-block py-2 border-bottom">
                                                <h6 class="h6 fgray">Q: Question name by writer will be here</h6>
                                                <p>A: Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veritatis est dolorem minus aperiam ad possimus praesentium ullam velit tempore voluptatibus.</p>
                                                <div class="row">
                                                    <!-- col 8-->
                                                    <div class="col-lg-8">
                                                        <div class="d-flex p-3">
                                                            <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                            <article class="px-4 align-self-center">
                                                                <h3 class="h6 mb-0 pb-0">
                                                                    <a href="javascript:void(0)">User Name will be here</a>
                                                                </h3>
                                                                <ul class="review-rating nav">
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <!--/ col 8 -->
                                                    <!-- col 4-->
                                                    <div class="col-lg-4 text-right align-self-center">
                                                        <p class="fgray pb-0 mb-0">Posted on 2 Oct 2019</p>
                                                    </div>
                                                    <!--/ col 4-->
                                                </div>
                                            </div>
                                            <!-- question -->

                                            <!-- question -->
                                            <div class="question-block py-2 border-bottom">
                                                <h6 class="h6 fgray">Q: Question name by writer will be here</h6>
                                                <p>A: Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veritatis est dolorem minus aperiam ad possimus praesentium ullam velit tempore voluptatibus.</p>
                                                <div class="row">
                                                    <!-- col 8-->
                                                    <div class="col-lg-8">
                                                        <div class="d-flex p-3">
                                                            <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                            <article class="px-4 align-self-center">
                                                                <h3 class="h6 mb-0 pb-0">
                                                                    <a href="javascript:void(0)">User Name will be here</a>
                                                                </h3>
                                                                <ul class="review-rating nav">
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <!--/ col 8 -->
                                                    <!-- col 4-->
                                                    <div class="col-lg-4 text-right align-self-center">
                                                        <p class="fgray pb-0 mb-0">Posted on 2 Oct 2019</p>
                                                    </div>
                                                    <!--/ col 4-->
                                                </div>
                                            </div>
                                            <!-- question -->

                                            <!-- question -->
                                            <div class="question-block py-2">
                                                <h6 class="h6 fgray">Q: Question name by writer will be here</h6>
                                                <p>A: Lorem ipsum, dolor sit amet consectetur adipisicing elit. Veritatis est dolorem minus aperiam ad possimus praesentium ullam velit tempore voluptatibus.</p>
                                                <div class="row">
                                                    <!-- col 8-->
                                                    <div class="col-lg-8">
                                                        <div class="d-flex p-3">
                                                            <img class="user-pic" src="img/data/chairmanpic.jpg"/>
                                                            <article class="px-4 align-self-center">
                                                                <h3 class="h6 mb-0 pb-0">
                                                                    <a href="javascript:void(0)">User Name will be here</a>
                                                                </h3>
                                                                <ul class="review-rating nav">
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                                                                    <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                                                </ul>
                                                            </article>
                                                        </div>
                                                    </div>
                                                    <!--/ col 8 -->
                                                    <!-- col 4-->
                                                    <div class="col-lg-4 text-right align-self-center">
                                                        <p class="fgray pb-0 mb-0">Posted on 2 Oct 2019</p>
                                                    </div>
                                                    <!--/ col 4-->
                                                </div>
                                            </div>
                                            <!-- question -->
                                        </div>
                                        <!--/ questions -->
                                    </div>
                                    <!--/ tab container -->
                                </div>
                                <!--/ tab -->                               
                          </div>
                          <!--/ quetions block-->                       

                       </div>
                       <!-- task detail right -->
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row left and right -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->



</body>

</html>