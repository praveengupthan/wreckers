<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
               <!-- left navigation -->
               <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <!-- title -->
                    <div class="db-pagetitle d-flex justify-content-between">
                        <article>
                            <h2 class="h5 fbold">Part Name will be here</h2>
                            <!-- <p>Myorders of Delivered, Payment Done, Waiting for Delivery</p> -->
                        </article>  
                        <a href="user-myoreders.php"><span class="icon-arrow-left"></span> Back to My Orders</a>                      
                    </div>
                    <!--/ title -->
                   
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-12">
                                <ul class="row primarydetails">
                                    <li class="col-lg-4">
                                        <h6>Name</h6>
                                        <p>Praveen Kumar Nandipati</p>
                                    </li>
                                    <li class="col-lg-4">
                                        <h6>Order number	</h6>
                                        <p>18100614451880850561	</p>
                                    </li>
                                    <li class="col-lg-4">
                                        <h6>Order Date & Time </h6>
                                        <p>06 Oct 2018 14:45:19	</p>
                                    </li>
                                </ul>
                                <ul class="row primarydetails">
                                    <li class="col-lg-4">
                                        <h6>Payment Method</h6>
                                        <p>Netbanking/Card/Wallet </p>
                                    </li>
                                    <li class="col-lg-4">
                                        <h6 class="h6">Total 	<span class=" float-right">Rs: 8315</span>	</h6>
                                        <p class="border-bottom">Delivery Charges :<span class=" float-right">Rs: 230</span>	</p>
                                        <h5 class="h6 py-3">Payble Amount <span class="fred  float-right">Rs:8315</span></h5>
                                    </li>                                       
                                </ul>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                         <!-- row -->
                         <div class="row py-4">
                            <!-- col -->
                            <div class="col-lg-2 col-md-3">
                                <figure class="imgproduct">
                                    <a href="javascript:void(0)">
                                        <img src="img/data/pro05.jpg" alt="" title="" class="img-fluid">
                                    </a>
                                </figure>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-10 col-md-9">
                                <h6 class="h6 fbold">Product Name will be here</h6>
                                <p>Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                
                                <!-- <a href="javascript:void(0)" class="redbtn">Paynow</a>                                       
                                <a href="javascript:void(0)" class="whitebtn">return / replace</a> -->
                            </div>
                            <!--/ col -->                                                        
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <p class="">Status: <span class="fred">Delivered</span></p>
                            </div>
                            <div class="col-lg-6 col-md-6 text-right"> 
                                <p class="text-right">Delivered on: <span class="fred">11 July 2019</span></p>
                            </div>
                        </div>
                        <!--/ row -->

                         <!-- order status -->
                         <div class="ord-status d-flex dashedbrd">
                            <div class="barcol"> <span class="fgreen sttext">Ordered</span>
                                <div class="barstrip"> <a href="javascript:void(0)" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><span class="circle"></span></a> </div>
                            </div>
                            <div class="barcol"> <span class="fgreen sttext">Packed</span>
                                <div class="barstrip"> <a href="javascript:void(0)" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><span class="circle"></span></a> </div>
                            </div>
                            <div class="barcol"> <span class="fgreen sttext">Shipped</span> <span class="fgreen sttext deliveredst">Delivered</span>
                                <div class="barstrip process"> 
                                    <a href="javascript:void(0)" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><span class="circle"></span></i>
                                </a> 
                                <a href="javascript:void(0)" class="delivered" data-target="toggle" data-toggle="tooltip" title="Wed, 7th Mar 12:25 pm" data-placement="bottom"><span class="circle"></span>
                                </a> 
                                </div>
                            </div>
                        </div>
                        <!--/ order status -->

                        <p class="py-3">Replacement was allowed till Sat, 25 Oct 2014.</p>

                        <div class="tw100 ">
                            <h6 class="h6 fred fbold">Shipping Information</h6>
                            <p>Praveen Guptha Nandipati</p>
                            <p>Plot No:25.1, 8-3-833/25, Phase I, Kamalapuri Colony, Hyderabad ,Near Satyasai Nigamagam , Hyderabad, Telangana - 500072 </p>
                            <p>Mobile No: 7995165789</p>
                        </div>


                   
                  

                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>