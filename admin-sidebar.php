<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="admin-dashboard.php"> 
            <div class="sidebar-brand-text mx-3">WRECKERS ADMIN</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
        <a class="nav-link" href="admin-dashboard.php">           
            <span>Dashboard</span></a>
        </li>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">           
                <a class="collapse-item" href="admin-datatable.php">Tables View</a>
                <a class="collapse-item" href="javascript:void(0)">Detail</a>
                <a class="collapse-item" href="admin-newobject.php">Create Form</a>
            </div>
        </div>
        </li>       

        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)">
                <i class="fas fa-fw fa-table"></i>
                <span>Dummy Link</span>
            </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>


        </ul>