<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
              <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <div class="db-pagetitle d-flex justify-content-between">
                        <article>
                            <h2 class="h5 fbold">Notifications</h2>
                            <p>Keep up to date with your tasks</p>
                        </article>
                        <div class="form-group">
                            <select class="form-control">
                                <option>All</option>
                                <option>Old Messages</option>
                                <option>Recent Messages</option>
                                <option>1 year Old</option>
                                <option>2 Years Old</option>
                            </select>
                        </div>
                    </div>

                    <!-- notifications -->
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action">Puja B. commented on Need a resume done </a>
                        <a href="#" class="list-group-item list-group-item-action">Mughees I. commented on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action">Mughees I. commented on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action">Mughees I. has made an offer on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action disabled">Mughees I. commented on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action">Puja B. commented on Need a resume done </a>
                        <a href="#" class="list-group-item list-group-item-action">Mughees I. commented on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action">Mughees I. commented on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action">Mughees I. has made an offer on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action disabled">Mughees I. commented on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action">Puja B. commented on Need a resume done </a>
                        <a href="#" class="list-group-item list-group-item-action">Mughees I. commented on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action">Mughees I. commented on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action">Mughees I. has made an offer on Need a resume done</a>
                        <a href="#" class="list-group-item list-group-item-action disabled">Mughees I. commented on Need a resume done</a>
                    </div>
                    <!--/ notificatins -->

                  


                  






                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>