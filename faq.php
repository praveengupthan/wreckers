<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Faq's</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
               <!-- row -->
               <div class="row">
                   <div class="col-lg-12">
                        <h4 class="h5 fred">Got Questions? We’ve Got Answers!</h4>
                        <p>Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in anim id est laborum. Allamco laboris nisi ut aliquip ex ea commodo consequat. Aser velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in anim id est laborum.</p>
                   </div>
                 <!-- col -->
                 <div class="col-lg-12">
                    <div class="accordion cust-accord">
                        <h3 class="panel-title">How can I place an order?</h3>
                        <div class="panel-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique ex massa, non interdum nisl hendrerit nec. Sed metus dui, vehicula blandit nisi dictum, euismod bibendum lorem. Mauris vel ligula ut ligula facilisis porttitor quis sit amet leo. Donec sapien tellus, pulvinar a bibendum eu, ultrices vel risus. Nunc fermentum justo vitae lectus molestie, nec suscipit arcu tempor.</p>
                        </div>
                        <h3 class="panel-title">How can I search parts?</h3>
                        <div class="panel-content">
                            <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                        </div>
                        <h3 class="panel-title">How can I sign up/Register on Wreckers?</h3>
                        <div class="panel-content">
                            <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                        </div>
                        <h3 class="panel-title">How can I change my address on Wreckers account?</h3>
                        <div class="panel-content">
                            <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor, vel dictum mauris gravida. Donec in arcu ligula. Suspendisse vitae volutpat turpis, ac fringilla mauris. Vivamus posuere ipsum in mi tempor, at efficitur leo sollicitudin. Sed laoreet, ligula sit amet euismod scelerisque, tortor quam luctus odio, at pharetra tortor ante in metus. Phasellus orci erat, pretium eget elit ut, dignissim varius ipsum.</p>
                        </div>
                        <h3 class="panel-title">How can I recover the password of my boodmo account?</h3>
                        <div class="panel-content">
                            <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor.</p>
                        </div>
                        <h3 class="panel-title">Cancellation before order is dispatched</h3>
                        <div class="panel-content">
                            <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor.</p>
                        </div>
                        <h3 class="panel-title">Cancellation after order is Ready to send/dispatcheds</h3>
                        <div class="panel-content">
                            <p>Curabitur sed placerat mi, quis consectetur quam. Mauris congue ac leo quis laoreet. In rutrum tortor nec lectus semper faucibus. Donec ut placerat nunc. Vivamus elementum tortor erat, a pulvinar massa cursus vitae. Quisque porta neque a nisl porttitor.</p>
                        </div>
                    </div>
                 </div>
                 <!--/ col -->
                
               </div>
               <!--/ row -->

            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>