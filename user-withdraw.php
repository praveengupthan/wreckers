<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Withdraw </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
                <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol wallet">
                    <div class="db-pagetitle d-flex justify-content-between">
                        <article>
                            <h2 class="h5 fbold">Withdraw</h2>
                            <p class="pb-0">Update your Withdraw details</p>                            
                        </article>                                           
                    </div>  
                    <!-- row -->
                    <div class="row">
                         <!-- col 12-->
                         <div class="col-lg-12">
                             <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Id rem magni repellat facere unde alias, quisquam inventore nisi, laudantium modi labore nulla cumque. Autem, itaque!</p>
                         </div>
                         <!--/ col 12-->
                         <div class="col-lg-6">
                             <h5 class="h6 fbold">Billing Address  <a data-toggle="modal" data-target="#newbilling-address" class="fblue pl-3" href="javascript:void(0)"><span class="icon-plus"></span> Add New </a></h5>
                             <p>Address Line 1 12, Fordview Cres, Suburb Bell Post Hill, State VIC, Postcode 3215, Country: Australia</p>
                             <a class="redbtn" href="javascript:void(0)">Remove</a>                             
                         </div>
                         <div class="col-lg-12">
                            <p>
                                 Your address will never been shown publickly.  It is only used for account verification purpose.
                            </p>
                         </div>
                    </div>
                    <!--/ row -->
                    <hr>

                    <!-- account details -->
                     <!-- row -->
                     <div class="row">     
                         <!-- col -->                   
                         <div class="col-lg-6">
                             <h5 class="h6 fbold">Bank Account Details  </h5>
                              <div>
                                    <p>Account Name: Chaitanya Bade</p>
                                    <p>Account Number: *******8866</p>
                                    <a class="redbtn" href="javascript:void(0)">Remove</a>  
                              </div>
                         </div>
                         <!--/ col -->

                         <!-- col 12-->
                         <div class="col-lg-12 pt-3">
                            <h5 class="h6 fbold">Add Bank Account Details  </h5>
                            <form>
                                <div class="row">
                                    <!-- col -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Account Holder Name</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Write Acocunt Holder Name">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Account Number</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Account Number">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <!-- col -->
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>BSB</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Ex: 000-000">
                                            </div>
                                        </div>
                                    </div>
                                    <!--/ col -->
                                    <div class="col-lg-12">
                                        <a class="redbtn" href="javascript:void(0)">Add Account</a>  
                                    </div>
                                </div>
                            </form>
                         </div>
                         <!--/ col 12-->
                         
                    </div>
                    <!--/ row -->
                    <!--/ account details -->
                    <hr>
                    <div>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, iure neque placeat nesciunt numquam ipsum, distinctio magnam corrupti sit sed, facere ab voluptates quae necessitatibus.</p>
                        <a class="redbtn" href="user-wallet.php">Confirm Details</a> 
                    </div>
                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

    

    <!-- Add address -->
    <!-- The Modal -->
  <div class="modal fade" id="newbilling-address" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
            <h4 class="modal-title h5">Add New Billing Address</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
           <p>Your billing address will be verfied before you can receive payments</p>

           <form>
               <!-- form group -->
              <div class="form-group">
                 <label>Address Line 1 </label>
                 <div class="input-group">
                     <input type="text" class="form-control">
                 </div>
              </div>
              <!--/ form group -->
               <!-- form group -->
               <div class="form-group">
                 <label>Suburb</label>
                 <div class="input-group">
                     <input type="text" class="form-control">
                 </div>
              </div>
              <!--/ form group -->
               <!-- form group -->
               <div class="form-group">
                 <label>State</label>
                 <div class="input-group">
                     <input type="text" class="form-control">
                 </div>
              </div>
              <!--/ form group -->
               <!-- form group -->
               <div class="form-group">
                 <label>Post Code</label>
                 <div class="input-group">
                     <input type="text" class="form-control">
                 </div>
              </div>
              <!--/ form group -->
               <!-- form group -->
               <div class="form-group">
                 <label>Country</label>
                 <div class="input-group">
                     <input type="text" class="form-control">
                 </div>
              </div>
              <!--/ form group -->
           </form>
           <a href="javascript:void(0)" class="redbtn">Add Billing Address</a>
        </div>        
        <!-- /modal body -->
    </div>
  </div>
    <!--/ Add address -->

</body>

</html>