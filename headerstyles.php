 <!-- style sheets -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/icomoon.css">
<link rel="stylesheet" href="css/slick.css">
<link rel="stylesheet" href="css/easy-responsive-tabs.css">
<link rel="stylesheet" href="css/simplegallery.css">
<link rel="stylesheet" href="css/fa.min.css">


<!-- data tables -->
<link rel="stylesheet" href="datatables/dataTables.bootstrap4.min.css">


<link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" /> <!-- date picker -->

