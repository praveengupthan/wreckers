<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard </title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">       
        <!-- sub page body -->
        <div class="subpage-body user-body">
           <!-- container -->
           <div class="container">
             <!-- row -->
             <div class="row justify-content-between">
              <!-- left navigation -->
              <div class="col-lg-3 ">
                    <div class="sticky-top">
                        <figure class="user">
                            <img src="img/data/chairmanpic.jpg">
                            <h1 class="h5">User Name will be here</h1>
                            <p class="text-center">praveennandipati@gmail.com</p>
                        </figure>
                        
                        <?php include 'user-dashboard-nav.php' ?>
                    </div>
                </div>
                <!--/ left navigation -->

                <!-- dashboard right -->
                <div class="col-lg-9 user-rightcol">
                    <div class="db-pagetitle">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <article>
                                <h2 class="h5 fbold">All Orders</h2>                            
                            </article>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-4">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Search">
                            </div>
                        </div>
                        <!--/ col -->

                             <!-- col -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <select class="form-control">
                                    <option>All</option>
                                    <option>Orders Delivered</option>
                                    <option>Waitng for Payment</option>
                                    <option>Cancelled</option>
                                    <option>Orders Returned</option>
                                </select>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->  
                    </div>

                    <!-- order row -->
                    <div class="userproduct">
                        <!-- row -->
                        <ul class="row primarydetails">
                            <li class="col-lg-4 col-sm-6">
                                <h6>Name</h6>
                                <p>Praveen Kumar Nandipati</p>
                            </li>
                            <li class="col-lg-4 col-sm-6">
                                <h6>Order number	</h6>
                                <p>18100614451880850561	</p>
                            </li>
                            <li class="col-lg-4 col-sm-6">
                                <h6>Order Date & Time </h6>
                                <p>06 Oct 2018 14:45:19	</p>
                            </li>
                        </ul>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row pb-3">
                            <!-- col -->
                            <div class="col-lg-2 col-md-3 col-sm-3">
                                <figure class="imgproduct">
                                    <a href="user-myordersdetail.php"><img src="img/data/cathome02.jpg" alt="" title="" class="img-fluid"></a>
                                </figure>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-7 col-md-9 col-sm-9">
                                <h6 class="h5 pb-1 mb-0">
                                    <a href="javascript:void(0)">Product Name will be here</a>
                                </h6>
                                <p class="darkgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                <a href="javascript:void(0)" class="redbtn">Review</a>
                                <a href="javascript:void(0)" class="redbtn">Return</a> 
                                <a href="user-myordersdetail.php" class="whitebtn">Order Details</a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-3 col-12 text-right">
                                <h2 class="h2">$498</h2>
                                <p class="fgreen text-right fbold">Delivered</p>
                            </div>
                            <!-- col -->
                            <div class="col-lg-12">
                                <p class="small pt-3 pb-0">Delivered on 25 Oct 2014.</p>
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ order row -->


                    <!-- order row -->
                    <div class="userproduct">
                        <!-- row -->
                        <ul class="row primarydetails">
                            <li class="col-lg-4 col-sm-6">
                                <h6>Name</h6>
                                <p>Praveen Kumar Nandipati</p>
                            </li>
                            <li class="col-lg-4 col-sm-6">
                                <h6>Order number	</h6>
                                <p>18100614451880850561	</p>
                            </li>
                            <li class="col-lg-4 col-sm-6">
                                <h6>Order Date & Time </h6>
                                <p>06 Oct 2018 14:45:19	</p>
                            </li>
                        </ul>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row pb-3">
                            <!-- col -->
                            <div class="col-lg-2 col-md-3 col-sm-3">
                                <figure class="imgproduct">
                                    <a href="user-myordersdetail.php"><img src="img/data/cathome03.jpg" alt="" title="" class="img-fluid"></a>
                                </figure>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-7 col-md-9 col-sm-9">
                                <h6 class="h5 pb-1 mb-0">
                                    <a href="javascript:void(0)">Product Name will be here</a>
                                </h6>
                                <p class="darkgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                <a href="javascript:void(0)" class="redbtn">Track Order</a>
                                <a href="javascript:void(0)" class="redbtn">Cancel Order</a>
                                <a href="user-myordersdetail.php" class="whitebtn">Order Details</a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-3 col-12 text-right">
                                <h2 class="h2">$498</h2>
                                <p class="fblue text-right fbold">Delivery in Process</p>
                            </div>
                            <!-- col -->
                            <div class="col-lg-12">
                                <p class="small pt-3 pb-0">Replacement was allowed till Sat, 25 Oct 2014.</p>
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ order row -->


                    <!-- order row -->
                    <div class="userproduct">
                        <!-- row -->
                        <ul class="row primarydetails">
                            <li class="col-lg-4 col-sm-6">
                                <h6>Name</h6>
                                <p>Praveen Kumar Nandipati</p>
                            </li>
                            <li class="col-lg-4 col-sm-6">
                                <h6>Order number	</h6>
                                <p>18100614451880850561	</p>
                            </li>
                            <li class="col-lg-4 col-sm-6">
                                <h6>Order Date & Time </h6>
                                <p>06 Oct 2018 14:45:19	</p>
                            </li>
                        </ul>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row pb-3">
                            <!-- col -->
                            <div class="col-lg-2 col-md-3 col-sm-3">
                                <figure class="imgproduct">
                                    <a href="user-myordersdetail.php"><img src="img/data/cathome04.jpg" alt="" title="" class="img-fluid"></a>
                                </figure>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-7 col-md-9 col-sm-9">
                                <h6 class="h5 pb-1 mb-0">
                                    <a href="javascript:void(0)">Product Name will be here</a>
                                </h6>
                                <p class="darkgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                <a href="javascript:void(0)" class="redbtn">Check Availability</a>
                                <a href="user-myordersdetail.php" class="whitebtn">Order Details</a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-3 col-12 text-right">
                                <h2 class="h2">$498</h2>
                                <p class="tred text-right fbold">Cancelled</p>
                            </div>
                            <!-- col -->
                            <div class="col-lg-12">
                                <p class="small pt-3 pb-0">Replacement was allowed till Sat, 25 Oct 2014.</p>
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ order row -->


                    <!-- order row -->
                    <div class="userproduct">
                        <!-- row -->
                        <ul class="row primarydetails">
                            <li class="col-lg-4 col-sm-6">
                                <h6>Name</h6>
                                <p>Praveen Kumar Nandipati</p>
                            </li>
                            <li class="col-lg-4 col-sm-6">
                                <h6>Order number	</h6>
                                <p>18100614451880850561	</p>
                            </li>
                            <li class="col-lg-4 col-sm-6">
                                <h6>Order Date & Time </h6>
                                <p>06 Oct 2018 14:45:19	</p>
                            </li>
                        </ul>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row pb-3">
                            <!-- col -->
                            <div class="col-lg-2 col-md-3 col-sm-3">
                                <figure class="imgproduct">
                                    <a href="user-myordersdetail.php"><img src="img/data/cathome05.jpg" alt="" title="" class="img-fluid"></a>
                                </figure>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-7 col-md-9 col-sm-9">
                                <h6 class="h5 pb-1 mb-0">
                                    <a href="javascript:void(0)">Product Name will be here</a>
                                </h6>
                                <p class="darkgray">Innovative Joyetech NCFilmTM heater along with the CUBIS Max tank. Being a coil-less</p>
                                <a href="javascript:void(0)" class="redbtn">Review</a>
                                <a href="user-myordersdetail.php" class="whitebtn">Order Details</a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-3 col-12 text-right">
                                <h2 class="h2">$498</h2>
                                <p class="tred text-right fbold">Returned</p>
                            </div>
                            <!-- col -->
                            <div class="col-lg-12">
                                <p class="small pt-3 pb-0">Replacement was allowed till Sat, 25 Oct 2014.</p>
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ order row -->


                </div>
                <!--/ dashboard right -->
            </div>
            <!--/ row --> 
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>