<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tasklist</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->

    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Task List</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- top search filters -->
                    <div class="col-lg-12">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <input type="text" placeholder="Enter Part name" class="form-control">
                                </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option value="">Select Status</option>
                                        <option value="">Filter 01</option>
                                        <option value="">Filter 02</option>
                                        <option value="">Filter 03</option>
                                        <option value="">Filter 04</option>
                                        <option value="">Filter 05</option>
                                        <option value="">Filter 06</option>
                                        <option value="">Filter 07</option>
                                        <option value="">Filter 08</option>
                                        <option value="">Filter 09</option>
                                    </select>
                                </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option value="">Select filter Name 02</option>
                                        <option value="">Filter 01</option>
                                        <option value="">Filter 02</option>
                                        <option value="">Filter 03</option>
                                        <option value="">Filter 04</option>
                                        <option value="">Filter 05</option>
                                        <option value="">Filter 06</option>
                                        <option value="">Filter 07</option>
                                        <option value="">Filter 08</option>
                                        <option value="">Filter 09</option>
                                    </select>
                                </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <select class="form-control">
                                        <option value="">Select filter Name 03</option>
                                        <option value="">Filter 01</option>
                                        <option value="">Filter 02</option>
                                        <option value="">Filter 03</option>
                                        <option value="">Filter 04</option>
                                        <option value="">Filter 05</option>
                                        <option value="">Filter 06</option>
                                        <option value="">Filter 07</option>
                                        <option value="">Filter 08</option>
                                        <option value="">Filter 09</option>
                                    </select>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ top search filters -->
                </div>
                <!--/ row -->   
                
                <!-- row left and right -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-lg-4">
                        <div class="left-task">
                            <!-- form group -->
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="">My Request</option>
                                    <option value="">Open</option>
                                    <option value="">Closed</option>
                                    <option value="">Expired</option>                                   
                                </select>
                            </div>
                            <!--/ form group -->

                            <!-- request col -->
                            <div class="request-col mb-2">
                                <h2 class="d-flex justify-content-between">
                                    <span>Request of Bumper</span>
                                    <span class="fbold fred price">$ 45</span>
                                </h2>
                                <div class="row address-block py-3">
                                    <div class="col-lg-8 align-self-center">
                                        <p>91, Jayanagar, Block No:41, Dadar Apartments, Hyderabad, JK, USA</p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open</span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> 10-01-2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                            <!-- request col -->
                            <div class="request-col mb-2">
                                <h2 class="d-flex justify-content-between">
                                    <span>Request of Bumper</span>
                                    <span class="fbold fred price">$ 45</span>
                                </h2>
                                <div class="row address-block py-3">
                                    <div class="col-lg-8 align-self-center">
                                        <p>91, Jayanagar, Block No:41, Dadar Apartments, Hyderabad, JK, USA</p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fgreen align-self-center">Open</span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> 10-01-2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                            <!-- request col -->
                            <div class="request-col mb-2 expired-request">
                                <h2 class="d-flex justify-content-between">
                                    <span>Request of Bumper</span>
                                    <span class="fbold fred price">$ 45</span>
                                </h2>
                                <div class="row address-block py-3">
                                    <div class="col-lg-8 align-self-center">
                                        <p>91, Jayanagar, Block No:41, Dadar Apartments, Hyderabad, JK, USA</p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fred align-self-center">Closed</span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> 10-01-2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->

                            <!-- request col -->
                            <div class="request-col mb-2 expired-request" disabled>
                                <h2 class="d-flex justify-content-between">
                                    <span>Request of Bumper</span>
                                    <span class="fbold fred price">$ 45</span>
                                </h2>
                                <div class="row address-block py-3">
                                    <div class="col-lg-8 align-self-center">
                                        <p>91, Jayanagar, Block No:41, Dadar Apartments, Hyderabad, JK, USA</p>
                                    </div>
                                    <div class="col-lg-4 align-self-center">
                                        <img src="img/data/part05.jpg" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <h3 class="d-flex justify-content-between">
                                    <span class="fred align-self-center">Expired</span>
                                    <span class="fgray datesel"><span class="icon-calendar icomoon"></span> 10-01-2019</span>
                                </h3>
                            </div>
                            <!--/ request col -->
                           
                        </div>
                    </div>
                    <!--/ left col -->

                    <!-- right col -->
                    <div class="col-lg-8">
                       <div class="task-right">
                            <!-- values -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-4 text-center">
                                    <div class="value-col">
                                        <h4><span>29</span></h4>
                                        <h5>Today Requests</h5>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-4 text-center">
                                    <div class="value-col">
                                        <h4><span>29</span></h4>
                                        <h5>This Week</h5>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-4 text-center">
                                    <div class="value-col">
                                        <h4><span>29</span></h4>
                                        <h5>Successful Requests</h5>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ values -->

                            <!-- map row -->
                            <div class="row py-3">
                                <div class="col-lg-12">
                                    <div class="whitebox">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15220.162064645885!2d78.41834594999999!3d17.50557925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1572786330386!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                    </div>
                                </div>
                            </div>
                            <!--/ map row -->
                       </div>
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row left and right -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>