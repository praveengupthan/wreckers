<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Return Product</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>
    <!-- header -->
    <?php include 'headerpostlogin.php' ?>
    <!--/ header -->
    <!--main subpage -->
    <main class="subpage">
        <!-- sub page title -->
        <div class="pagetitle">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h1>Return Product</h1>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page title -->
        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <form>
                    <!-- row -->
                    <div class="row justify-content-center pb-4">
                        <!-- left col 6-->
                        <div class="col-lg-6">
                            <!-- return product details -->
                            <div class="row pb-3">
                                <div class="col-lg-12">
                                    <p>Return Product Details</p>
                                </div>
                                <div class="col-lg-4">
                                    <a href="javascript:void(0)">
                                        <img src="img/data/cathome02.jpg" alt="" class="img-fluid">
                                    </a>
                                </div>
                                <div class="col-lg-8 return-product">
                                    <h2>Part Name will be here</h2>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Enim est eligendi expedita.</p>
                                    <small class="small">Purchased on: <span class="fbold">13-09-2019</span></small>
                                </div>
                            </div>
                            <!--/ return product details -->
                            <div class="form-group">
                                <label class="label">Why do you want to return this product <span>*</span></label>
                                <select class="form-control">
                                    <option>Return Reason 01</option>
                                    <option>Return Reason 02</option>
                                    <option>Return Reason 03</option>
                                    <option>Return Reason 04</option>
                                    <option>Return Reason 05</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="label">Description <span>*</span></label>
                                <textarea style="height:100px;" class="form-control" placeholder="(Minimum 25 Characters)"></textarea>
                            </div>

                            <div class="form-group">
                                <label class="d-block">Upload Images  </label>
                                <div class="custom-file-upload float-left mr-2">
                                    <label for="file-upload" class="custom-file-upload1">
                                        <p class="text-center"> <span class="icon-camera icomoon"></span></p>
                                        Upload Image
                                    </label>
                                    <input id="file-upload" type="file"/>
                                </div>
                                <div class="custom-file-upload mx-2">
                                    <label for="file-upload2" class="custom-file-upload1">
                                        <p class="text-center"> <span class="icon-camera icomoon"></span></p>
                                        Upload Image
                                    </label>
                                    <input id="file-upload2" type="file"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="redbtn" value="Cancel & Save draft">
                                <input type="submit" class="redbtn" value="Post Now">
                            </div>
                        </div>
                        <!--/ left col 6-->
                    </div>
                    <!--/ row -->
                </form>
            </div>
            <!--/ container -->
            <!-- new cars -->
            <div class="new-cars">
                <!-- container -->
                <div class="container">
                   <!-- title row -->
                   <div class="row">
                        <div class="col-lg-12">
                            <div class="title-row">
                                <h2>New Cars</h2>
                            </div>
                        </div>
                    </div>
                    <!--/ title row -->
                    <!-- row -->
                    <div class="row">
                        <!-- col 12-->
                        <div class="col-lg-12">
                             <!-- slick row -->
                             <div class="testimonial-slick custom-slick">
                                <!-- slider -->
                                <div class="slider text-center test-col carcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/car01.jpg" class="mx-auto img-fluid">
                                        </figure>
                                        <h3>Audi New A6</h3>
                                        <h4>50-60 Lakhs</h4>                                        
                                        <small class="small">Estimated Price</small>
                                    </a>
                                </div>
                                <!--/ slider -->
                               <!-- slider -->
                               <div class="slider text-center test-col carcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/car02.jpg" class="mx-auto img-fluid">
                                        </figure>
                                        <h3>Hyundai  Tucson Facelift</h3>
                                        <h4>18-20 Lakhs</h4>                                        
                                        <small class="small">Estimated Price</small>
                                    </a>
                                </div>
                                <!--/ slider -->
                                <!-- slider -->
                                <div class="slider text-center test-col carcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/car03.jpg" class="mx-auto img-fluid">
                                        </figure>
                                        <h3>Audi Q2</h3>
                                        <h4>50-60 Lakhs</h4>                                        
                                        <small class="small">Estimated Price</small>
                                    </a>
                                </div>
                                <!--/ slider -->
                                <!-- slider -->
                                <div class="slider text-center test-col carcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/car04.jpg" class="mx-auto img-fluid">
                                        </figure>
                                        <h3>Tata Buzzard</h3>
                                        <h4>12-15 Lakhs</h4>                                        
                                        <small class="small">Estimated Price</small>
                                    </a>
                                </div>
                                <!--/ slider -->
                                 <!-- slider -->
                                <div class="slider text-center test-col carcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/car01.jpg" class="mx-auto img-fluid">
                                        </figure>
                                        <h3>Audi New A6</h3>
                                        <h4>50-60 Lakhs</h4>                                        
                                        <small class="small">Estimated Price</small>
                                    </a>
                                </div>
                                <!--/ slider -->
                               <!-- slider -->
                               <div class="slider text-center test-col carcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/car02.jpg" class="mx-auto img-fluid">
                                        </figure>
                                        <h3>Audi New A6</h3>
                                        <h4>50-60 Lakhs</h4>                                        
                                        <small class="small">Estimated Price</small>
                                    </a>
                                </div>
                                <!--/ slider -->
                            </div>
                            <!--/ slick row -->
                        </div>
                        <!--/ col 12-->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ new cars -->

            <!-- recommendations -->           
            <div class="container">
                <!-- title row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="title-row">
                            <h2>Recommendations <a href="javascript:void(0)">View All</a></h2>
                        </div>
                    </div>
                </div>
                <!--/ title row -->
                <!-- slick slider row -->
                <div class="catgories-home custom-slick">
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome01.jpg" alt="" class="img-fluid">                            
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>                           
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome02.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>                            
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome03.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome04.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome05.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome06.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome07.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                    <!-- slide -->
                    <div class="slide">
                        <a href="javascript:void(0)">
                            <img src="img/data/cathome08.jpg" alt="" class="img-fluid">
                            <article class="product-det text-center">
                                <h5>Auto Clutch & Brake</h5>
                                <p class="d-flex justify-content-around align-self-center">
                                    <span class="price">$165.00</span>
                                    <span class="icon-shopping-cart-of-checkered-design icomoon align-self-center"></span>
                                </p>
                            </article>
                        </a>
                    </div>
                    <!--/ slide -->
                </div>
                <!--/ slick slider row -->
            </div>
            <!--/ recommendations -->
        </div>
        <!--/ sub page body -->
    </main>
    <!--/ main subpage -->
    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->
</body>

</html>