<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->

    <!--main -->
    <main>
        <!-- sign template -->
        <div class="sign-section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-5">
                        <h1 class="title-login">CUSTOMER REGISTRATION</h1>
                       
                        <div class="signcol">
                            <form>
                                <div class="form-group">
                                    <label>Create User Name</label>
                                    <input type="text" class="form-control" placeholder="Create Username">
                                    <span class="icon-user-silhouette icomoon"></span>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" placeholder="Write your Email">
                                    <span class="icon-sent-mail icomoon"></span>
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control" placeholder="Write Your Mobile Number">
                                    <span class="icon-telephone icomoon"></span>
                                </div>
                                <div class="form-group">
                                    <label>Create Password</label>
                                    <input type="password" class="form-control" placeholder="Create Password">
                                    <span class="icon-unlock icomoon"></span>
                                </div>                               
                                <input type="submit" value="REGISTER" class="w-100 redbtn">
                            </form>

                            <p class="text-center or"><span>OR</span></p>
                            <p class="small text-center">Signin with your Social Networks</p>

                            <div class="d-flex justify-content-between py-3">
                                <a class="socialbtn fbbtn" href="javascript:void(0)">
                                    <span class="icon-facebook icomoon"></span> 
                                    Facebook
                                </a>
                                <a class="socialbtn googlesocial" href="javascript:void(0)">
                                    <span class="icon-google-plus icomoon"></span> 
                                    Google Plus
                                </a>
                            </div>
                            <p class="text-center small">You Already have Account?, <a class="fred" href="javascript:void(0)">Signin</a></p>
                            <p class="text-center">Register as</a></p>
                            <ul class="nav justify-content-center">                                
                                <li class="nav-item"><a class="nav-link text-uppercase" href="javascript:void(0)">WRECKER</a></li>
                                <li class="nav-item"><a class="nav-link text-uppercase" href="javascript:void(0)">MECHANIC</a></li>
                            </ul>
                        </div>
                       
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sign template -->
    </main>
    <!--/ main -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>