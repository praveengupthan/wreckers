<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wreckers Parts</title>
    <?php include 'headerstyles.php' ?>
</head>

<body>

    <!-- header -->
    <?php include 'header.php' ?>
    <!--/ header -->
    <!--main -->
    <main>
        <!-- slider -->
        <div class="slider-section">
            <!-- container -->
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center d-none d-sm-block">
                        <img src="img/slidercarimg.png" class="img-fluid" title="" alt="">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-5  text-center justify-content-center slider-right">
                        <h1>Locate auto parts & spares at wreckers near you</h1>

                        <!-- search -->
                        <div class="search-section">
                            <div class="d-flex justify-content-between">
                                <input type="text" placeholder="Enter Part Details here">
                               <a href="javascript:void(0)"> <span class="icon-search icomoon"></span></a>
                               
                            </div>
                            <a class="d-block text-right py-2" href="javascript:void(0)">Advance Search</a>
                        </div>
                        <!--/ search-->

                        <!-- row service columns -->
                        <div class="row justify-content-center pt-3">
                            <!-- col -->
                            <div class="col-sm-4 col-4 text-center px-0">
                                <div class="slider-bancol">
                                    <a href="javascript:void(0)">
                                        <img src="img/banner-part-request.svg" alt="">
                                        <h5 class="h5">Part Request</h5>
                                    </a>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-sm-4 col-4 text-center px-0">
                                <div class="slider-bancol">
                                    <a href="javascript:void(0)">
                                        <img src="img/banner-fixmycar.svg" alt="">
                                        <h5 class="h5">Fix my Car</h5>
                                    </a>
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-sm-4 col-4 text-center px-0">
                                <div class="slider-bancol">
                                    <a href="javascript:void(0)">
                                        <img src="img/banner-wreckmycar.svg" alt="">
                                        <h5 class="h5">Wreck my Car</h5>
                                    </a>
                                </div>
                            </div>
                            <!--/ col -->
                           
                        </div>
                        <!--/ row service columns -->
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container-->
        </div>
        <!--/ slider -->

        <!-- popular categories -->
        <div class="container">
            <!-- title row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-row">
                        <h2>Popular Categories <a href="javascript:void(0)">View All</a></h2>
                    </div>
                </div>
            </div>
            <!--/ title row -->

            <!-- slick slider row -->
            <div class="catgories-home custom-slick">
                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/cathome01.png" alt="" class="img-fluid">
                        <span class="d-block sltitle">Head Lights</span>
                    </a>
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/cathome02.png" alt="" class="img-fluid">
                        <span class="d-block sltitle">Head Lights</span>
                    </a>
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/cathome03.png" alt="" class="img-fluid">
                        <span class="d-block sltitle">Car Breaks</span>
                    </a>
                </div>
                <!--/ slide -->
                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/cathome04.png" alt="" class="img-fluid">
                        <span class="d-block sltitle">Seat Covers</span>
                    </a>
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/cathome05.png" alt="" class="img-fluid">
                        <span class="d-block sltitle">Car Wheels</span>
                    </a>
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/cathome06.png" alt="" class="img-fluid">
                        <span class="d-block sltitle">Head Lights</span>
                    </a>
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/cathome07.png" alt="" class="img-fluid">
                        <span class="d-block sltitle">Head Lights</span>
                    </a>
                </div>
                <!--/ slide -->

                <!-- slide -->
                <div class="slide">
                    <a href="javascript:void(0)">
                        <img src="img/data/cathome08.png" alt="" class="img-fluid">
                        <span class="d-block sltitle">Head Lights</span>
                    </a>
                </div>
                <!--/ slide -->
            </div>
            <!--/ slick slider row -->
        </div>
        <!--/ popular categories -->

        <!-- brands -->
        <div class="home-brands py-4">
            <!-- container-->
            <div class="container">
                <!-- title row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="title-row">
                            <h2>Popular Makes</h2>
                        </div>
                    </div>
                </div>
                <!--/ title row -->
            </div>
            <!--/ container -->
            
            <!-- container fluid -->
            <div class="container-fluid">
                <div class="block-hdnews">
                    <div class="list-wrpaaer" style="height:150px;">
                        <ul class="list-aggregate" id="marquee-horizontal">                                                          
                            <li class="fat-l">   <img src="img/data/brand01.jpg" alt="" class="img-fluid">
                            </li>
                            <li class="fat-l">   <img src="img/data/brand02.jpg" alt="" class="img-fluid">
                            </li>
                            <li class="fat-l">   <img src="img/data/brand03.jpg" alt="" class="img-fluid">
                            </li>
                            <li class="fat-l">   <img src="img/data/brand04.jpg" alt="" class="img-fluid">
                            </li>
                            <li class="fat-l">   <img src="img/data/brand05.jpg" alt="" class="img-fluid">
                            </li>
                            <li class="fat-l">  <img src="img/data/brand06.jpg" alt="" class="img-fluid">
                            </li>
                            <li class="fat-l">  <img src="img/data/brand07.jpg" alt="" class="img-fluid">
                            </li>
                            <li class="fat-l">  <img src="img/data/brand08.jpg" alt="" class="img-fluid">
                            </li>
                            <li class="fat-l">  <img src="img/data/brand09.jpg" alt="" class="img-fluid">
                            </li>
                            <li class="fat-l">  <img src="img/data/brand10.jpg" alt="" class="img-fluid">
                            </li>
                        </ul>
                    </div>
                    <!-- list-wrpaaer -->
                </div>
            </div>
            <!--/ container fluid-->
        </div>
        <!--/ brands-->

        <!-- home about -->
        <div class="homeabout">
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6">
                        <iframe width="100%" height="420" src="https://www.youtube.com/embed/5pn8qAKL0V4"
                            frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center">
                        <article>
                            <h3 class="fthin pb-0 mb-0">About us</h3>
                            <small>We are Wreckers Parts</small>
                            <p class="text-justify pt-4 pb-2">Lorem Ipsum is simply dummy text of the printing and
                                typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since
                                the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
                                specimen book. It has survived not only five centuries, but also the leap into
                                electronic typesetting, remaining essentially unchanged.</p>
                            <p class="text-justify"> It was popularised in the 1960s with the release of Letraset sheets
                                containing Lorem Ipsum passages, and more recently with desktop publishing software like
                                Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <div class="chairman-col float-right py-4 d-flex">
                                <img src="img/data/chairmanpic.jpg" alt="" class="pic">
                                <img src="img/data/sign.png" alt="">
                            </div>
                        </article>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
        </div>
        <!--/ home about-->       

        <!-- services -->
        <div class="home-services">
            <!-- container -->
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <h3 class="h1 fbold pb-0 mb-0">What we Offer</h3>
                        <h5 class="fthin">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Iure, sequi eligendi sed assumenda nostrum rerum?  </h5>
                    </div>
                </div>
                <div class="parentHorizontalTab">
                    <ul class="resp-tabs-list hor_1 nav justify-content-center">
                        <li>E-Commerce</li>
                        <li>Part Request</li>
                        <li>Fix my Car</li>
                        <li>Wreck my Car</li>
                    </ul>
                    <div class="resp-tabs-container hor_1">
                        <!-- e commerce -->
                        <div>
                            <!-- container -->
                            <div class="container">
                                 <!-- row -->
                                 <div class="row">
                                        <!-- content col -->
                                        <div class="col-lg-7 align-self-center">  
                                           <article class="home-service-article move-left">
                                            <h5>E- Commerce is simply dummy text of the printing and typesetting
                                                    industry. </h5>
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry. Lorem Ipsum has been the industry's standard dummy text ever
                                                    since the 1500s, when an unknown printer took a galley of type and
                                                    scrambled it to make a type specimen book.</p>
                                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.</p>
                                                <p>
                                                    <a class="float-left mt-2" href="javascript:void(0)">Read More</a>
                                                </p>
                                           </article>
                                        </div>
                                        <!--/ content col -->
                                        <!-- iamge col -->
                                        <div class="col-lg-5 rtimg text-right d-none d-sm-block">
                                            <figure class="home-service-img move-right">
                                                <img src="img/ecommercevector.svg" alt="" class="img-fluid">
                                            </figure>
                                        </div>
                                        <!--/ image col -->
                                    </div>
                                    <!--/ row -->
                            </div>
                            <!--/ container -->
                        </div>
                        <!--/ e commerce -->
                         <!-- Part Request-->
                         <div>
                             <!-- container -->
                             <div class="container">
                                 <!-- row -->
                                 <div class="row">
                                    <!-- content col -->
                                    <div class="col-lg-7 align-self-center">    
                                        <article class="home-service-article move-left">                                  
                                            <h5>Part Request is simply dummy text of the printing and typesetting
                                                industry. </h5>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum has been the industry's standard dummy text ever
                                                since the 1500s, when an unknown printer took a galley of type and
                                                scrambled it to make a type specimen book.</p>
                                        
                                            <p>
                                                <a class="float-left mt-2" href="javascript:void(0)">Read More</a>
                                            </p>
                                        </article>
                                    </div>
                                    <!--/ content col -->
                                    <!-- iamge col -->
                                    <div class="col-lg-5 rtimg text-right d-none d-sm-block">
                                        <figure class="home-service-img move-right">
                                            <img src="img/partrequest2.svg" alt="" class="img-fluid">
                                        </figure>
                                    </div>
                                    <!--/ image col -->
                                </div>
                                <!--/ row -->
                             </div>
                             <!--/ container -->
                         </div>
                        <!--/ Part Request -->
                         <!-- fix my Car -->
                         <div>
                             <!-- container -->
                             <div class="container">
                                  <!-- row -->
                                  <div class="row">
                                    <!-- content col -->
                                    <div class="col-lg-7 align-self-center">    
                                        <article class="home-service-article move-left">                                   
                                            <h5>Fix my Car is simply dummy text of the printing and typesetting
                                                industry. </h5>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum has been the industry's standard dummy text ever
                                                since the 1500s, when an unknown printer took a galley of type and
                                                scrambled it to make a type specimen book.</p>  
                                            <p>
                                                <a class="float-left mt-2" href="javascript:void(0)">Read More</a>
                                            </p>
                                        </a>
                                    </div>
                                    <!--/ content col -->
                                    <!-- iamge col -->
                                    <div class="col-lg-5 rtimg text-right d-none d-sm-block">
                                        <figure class="home-service-img move-right">
                                            <img src="img/partrequest-vector.svg" alt="" class="img-fluid">
                                        </figure>
                                    </div>
                                    <!--/ image col -->
                                </div>
                                <!--/ row -->
                             </div>
                             <!--/ container -->
                         </div>
                        <!--/ Fix my Car-->
                         <!-- Wreck my Car -->
                         <div>
                             <!--container -->
                             <div class="container">
                                  <!-- row -->
                                  <div class="row">
                                    <!-- content col -->
                                    <div class="col-lg-7 align-self-center">    
                                        <article class="home-service-article move-left">
                                            <h5>Wreck my car is simply dummy text of the printing and typesetting
                                                industry. </h5>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry. Lorem Ipsum has been the industry's standard dummy text ever
                                                since the 1500s, when an unknown printer took a galley of type and
                                                scrambled it to make a type specimen book.</p>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                industry.</p>
                                            <p>
                                                <a class="float-left mt-2" href="javascript:void(0)">Read More</a>
                                            </p>
                                        </article>
                                    </div>
                                    <!--/ content col -->
                                    <!-- iamge col -->
                                    <div class="col-lg-5 rtimg text-right d-none d-sm-block">
                                        <figure class="home-service-img move-right">
                                            <img src="img/wreckmycar.svg" alt="" class="img-fluid">
                                        </figure>
                                    </div>
                                    <!--/ image col -->
                                </div>
                                <!--/ row -->
                             </div>
                             <!--/ container -->
                         </div>
                        <!--/ wreckmy car -->  
                    </div>
                </div>
            </div>
            <!--/ container -->
                            
        </div>
        <!--/ services -->

        <!-- Features -->
        <div class="features">
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-lg-6 leftfeatures">
                        <div class="sliderdiv">
                            <div class="sliderdiv-in">
                               
                            </div>
                        </div>
                    </div>
                    <!--/ left col -->
                    <!-- right col -->
                    <div class="col-lg-6 align-self-center">
                        <article>
                            <h2 class="fbold">Highly Competitive Mobile Responsive</h2>
                            <h4>When creating email templates in Stripo, you do not need to worry about their compatibility.</h4>
                            <p>All email templates designed in Stripo HTML email editor become responsive on mobile by default. In case you need advanced customization, we have developed a set of tools to let you easily change the appearance on mobile point-by-point without any fuss.</p>

                            <a class="redbtn" href="javascript:void(0)">Try it Now</a>
                        </article>
                    </div>
                    <!--/ right col -->
                </div>
                <!--/ row -->
            </div>
        </div>
        <!--/ features -->

        <!-- testimonials -->
        <div class="testimonials-home">
            <!-- container -->
            <div class="container-fluid">
                <!-- title row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="title-row">
                            <h2>What Customer Says </h2>
                        </div>
                    </div>
                </div>
                <!--/ title row -->

                <!-- slick row -->
                <div class="testimonial-slick custom-slick">
                    <!-- slider -->
                    <div class="slider text-center test-col">
                        <figure class="text-center mx-auto py-2">
                            <img src="img/data/custimg01.jpg" class="img-rounded">
                        </figure>
                        <h5>Customer Name</h5>
                        <ul class="review-rating nav justify-content-center">
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                        </ul>
                        <p class="small">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum.
                        </p>
                    </div>
                    <!--/ slider -->
                    <!-- slider -->
                    <div class="slider text-center test-col">
                        <figure class="text-center mx-auto py-2">
                            <img src="img/data/custimg02.jpg" class="img-rounded">
                        </figure>
                        <h5>Customer Name</h5>
                        <ul class="review-rating nav justify-content-center">
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                        </ul>
                        <p class="small">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum.
                        </p>
                    </div>
                    <!--/ slider -->
                    <!-- slider -->
                    <div class="slider text-center test-col">
                        <figure class="text-center mx-auto py-2">
                            <img src="img/data/custimg03.jpg" class="img-rounded">
                        </figure>
                        <h5>Customer Name</h5>
                        <ul class="review-rating nav justify-content-center">
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                        </ul>
                        <p class="small">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum.
                        </p>
                    </div>
                    <!--/ slider -->
                    <!-- slider -->
                    <div class="slider text-center test-col">
                        <figure class="text-center mx-auto py-2">
                            <img src="img/data/custimg04.jpg" class="img-rounded">
                        </figure>
                        <h5>Customer Name</h5>
                        <ul class="review-rating nav justify-content-center">
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                        </ul>
                        <p class="small">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum.
                        </p>
                    </div>
                    <!--/ slider -->
                    <!-- slider -->
                    <div class="slider text-center test-col">
                        <figure class="text-center mx-auto py-2">
                            <img src="img/data/custimg01.jpg" class="img-rounded">
                        </figure>
                        <h5>Customer Name</h5>
                        <ul class="review-rating nav justify-content-center">
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                        </ul>
                        <p class="small">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum.
                        </p>
                    </div>
                    <!--/ slider -->
                    <!-- slider -->
                    <div class="slider text-center test-col">
                        <figure class="text-center mx-auto py-2">
                            <img src="img/data/custimg02.jpg" class="img-rounded">
                        </figure>
                        <h5>Customer Name</h5>
                        <ul class="review-rating nav justify-content-center">
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star-half icomoon select"></span></li>
                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                        </ul>
                        <p class="small">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum.
                        </p>
                    </div>
                    <!--/ slider -->
                </div>
                <!--/ slick row -->

            </div>
            <!--/ container -->
        </div>
        <!--/ testimonials -->

        <!-- mechanics and wrerckers -->
        <div class="mechanics-section">
            <!-- container -->
            <div class="container">
                <div class="parentHorizontalTab">
                    <ul class="resp-tabs-list hor_1 nav justify-content-center">
                        <li>Top Mechanics</li>
                        <li>Top Wreckers</li>
                    </ul>
                    <div class="resp-tabs-container hor_1">
                        <div>
                            <!-- top mechanics row-->
                            <div class="row justify-content-center">
                                <!-- col -->
                                <div class="col-lg-3 text-center test-col mechcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/custimg01.jpg" class="img-rounded">
                                        </figure>
                                        <h5>Mechanic Name, city name</h5>
                                        <ul class="review-rating nav justify-content-center">
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span>
                                            </li>
                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                        </ul>
                                        <p>Completed 220 Jobs</p>
                                    </a>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 text-center test-col mechcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/custimg02.jpg" class="img-rounded">
                                        </figure>
                                        <h5>Mechanic Name, city name</h5>
                                        <ul class="review-rating nav justify-content-center">
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span>
                                            </li>
                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                        </ul>
                                        <p>Completed 220 Jobs</p>
                                    </a>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 text-center test-col mechcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/custimg03.jpg" class="img-rounded">
                                        </figure>
                                        <h5>Mechanic Name, city name</h5>
                                        <ul class="review-rating nav justify-content-center">
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span>
                                            </li>
                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                        </ul>
                                        <p>Completed 220 Jobs</p>
                                    </a>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 text-center test-col mechcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/custimg04.jpg" class="img-rounded">
                                        </figure>
                                        <h5>Mechanic Name, city name</h5>
                                        <ul class="review-rating nav justify-content-center">
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span>
                                            </li>
                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                        </ul>
                                        <p>Completed 220 Jobs</p>
                                    </a>
                                </div>
                                <!--/ col -->                               

                                <div class="col-lg-12 text-center my-3">
                                    <a href="javascript:void(0)" class="btn">View All</a>
                                </div>
                            </div>
                            <!--/ top mechanics row -->
                        </div>
                        <div>
                            <!-- top Wreckers row-->
                            <div class="row justify-content-center">
                                <!-- col -->
                                <div class="col-lg-3 text-center test-col mechcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/custimg01.jpg" class="img-rounded">
                                        </figure>
                                        <h5>Wrecker Name, city name</h5>
                                        <ul class="review-rating nav justify-content-center">
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span>
                                            </li>
                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                        </ul>
                                        <p>Completed 220 Jobs</p>
                                    </a>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 text-center test-col mechcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/custimg02.jpg" class="img-rounded">
                                        </figure>
                                        <h5>Wrecker Name, city name</h5>
                                        <ul class="review-rating nav justify-content-center">
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span>
                                            </li>
                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                        </ul>
                                        <p>Completed 220 Jobs</p>
                                    </a>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 text-center test-col mechcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/custimg03.jpg" class="img-rounded">
                                        </figure>
                                        <h5>Wrecker Name, city name</h5>
                                        <ul class="review-rating nav justify-content-center">
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span>
                                            </li>
                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                        </ul>
                                        <p>Completed 220 Jobs</p>
                                    </a>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-3 text-center test-col mechcol">
                                    <a href="javascript:void(0)">
                                        <figure class="text-center mx-auto py-2">
                                            <img src="img/data/custimg04.jpg" class="img-rounded">
                                        </figure>
                                        <h5>Wrecker Name, city name</h5>
                                        <ul class="review-rating nav justify-content-center">
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star icomoon select"></span></li>
                                            <li class="nav-item"><span class="icon-star-half icomoon select"></span>
                                            </li>
                                            <li class="nav-item"><span class="icon-star icomoon"></span></li>
                                        </ul>
                                        <p>Completed 220 Jobs</p>
                                    </a>
                                </div>
                                <!--/ col -->                               

                                <div class="col-lg-12 text-center my-3">
                                    <a href="javascript:void(0)" class="btn">View All</a>
                                </div>
                            </div>
                            <!--/ top Wreckers row -->
                        </div>
                    </div>
                </div>
            </div>
            <!--/ container -->
        </div>
        <!--/ mechanics and wereckers -->
    </main>
    <!--/ main -->

    <!--footer -->
    <?php include 'footer.php' ?>
    <!--/ footer -->

</body>

</html>